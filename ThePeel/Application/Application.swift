//
//  Application.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct Application {
    // MARK: - SingleTon pattern
    static let shared = Application()
    private let tabBarController = UITabBarController()
    private init() {}
    
    func configMainInterface(window: UIWindow) {
        let pizzaNavigationController = UINavigationController()
        pizzaNavigationController.tabBarItem = UITabBarItem(title: "",
                                                           image: #imageLiteral(resourceName: "iconTabPizza"),
                                                           selectedImage: #imageLiteral(resourceName: "iconTabPizzaActive"))
        let homeNavigator = DefaultHomeNavigator(navigation: pizzaNavigationController)
        let othersNavigationController = UINavigationController()
        othersNavigationController.tabBarItem = UITabBarItem(title: "",
                                                            image: #imageLiteral(resourceName: "iconTabDrink"),
                                                            selectedImage: #imageLiteral(resourceName: "iconTabDrinkActive"))
        let orthersNavigator = OthersDrinkNavigator(navigation: othersNavigationController)
        let profileNavigationController = UINavigationController()
        profileNavigationController.tabBarItem = UITabBarItem(title: "",
                                                            image: #imageLiteral(resourceName: "iconTabProfileNone"),
                                                            selectedImage: #imageLiteral(resourceName: "iconTabProfileActive"))
        let profileNavigator = DefaultMainProfileNavigator(navigation: profileNavigationController)
        let notifiNavigationController = UINavigationController()
        notifiNavigationController.tabBarItem = UITabBarItem(title: "",
                                                              image: #imageLiteral(resourceName: "iconTabNotifi"),
                                                              selectedImage: #imageLiteral(resourceName: "iconTabNotifiActive"))
        let notificationNavigator = DefaultNotificationNavigator(navigation: notifiNavigationController)
        setupUI()
        let navigationViewControllers = [pizzaNavigationController, othersNavigationController, notifiNavigationController, profileNavigationController]
        navigationViewControllers.forEach { viewController in
            viewController.removeTabbarItemsText()
        }
    
        tabBarController.viewControllers = navigationViewControllers
        window.rootViewController = tabBarController
        homeNavigator.toHome()
        orthersNavigator.toOthers()
        profileNavigator.toMainProfile()
        notificationNavigator.toNotification()
        window.makeKeyAndVisible()
    }
    
    private func setupUI() {
        /// Setup tabBar UI
        tabBarController.tabBar.backgroundColor = UIColor(hexString: GlobalColor.globalCustomNavColor)
        tabBarController.tabBar.barTintColor = UIColor(hexString: GlobalColor.globalCustomNavColor)
        tabBarController.tabBar.tintColor = .black
        /// Remove the top black line
        tabBarController.tabBar.shadowImage = UIImage()
        tabBarController.tabBar.backgroundImage = UIImage()
        tabBarController.tabBar.layer.borderWidth = 0.0
        tabBarController.tabBar.layer.shadowColor = UIColor.gray.cgColor
        tabBarController.tabBar.layer.shadowOffset = CGSize(width: 1, height: 1)
        tabBarController.tabBar.layer.shadowOpacity = 0.6
        tabBarController.tabBar.layer.shadowRadius = 4
    }
}

extension UINavigationController {
    func removeTabbarItemsText() {
        self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
    }
}

//swiftlint:disable force_unwrapping
extension UITabBarController {
    func increaseBadge(indexOfTab: Int, num: String) {
        let tabItem = tabBar.items![indexOfTab]
        tabItem.badgeValue = num
    }
    
    func removeBadge(indexOfTab: Int) {
        let tabItem = tabBar.items![indexOfTab]
        tabItem.badgeValue = nil
        
    }
}
