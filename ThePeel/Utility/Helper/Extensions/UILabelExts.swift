//
//  UILabelExts.swift
//  ThePeel
//
//  Created by Gone on 1/25/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

extension UILabel {
    func addCharacterSpacing(kernValue: Double) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}
