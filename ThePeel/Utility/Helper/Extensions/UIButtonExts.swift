//
//  UIButtonExts.swift
//  ThePeel
//
//  Created by Gone on 1/26/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

//swiftlint:disable force_unwrapping
extension UIButton{
    func addTextSpacing(_ spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text)!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: (self.titleLabel?.text?.count)!))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
