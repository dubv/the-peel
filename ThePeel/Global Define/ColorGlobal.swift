//
//  ColorGlobal.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct GlobalColor {
    static let gray = "EDEDED"
    static let grayTableView = "F5F5F5"
    static let white = "FFFFFF"
    static let black = "000000"
    static let darknight = "150703"
    static let disableButton = "C6C6C6"
    static let lightDark = "A2A2A2"
    static let volumeBtnBG = "E1DDD6"
    static let speechBtn = "ADD9F4"
    static let globalCustomNavColor = "FCFCFC"
}
