//
//  Utilities.swift
//  ThePeel
//
//  Created by Gone on 2/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

enum UUToken {
    static var setToken: String?
    static var getToken: String? {
        return setToken
    }
}

enum UUID {
    ///storedTypeProperty
    static var uid: String?
    ///computedTypeProperty
    static var uuid: String? {
        return uid
    }
}

enum UUName {
    static var name: String?
    static var named: String? {
        return name
    }
}

public func moneyLocale(input: String) -> String {
    var result: String = ""
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    
    if let localeSecond = Float(input), let priceString = currencyFormatter.string(from: NSNumber(value: localeSecond)) {
        result = priceString
    }
    return result
}

public func moneyCalculateConvert(input: Double) -> String {
    var result: String = ""
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    
    let locale = String(input)
    if let localeSecond = Float(locale), let priceString = currencyFormatter.string(from: NSNumber(value: localeSecond)) {
        result = priceString
    }
   return result
}

public func userStatus() -> UInt {
    if UUID.uuid != nil {
        return 0
    }
    return 1
}

public func converTickToTimeslive(tickFromPass: UInt64) -> String{
    let ticks = Date().ticks
    let afterTicks = ticks - tickFromPass
    let microsecond = Double(afterTicks)*0.1
    let seconds = microsecond*pow(10, -6)
    let date = Date(timeIntervalSinceNow: -seconds)
    return date.getElapsedInterval()
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}
