//
//  TextGlobal.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct GlobalText {
    // MARK: - Networking
    static let TITLE_NO_INTERNET = "No Internet!"
    static let MESSAGE_NO_INTERNET = "Checking the network cables, modem, and router"
    
    // MARK: - Authentication
    static let TITLE_SIGNUP_FAILED = "Sign up failed!"
    static let TITLE_SIGNIN_FAILED = "Sign in failed!"
    static let TITLE_SIGNUP_FINISH = "Sign up finished!"
    static let TITLE_BUTTON_SIGNOUT = "Sign out"
    static let TITLE_SIGNOUT_FAILED = "Sign out failed!"
    static let TITLE_VERIFI_FAILED = "Verification failed!"
    static let TITLE_VERIFI_FINISHED = "Verification finished!"
    static let MESS_BUTTON_AUTH_CART = "Sign up to get started"
    static let TITLE_BUTTON_TO_ORDER = "To order"
    static let TITLE_BUTTON_TO_CHECKOUT = "To checkout"
    static let MESS_CHECK_USER_FAILED = "You need to signup to get started"
    static let TITLE_ACC = "Membership"
    static let MESS_WRONG_PASSWORD = "The password you entered is incorrect!"
    static let MESS_WRONG_PASSWORD_OR_PHONENUMBER = "The password or phone number you entered is incorrect!"
    static let MESS_SIGNIN_OVERFLOW = "We have blocked all requests from this device due to unusual activity. Try a hour later."
    
    // MARK: - Product
    static let TITLE_NO_PRODUCT = "Get products failed!"
    static let TITLE_NOT_AVAILABEL = "These products you are finding not availabel!"
    static let TITLE_AVAILABEL = "There are products you are looking for!"
    static let TITLE_NO_HEROIMAGE = "Get hero cover images failed!"
    static let TITLE_NO_DESSERT = "Get desserts failed!"
    static let TITLE_NO_DRINK = "Get drinks failed!"
    static let TITLE_NO_PIZZA = "Get pizza failed!"
    static let TITLE_COMMENT_FAILED = "Comment failed!"
    static let TITLE_BUTTON_VIEWALLCOMMENT = "View all comments"
    static let PLACEHOLDER_TEXTVIEW_COMMENT = "Write a comment"
    static let TITLE_MINIUM_ORDER = "Minimum order ₫20.000"
    static let TITLE_PAYMENT_FINISHED = "Thanks for your order!"
    static let MESS_PAYMENT_FINISHED = "Your order item's in-processing, please wait for the call."
    
    // MARK: - Cart
    static let TITLE_POPUP_PAYMENT_FAILED = "To payment failed!"
    static let TITLE_POPUP_ADDCART_FINISH = "Add product completed!"
    static let TITLE_POPUP_ADDCART_FAILED = "Add product failed!"
    static let TITLE_BUTTON_TOCART = "Go to cart"
    static let TITLE_POPUP_EMPTY_CART = "You need to order at least once item"
    static let TITLE_POPUP_EMPTY_NOT_AVAILABEL = "Item is not availabel!"
    static let TITLE_LABEL_PRODUCT_TOTAL = "Total to pay: "
    static let TITLE_SOMETHING_UPDATED = "Somethings was updated!"
    static let MESS_SOMETHING_UPDATED = "You have just updated an item"
    static let MESS_CART_MAXIMUN = "Sorry, up to 10 items of this product can be purchased per order"
    
    // MARK: - Home
    static let TITLE_POPUP_TOHOME = "Go to home"
    static let TITLE_SECTION_DESSERT = "DESSERT"
    static let TITLE_SECTION_DRINKS = "DRINKS"
    
    // MARK: - Profile
    static let TITLE_PROFILE = "Account overview "
    static let TITLE_PURCHASING = "Purchases from "
    static let MESS_PROFILE_EDITED = "You have just updated your profile"
    
    // MARK: - Global Error
    static let TITLE_ERROR_GLOBAL = "Something was wrong!"
    static let TITLE_SORRY_GLOBAL = "Sorry!"
    static let TITLE_MESS_GLOBAL = "This feature's in-progress development"
    
    // MARK: - Recogzie
    static let TITLE_ERROR_RECOGNIZE = "Recognizer Authorization!"
    static let MESS_RECOGNIZE_FAILED = "Unable to create an SFSpeechAudioBufferRecognitionRequest object"
    static let MESS_RECOGNIZE_DENIED = "User denied access to speech recognition"
    static let MESS_RECOGNIZE_RESTRICTED = "Speech recognition restricted on this device"
    static let MESS_RECOGNIZE_NOT_DETERMINED = "Speech recognition not yet authorized"
    static let MESS_REGCONIZE_TIPS = """
    You can search by categories, or compositions of Pizza and others. E.g "chicken", "olives", "tomato". For more details, you have also click on the helper icon below.
    """
    
    ///FirebaseError
    static let KEY_NOT_AVAILABEL = "Key not availabel!"
    
    // MARK: - Global Finished
    static let TITLE_FINISH_GLOBAL = "Finished!"
    
    // MARK: - Action Sheet
    static let TITLE_DELETE = "Delete"
    static let TITLE_REPLY = "Reply"
    static let TITLE_COPY = "Copy"
    static let TITLE_CANCEL = "Cancle"
    static let TITLE_HIDE = "Hide"
    static let TITLE_VIEW_DETAIL = "View Detail"
    static let TITLE_CHOOSE_IMAGE = "Choose Image"
    static let TITLE_CAMERA = "Camera"
    static let TITLE_GALLERY = "Gallery"
    static let MESS_NO_CAMERA = "You don't have camera."
    static let MESS_UPLOAD_RESTRICTED = "User do not have access to photo album."
    static let MESS_UPLOAD_DENIED = "User has denied the permission."
    
    // MARK: - Button
    static let TITLE_BUTTON_TRY = "Try again"
    static let TITLE_BUTTON_OKAY = "OK, I GOT IT"
    static let TITLE_BUTTON_RECOGNIZE_START = "Start Recording"
    static let TITLE_BUTTON_RECOGNIZE_STOP = "Stop Recording"
    static let TITLE_BUTTON_CHANGE_PROFILENAME = "Change Profile Name"
    static let TITLE_BUTTON_CHANGE_PASSWORD = "Change Password"
    
    // MARK: TextView
    static let TITLE_TEXTVIEW_RECOGNIZE = "Say something, we're listening!"
    static let TITLE_TEXTVIEW_START_RECOGNIZE = "What can I help you with?"
    
    // MARK: - PlaceHolder
    static let TITLE_PLACEHOLDER_NEW_PROFILENAME = "New Profile Name"
    static let TITLE_PLACEHOLDER_NEW_PASSWORD = "New Password"
    static let TITLE_PLACEHOLDER_OLD_PASSWORD = "Old Password"
    
    // MARK: - Handle product
    static let TITLE_NEW_PRODUCT = "New Product 🍕🍕🍕"
    static let TITLE_DELIVERY_STATES = "Delivered"
}
