//
//  Notification.swift
//  ThePeel
//
//  Created by Gone on 3/25/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let toHome = Notification.Name("home")
    static let toBlank = Notification.Name("blank")
    static let newProduct = Notification.Name("newProduct")
    static let toItemOrdered = Notification.Name("itemOrdered")
}
