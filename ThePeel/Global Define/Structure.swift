//
//  Structure.swift
//  ThePeel
//
//  Created by Gone on 2/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct Navigation {
    static let homeTitle = "THE PEEL"
    static let notifisTitle = "Notifications"
    static let cartTitle = "Cart"
    static let checkoutTitle = "Checkout"
    static let profileTitle = "Profile"
    static let myProfileTitle = "My profile"
    static let myOrderTitle = "My order"
}

struct NotificationForm {
    /// New product
    struct NewProduct {
        static let title = "New Product"
        static let content = "We have just launched a new product: "
    }
}

struct ProfileInfo {
    struct sectionZero {
        static let myOrder = "My order"
    }
    
    struct sectionFirst {
        static let myProfile = "My profile"
        static let reward = "The Peel rewards"
        static let payment = "Payment methods"
    }
    
    struct sectionSecond {
        static let myFavorite = "My favorite"
        static let history = "History"
        static let changePassword = "Change password"
    }
    
    struct sectionThird {
        static let needHelp = "Need help?"
        static let logout = "Log out"
    }
}

struct IconMenu {
    var iconImage: UIImage?
    var title: String?
}

struct Onboarding {
    var image: UIImage?
    var title: String
    var content: String
}

struct Setting {
    var title: String?
}

struct AccoutCell {
    var name: String
}

struct AlertView {
    var title: String
    var message: String
    var buttonTitle: String
}

struct ProfileCell {
    var largerTitle: String
    var detailTitle: String
}

struct Location {
    var x: Double
    var y: Double
}

struct ProductData<T> {
    var data = [T]()
    
    mutating func add(_ item: T) {
        data.append(item)
    }
    
    mutating func equal(_ item: [T]) {
        data = item
    }
    
    mutating func append(_ item: [T]) {
        data += item
    }
    
    mutating func remove(){
        return data.removeAll()
    }
}
