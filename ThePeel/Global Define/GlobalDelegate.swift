//
//  DelegateGlobal.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol BindingAlerDelegate: NSObjectProtocol {
    func binding(title: String, mess: String, titleButton: String)
}
