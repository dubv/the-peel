//
//  Constants.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    // MARK: Global
    static let en_US = "en-US"
    
    // MARK: - Networking
    /// Realtime DB
    static let heroimage = "heroimage"
    static let userPath = "user"
    static let adminPath = "admin"
    static let userNamePath = "userName"
    static let productPath = "product"
    static let userCartPath = "cart"
    static let pizzaPath = "pizza"
    static let drinkPath = "drink"
    static let dessertPath = "dessert"
    static let commentPath = "comment"
    static let checkoutPath = "checkouted"
    static let itemsCheckoutPath = "checkouted"
    static let itemsCheckoutChildPath = "items"
    static let likeCountPath = "likeCount"
    static let likeByUserPath = "likeByUser"
    static let notifisPath = "notifis"
    static let readPath = "read"
    static let userTokenPath = "user_token"
    
    static let sortByPricePath = "detail/description/price"
    static let sortByIdProductPath = "id"
    static let sortByCategoryPath = "category"
    static let sortByPasswordPath = "password"
    
    ///Storage DB
    static let avatarUserPath = "avatarUser"
    
    // MAKR: - Edit Profile De-TextTitleField
    static let avatarPath = "avatar"
    static let profileNameEF = "PROFILE NAME"
    static let profileNamePath = "userName"
    static let descriptionEF = "DESCRIPTION"
    static let descriptionsPath = "descriptions"
    static let externalLinkEF = "EXTERNAL LINK"
    static let externalLinkPath = "externalLink"
    static let phoneNumberEF = "PHONE NUMBER"
    static let phoneNumberPath = "phoneNumber"
    static let passwordEF = "PASSWORD"
    static let passwordPath = "password"
    static let emailEF = "EMAIL"
    static let emailPath = "email"
    static let lastNameEF = "LAST NAME"
    static let lastNamePath = "lastName"
    static let firstNameEF = "FIRST NAME"
    static let firstNamePath = "firstName"
    static let addressEF = "ADDRESS"
    static let addressPath = "address"
    static let userGlobalPath = "userglobal"
    
    // MARK: - Checkout
    static let shipTo = "SHIP TO"
    static let contactEmaiL = "CONTACT EMAIL"
    
    // MARK: - UIs
    static let weight = "Weight: "
    static let size = "Size: "
    
    // MARK: - Icons
    static let iconOrder = "iconOrder"
    static let signIn = "iconProfileChangePass"
    
    // MARK: - Date formater
    static let dateDefaultFormat = "yyyy'-'MM'-'dd'T'HH'"
    static let dateDetailFormat = "MMM d 'at' h:mm a"
    static let dateWeekFormat = "EEEE 'at' h:mm a"
    
    // MARK: - Fonts
    static let PFMedium = "PFDinTextCompPro-Medium"
    static let PFRegular = "PFDinTextCompPro-Regular"
    static let PFThin = "PFDinTextCompPro-Thin"
    static let PFLight = "PFDinTextCompPro-Light"
    static let ModeratRegular = "Moderat-Regular"
    static let ModerateBold = "Moderat-Bold"
    static let OpenSans = [1: "OpenSans-Light", 2: "OpenSans-Regular", 3: "OpenSans-SemiBold", 4: "OpenSans-Bold"]
    
    static let homeSectionTitle = ["CategoriesButton", "Pizza", "Dessert Category", "Dessert", "Drink Category", "Drink"]
    static let checkoutSectionTitle = ["Customer Info", "Items"]
    static let phoneNumber = "033912545"
    
    static let menuData: [IconMenu] = [ IconMenu(iconImage: #imageLiteral(resourceName: "iconMapLarge"), title: "Coverage Area"), IconMenu(iconImage: #imageLiteral(resourceName: "iconTelephone"), title: "(+84) 033 91 25 45"), IconMenu(iconImage: #imageLiteral(resourceName: "iconClock"), title: "10:00 - 22:30"), IconMenu(iconImage: #imageLiteral(resourceName: "iconPeel"), title: "Peel Voicing")]
    
    // MARK: - Categories
    static let leftCategories: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconCutter"), title: "All"), IconMenu(iconImage: #imageLiteral(resourceName: "sea"), title: "Sea"), IconMenu(iconImage: #imageLiteral(resourceName: "veggie"), title: "Fruit"), IconMenu(iconImage: #imageLiteral(resourceName: "sharp"), title: "Sharp"), IconMenu(iconImage: #imageLiteral(resourceName: "child"), title: "For Child"), IconMenu(iconImage: #imageLiteral(resourceName: "withoutOnion"), title: "Without Onion"), IconMenu(iconImage: #imageLiteral(resourceName: "withoutOlive"), title: "Without Olives")]
    
        static let leftCategoriesActive: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconCutter"), title: "All"), IconMenu(iconImage: #imageLiteral(resourceName: "iconSeaActive"), title: "Sea"), IconMenu(iconImage: #imageLiteral(resourceName: "iconVeggieActive"), title: "Fruit"), IconMenu(iconImage: #imageLiteral(resourceName: "sharpActive"), title: "Sharp"), IconMenu(iconImage: #imageLiteral(resourceName: "iconChildActive"), title: "For Child"), IconMenu(iconImage: #imageLiteral(resourceName: "iconWithoutOnionActive"), title: "Without Onion"), IconMenu(iconImage: #imageLiteral(resourceName: "iconWithoutOliveActive"), title: "Without Olives")]
    
    static let rightCategories: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "cheese"), title: "Cheese"), IconMenu(iconImage: #imageLiteral(resourceName: "new"), title: "New"), IconMenu(iconImage: #imageLiteral(resourceName: "meat"), title: "Chicken"), IconMenu(iconImage: #imageLiteral(resourceName: "lightWeight"), title: "Lightweight"), IconMenu(iconImage: #imageLiteral(resourceName: "noMushroom"), title: "No Mushroom"), IconMenu(iconImage: #imageLiteral(resourceName: "sweet"), title: "Sweet"), IconMenu(iconImage: #imageLiteral(resourceName: "iconOrther-1"), title: "Other")]
    
    static let rightCategoriesActive: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconCheeseActive"), title: "Cheese"), IconMenu(iconImage: #imageLiteral(resourceName: "newActive"), title: "New"), IconMenu(iconImage: #imageLiteral(resourceName: "iconMeatActive"), title: "Chicken"), IconMenu(iconImage: #imageLiteral(resourceName: "lightWeightActive"), title: "Lightweight"), IconMenu(iconImage: #imageLiteral(resourceName: "iconNoMushroonActive"), title: "No Mushroom"), IconMenu(iconImage: #imageLiteral(resourceName: "sweetActive"), title: "Sweet"), IconMenu(iconImage: #imageLiteral(resourceName: "iconOrtherActive"), title: "Other")]
    
    // MARK: - Setting
    static let setting: [Setting] = [Setting(title: "About PEEL"), Setting(title: "Preference"), Setting(title: "Privacy"), Setting(title: "Licensing"), Setting(title: "Support")]
    
    // MARK: - Profile
    static let profileSectionZero: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileOrder"), title: ProfileInfo.sectionZero.myOrder)]
    static let profileSectionFirst: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileDetail"), title: ProfileInfo.sectionFirst.myProfile), IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileRewards"), title: ProfileInfo.sectionFirst.reward), IconMenu(iconImage: #imageLiteral(resourceName: "iconProfilePayment"), title: ProfileInfo.sectionFirst.payment)]
    static let profileSectionSecond: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileFavorite"), title: ProfileInfo.sectionSecond.myFavorite), IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileHistory"), title: ProfileInfo.sectionSecond.history), IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileChangePass"), title: ProfileInfo.sectionSecond.changePassword)]
    static let profileSectionThird: [IconMenu] = [IconMenu(iconImage: #imageLiteral(resourceName: "iconHelp"), title: ProfileInfo.sectionThird.needHelp), IconMenu(iconImage: #imageLiteral(resourceName: "iconProfileLogout"), title: ProfileInfo.sectionThird.logout)]
    
    // MARK: - Onboarding
    static let onboardingData: [Onboarding] = [Onboarding(image: UIImage(named: "onboarding1"), title: "First Turorial", content: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt"), Onboarding(image: UIImage(named: "onboarding2"), title: "Second Turorial", content: "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam"), Onboarding(image: UIImage(named: "onboarding3"), title: "Third Turorial", content: "Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit")]
}
