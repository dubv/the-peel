//
//  Dictionary.swift
//  ThePeel
//
//  Created by Gone on 2/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

var orderItemStatus = [1: "Parcel Received",
                       2: "Shipped",
                       3: "Out for Delivery"]

var notificationsTopic = [1: "delivery",
                          2: "newProduct"]

var notificationsStatus = [1: "Delete",
                          2: "Archive"]

var notificationsColors = [1: "FF3100",
                           2: "00ACC9"]
