//
//  LocalService.swift
//  ThePeel
//
//  Created by Gone on 1/26/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct CountrySuffixes: Codable {
    var name: String
    var dial_code: String
    var code: String
    
    mutating func returns(name: String, dial_code: String, code: String) -> String {
        self.name = name
        self.dial_code = dial_code
        self.code = code
        return "\(code) - \(name) [\(dial_code)]"
    }
}

class LocalService: NSObject {
    
    static let shared = LocalService()
    private var filePath: String!
    
    private override init() {
       filePath = "SuffixesPhoneNumber"
    }
    
    func getOrRetrieve(_ completionBlock: @escaping ([CountrySuffixes]) -> Void) {
        guard let url = Bundle.main.url(forResource: filePath, withExtension: "json") else { return }
        do {
            let data = try Data(contentsOf: url)
            let jsonData = try JSONDecoder().decode([CountrySuffixes].self, from: data)
            completionBlock(jsonData)
        } catch {
            print("error:\(error)")
        }
    }
}
