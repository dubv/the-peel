//
//  DataManager.swift
//  ThePeel
//
//  Created by Gone on 1/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class DataManager: NSObject {
    
    // MARK: - Singleton pattern
    static let shared = DataManager()
    private var ref: Database!
    private var auth: Auth!
    private var handle: AuthStateDidChangeListenerHandle?
    
    public var baseAuth: Auth {
        return auth
    }
    
    private override init() {
        Auth.auth().languageCode = Constants.en_US
        /// Handle offline
        Database.database().isPersistenceEnabled = true
        ref = Database.database()
    }
    
    func addOrUpdateWithinAuth(idPath: String, completionBlock: @escaping (DatabaseReference) -> Void, fail: @escaping (String) -> Void) {
        checkCurrentUser(completionBlock: { uid in
            let userRef = self.ref.reference(withPath: idPath).child(uid)
            completionBlock(userRef)
        }, fail: {(error) in
            fail(error)
        })
    }
    
    func delete(idPath: String, idChildPath: String, completionBlock: @escaping (DatabaseReference?) -> Void, fail: @escaping (String) -> Void) {
        checkCurrentUser(completionBlock: { uid in
            let cartRef = self.ref.reference(withPath: idPath).child(uid).child(idChildPath)
            completionBlock(cartRef)
        }, fail: {(error) in
            fail(error)
        })
    }
    
    /// Do get verificationID in signin/siginup
    func verifyPhoneNumbers(phoneNumber: String, completionBlock: @escaping (String) -> Void, fail: ((NSError) -> Void)?) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil, completion: { (verificationID, error) in
            if let error = error {
                fail?(error as NSError)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            guard let verificationID = verificationID else { return }
            completionBlock(verificationID)
        })
    }
    
    /// Do verify
    func signInAndRetrieve(verificationID: String, verificationCode: String, completionBlock: @escaping (String) -> Void, fail: ((NSError) -> Void)?) {
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error{
                fail?(error as NSError)
                return
            }
            guard let user = authResult?.user else { return }
            completionBlock(user.uid)
        }
    }
    
    func checkCurrentUser(completionBlock: @escaping (String) -> Void, fail: @escaping (String) -> Void){
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if auth.currentUser != nil {
                guard let uid = user?.uid else {
                    fail(GlobalText.MESS_CHECK_USER_FAILED)
                    return}
                UUID.uid = uid  // Global uid
                completionBlock(uid)
            } else {
                fail(GlobalText.MESS_CHECK_USER_FAILED)
            }
        }
    }
    
    func getOrRetrieve(path: String, childPath: String? = nil, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: ((NSError) -> Void)?){
        if let childPath = childPath {
            ref.reference(withPath: path).child(childPath).observe(DataEventType.value, with: { snapshot in
                completionBlock(snapshot)
            })
        } else {
            ref.reference(withPath: path).observe(DataEventType.value, with: { snapshot in
                completionBlock(snapshot)
            })
        }
    }
    
    func getOrRetrieveNotObserve(path: String, childPath: String? = nil, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void){
        guard let childPath = childPath else { return }
        ref.reference(withPath: path).child(childPath).observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.exists() {
                completionBlock(snapshot)
            } else {
                fail(GlobalText.MESS_WRONG_PASSWORD_OR_PHONENUMBER)
            }
        })
    }
    
    func getOrRetrieveWithAuth(path: String, childPath: String, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void){
        checkCurrentUser(completionBlock: { uid in
            self.ref.reference(withPath: path).child(uid).child(childPath).observe(DataEventType.value, with: { snapshot in
                completionBlock(snapshot)
            })
        }, fail: {(error) in
            fail(error)
        })
    }

    ///Do get single object
    func queryObserve(path: String, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void) {
        checkCurrentUser(completionBlock: { uid in
            self.ref.reference(withPath: path).child(uid).observe(DataEventType.value, with: { (snapshot) in
                completionBlock(snapshot)
            })
        }, fail: {(error) in
            fail(error)
        })
    }
    
    func queryObserveWithoutUUId(path: String, childPath: String, key: String? = nil, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void) {
        if let key = key {
            self.ref.reference().child(path).child(childPath).child(key).observe(DataEventType.value, with: { (snapshot) in
                completionBlock(snapshot)
            }, withCancel: {(error) in
                print(error)
            })
        } else {
            self.ref.reference().child(path).child(childPath).observe(DataEventType.value, with: { (snapshot) in
                completionBlock(snapshot)
            }, withCancel: {(error) in
                print(error)
            })
        }
    }
    
    func queryObserveSingleEvent(path: String, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void) {
        checkCurrentUser(completionBlock: { uid in
            self.ref.reference().child(path).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                completionBlock(snapshot)
            })
        }, fail: {(error) in
            fail(error)
        })
    }
    
    func queryOrderedByKey(path: String, childPath: String, sortby keypath: String? = nil, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: ((NSError) -> Void)?) {
        guard let key = keypath else { return }
        ref.reference(withPath: path).child(childPath).queryOrdered(byChild: key).observe(.value, with: { snapshot in
                completionBlock(snapshot)
                print(snapshot)
        }, withCancel: {(error) in
            fail?(error as NSError)
        })
    }
    
    func queryOrderedByValue(path: String, childPath: String, sortby keypath: String? = nil, value: Any, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: ((NSError) -> Void)?) {
        guard let key = keypath else { return }
        ref.reference(withPath: path).child(childPath).queryOrdered(byChild: key).queryEqual(toValue: value).observe(DataEventType.value, with: { snapshot in
            if keypath != nil {
                completionBlock(snapshot)
            }
        })
    }
    
    func queryObserveOrderedByValue(path: String, childPath: String, sortby keypath: String? = nil, value: String, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: ((NSError) -> Void)?) {
        guard let key = keypath else { return }
        ref.reference(withPath: path).child(childPath).queryOrdered(byChild: key).queryEqual(toValue: value).observe(DataEventType.value, with: { snapshot in
            if keypath != nil {
                completionBlock(snapshot)
            }
        })
    }
        
    func uploadImage(_ image: UIImage, parentPath: String, childPath: String, path: String? = nil, completionBlock: @escaping (URL?) -> Void, fail: @escaping (String?) -> Void) {
        guard let path = path else { return }
        let storageRef = Storage.storage().reference(withPath: parentPath).child(childPath).child(path)
        let metaData = StorageMetadata()
        guard let imageData = image.jpegData(compressionQuality: 0.5) else { return }
        metaData.contentType = "image/jpg"
        
        storageRef.putData(imageData, metadata: metaData) { metaData, error in
            if error == nil, metaData != nil {
                storageRef.downloadURL { url, _ in
                    completionBlock(url)
                }
            } else {
                fail(nil)
            }
        }
    }
    
    func addOrUpdateWithTransaction(productPath: String, childPath: String, productId: String, reactPath: String, reactCountPath: String, completionBlock: @escaping () -> Void, fail: @escaping (String?) -> Void) {
        ref.reference(withPath: productPath).child(childPath).child(productId).runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var object = currentData.value as? [String : AnyObject], let uid = UUID.uuid {
                
                var likes: [String: Bool]
                likes = object[reactPath] as? [String : Bool] ?? [:]
                var likeCount = object[reactCountPath] as? Int ?? 0
                
                if let _ = likes[uid] {
                    // Unlike the product and remove self from likeCount
                    likeCount -= 1
                    likes.removeValue(forKey: uid)
                } else {
                    // Like the post and add self to likeCount
                    likeCount += 1
                    likes[uid] = true
                }
                object[reactCountPath] = likeCount as AnyObject?
                object[reactPath] = likes as AnyObject?
                
                // Set value and report transaction success
                currentData.value = object
                
                completionBlock()
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, _, _) in
            if let error = error {
                fail(error.localizedDescription)
            }
        }
    }
    
    func queryWithoutAuth(idPath: String, completionBlock: @escaping (DatabaseReference) -> Void) {
        let userRef = self.ref.reference(withPath: idPath)
        completionBlock(userRef)
    }
    
    func addOrUpdateNotSpecific(completionBlock: @escaping (Database) -> Void, fail: @escaping (String) -> Void) {
        handle = Auth.auth().addStateDidChangeListener { (auth, _) in
            if auth.currentUser != nil {
                guard let userRef = self.ref else { return }
                completionBlock(userRef)
            } else {
                fail(GlobalText.MESS_CHECK_USER_FAILED)
            }
        }
    }
}
