//
//  ThumProductViewController.swift
//  ThePeel
//
//  Created by Gone on 1/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ThumProductViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private weak var detailTableView: UITableView!
    
    // MARK: - Properties
    public var index: Int?
    public var pizzaData: Pizza?
    public var cartData = [Cart]()
    public var profileInfo: CustomUser?
    public var handlePassBackIndex: (() -> Void)?
    public var selectedIndexPath: IndexPath?
    private var cartPresenter: CartPresenter!
    private var profilePresenter: ProfilePresenter!
    private var homePresenter: HomePresenter!
    private var productDetailPresenter: ProductDetailPresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        initPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        callAPI()
        setupUI()
        handlePassBackIndex?()
    }
    
    // MARK: - Initialization
    static func viewController() -> ThumProductViewController? {
        return Helper.getViewController(named: "ThumProductViewController", inSb: "Product")
    }
    
    private func setupUI() {
        setupBackButton()
        view.backgroundColor = .white
    }
    
    fileprivate func initPresenter() {
        cartPresenter = CartPresenter()
        cartPresenter.cartDelegate = self
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
        productDetailPresenter = ProductDetailPresenter()
        productDetailPresenter.productDetailDelegate = self
    }
    
    // MARK: - Private Method
    fileprivate func registerCell() {
        detailTableView.register(ThumbTableViewCell.nib, forCellReuseIdentifier: ThumbTableViewCell.indentifier)
    }
    
    fileprivate func callAPI() {
        cartPresenter.getCartsBasedUserCurrent(true)
    }
    
    // MARK: - Public Method
    public func setValue(_ index: Int, _ data: Pizza) {
        self.index = index
        self.pizzaData = data
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ThumProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ThumbTableViewCell.indentifier, for: indexPath) as? ThumbTableViewCell else { let cell = ThumbTableViewCell(style: .default, reuseIdentifier: ThumbTableViewCell.indentifier)
            return cell }
        cell.data = pizzaData
        cell.addProductToCartDelegate = self
        cell.thumbTableViewCellDelegate = self
        cell.viewAllCommentDelegate = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - UITextViewDelegate, ViewAllCommentDelegate
extension ThumProductViewController: UITextViewDelegate, ViewAllCommentDelegate {
    func viewAllComment() {
        guard let commentVC = CommentViewController.viewController() else { return }
        commentVC.idPizza = self.pizzaData?.id
        self.present(commentVC, animated: true, completion: nil)
    }
}

// MARK: - ThumbTableViewCellDelegate, ProfileDelegate, PushComment, ProductDetailDelegate
extension ThumProductViewController: ThumbTableViewCellDelegate, ProfileDelegate, ProductDetailDelegate {
    func likeHandle() {
        guard let productId = pizzaData?.ref?.key else { return }
        if self.isNetworkAvailable() == true {
            if UUID.uuid != nil {
                productDetailPresenter.handleLikes(isCall: true, productId: productId)
            } else {
                showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
            }
        }
    }
    
    func likeFailed(_ mess: String) {
        self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func presentingCoverageAreaVC() {
        guard let coverageAreaVC = CoverageAreaViewController.viewController() else { return }
        self.presentVC(viewController: coverageAreaVC)
    }
    
    func getUserInfoFinished(info: CustomUser) {
        self.profileInfo = info
    }
    
    func getUserInfoFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
}

// MARK: - AddProductToCartDelegate, CartDelegate, HomeDelegate
extension ThumProductViewController: AddProductToCartDelegate, CartDelegate, HomeDelegate {
    func saveProduct(_ id: String, _ quanlity: String, _ favicon: String, name: String, price: String, size: String, desc: String) {
        let ticks = Date().ticks
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        let uidCart = String(ticks)
        let object = Cart(key: "", idItem: uidCart, idProduct: id, coverImage: favicon, price: price, size: size, quanlity: quanlity, desc: desc, dateTime: dateTime, userID: "", productName: name)
        if self.isNetworkAvailable() == true {
            UUID.uuid != nil ? checkCartAvailabel(object: object, favicon: favicon) : showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
    
    func getCartFinished(_ data: [Cart]) {
        self.cartData = data
    }
    
    func addOrUpdate(object: Cart, favicon: String) {
        self.setupPopupView(object: object, favicon: favicon)
    }
    
    func addOrUpdateFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func checkCartAvailabel(object: Cart, favicon: String) {
        if let index = self.cartData.firstIndex(where: {$0.idProduct == object.idProduct}) {
            object.idItem = self.cartData[index].idItem
            guard let currentCount = Int(self.cartData[index].quanlity) else { return }
            guard let newCount = Int(object.quanlity) else { return }
            let tempQuanlity = currentCount + newCount
            if tempQuanlity <= 10 {
                object.quanlity = String(tempQuanlity)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            } else {
                object.quanlity = String(currentCount)
                self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, GlobalText.MESS_CART_MAXIMUN, GlobalText.TITLE_BUTTON_OKAY)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            }
        } else {
            homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
        }
    }
}
