//
//  CoverImageCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class CoverImageCollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainImageView: UIView!
    @IBOutlet public weak var coverImageView: UIImageView!
    
    // MARK: - Properties
    var isZooming = false
    var originalImageCenter: CGPoint?
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .clear
        self.clipsToBounds = false
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pincher(sender:)))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(sender:)))
        pinch.delegate = self
        pan.delegate = self
        coverImageView.addGestureRecognizer(pinch)
        coverImageView.addGestureRecognizer(pan)
        coverImageView.isUserInteractionEnabled = true
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    @objc func pan(sender: UIPanGestureRecognizer) {
        if self.isZooming && sender.state == .began {
            self.originalImageCenter = sender.view?.center
        } else if self.isZooming && sender.state == .changed {
            let translation = sender.translation(in: mainContainImageView)
            if let view = sender.view {
                view.center = CGPoint(x:view.center.x + translation.x,
                                      y:view.center.y + translation.y)
            }
            sender.setTranslation(CGPoint.zero, in: self.coverImageView.superview)
        }
    }
    
    @objc func pincher(sender:UIPinchGestureRecognizer) {
        if sender.state == .began {
            let currentScale = self.coverImageView.frame.size.width / self.coverImageView.bounds.size.width
            let newScale = currentScale*sender.scale
            
            if newScale > 1 {
                self.isZooming = true
            }
        } else if sender.state == .changed {
            
            guard let view = sender.view else { return }
            
            let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
                                      y: sender.location(in: view).y - view.bounds.midY)
            let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                .scaledBy(x: sender.scale, y: sender.scale)
                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
            
            let currentScale = self.coverImageView.frame.size.width / self.coverImageView.bounds.size.width
            var newScale = currentScale*sender.scale
            
            if newScale < 1 {
                newScale = 1
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                self.coverImageView.transform = transform
                sender.scale = 1
            } else {
                view.transform = transform
                sender.scale = 1
            }
            
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            guard let center = self.originalImageCenter else { return }
            UIView.animate(withDuration: 0.3, animations: {
                self.coverImageView.transform = CGAffineTransform.identity
                self.coverImageView.center = center
                self.coverImageView.frame = CGRect(x: 0, y: 0, width: self.mainContainImageView.frame.width, height: self.mainContainImageView.frame.height)
            }, completion: { _ in
                self.isZooming = false
            })
        }
    }
}

extension CoverImageCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
