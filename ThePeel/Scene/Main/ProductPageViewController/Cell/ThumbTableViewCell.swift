//
//  ThumbTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

protocol ThumbTableViewCellDelegate: NSObjectProtocol {
    func presentingCoverageAreaVC()
    func likeHandle()
}

protocol ViewAllCommentDelegate: NSObjectProtocol {
    func viewAllComment()
}

class ThumbTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var coverImageCollectionView: UICollectionView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var compositionTextView: UITextView!
    @IBOutlet private weak var caloriesLabel: UILabel!
    @IBOutlet private weak var cabohudrateLabel: UILabel!
    @IBOutlet private weak var proteinsLabel: UILabel!
    @IBOutlet private weak var fatsLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var orderBtn: CustomAddToCartButton!
    @IBOutlet private weak var increaseBtn: UIButton!
    @IBOutlet private weak var decreaseBtn: UIButton!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var sizeLabel: UILabel!
    @IBOutlet private weak var coverageAreaBtn: CustomCoverageButton!
    @IBOutlet private weak var heroImageCountLabel: UILabel!
    /// UIs for comment
    @IBOutlet private weak var firstCommentLabel: UILabel!
    @IBOutlet private weak var secondCommentLabel: UILabel!
    @IBOutlet private weak var thirdCommentLabel: UILabel!
    @IBOutlet private weak var viewAllCommentBtn: UIButton!
    /// UIs for likes
    @IBOutlet private weak var likeButton: UIButton!
    @IBOutlet private weak var countLikeLabel: UILabel!
    @IBOutlet private weak var likeLargerImageView: UIImageView!
    // MARK: - Properties
    public var productCount = 1
    public var heroImages: [ProductImage] = []
    weak var addProductToCartDelegate: AddProductToCartDelegate?
    weak var thumbTableViewCellDelegate: ThumbTableViewCellDelegate?
    weak var viewAllCommentDelegate: ViewAllCommentDelegate?
    public var data: Pizza! {
        didSet {
            guard let data = data else { return }
            titleLabel.text = data.name
            compositionTextView.text = data.detail?.composition
            if let desc = data.detail?.description {
                priceLabel.text = moneyLocale(input: desc.price)
                weightLabel.text = Constants.weight + desc.weight
                sizeLabel.text = Constants.size + desc.size
            }
            if let desc = data.detail?.nutritional {
                caloriesLabel.text = desc.calories
                cabohudrateLabel.text = desc.carbohydrate
                proteinsLabel.text = desc.proteins
                fatsLabel.text = desc.fats
            }
            self.heroImages = data.image
            var arrListComment: [UILabel] = [firstCommentLabel, secondCommentLabel, thirdCommentLabel]
            data.comment.sort { a, b in
                a.key > b.key
            }
            for index in (0..<data.comment.count) where index < 3 {
                guard let userName = data.comment[index].userName, let comment = data.comment[index].commentContent else { return }
                let formattedString = NSMutableAttributedString()
                formattedString
                    .bold("\(userName) ")
                    .normal(comment)
                arrListComment[index].attributedText = formattedString
            }
            viewAllCommentBtn.setTitle("\(GlobalText.TITLE_BUTTON_VIEWALLCOMMENT) (\(String(data.comment.count))) ", for: .normal)
            guard let likeByUser = data.likeByUser else { return }
            handleLikeCount(count: String(data.likeCount), likeByUser: likeByUser)
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        let doubleTap = UITapGestureRecognizer(target: self, action:#selector(self.likeAction))
        doubleTap.numberOfTapsRequired = 2
        coverImageCollectionView.addGestureRecognizer(doubleTap)
        orderBtn.delegate = self
        setupUI()
        setupCoverImageView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    private func setupUI() {
        countLabel.layer.borderColor = UIColor.lightGray.cgColor
        orderBtn.layer.borderColor = UIColor.lightGray.cgColor
        increaseBtn.layer.borderColor = UIColor.lightGray.cgColor
        decreaseBtn.layer.borderColor = UIColor.lightGray.cgColor
        countLabel.layer.borderWidth = 0.6
        orderBtn.layer.borderWidth = 0.6
        increaseBtn.layer.borderWidth = 0.6
        decreaseBtn.layer.borderWidth = 0.6
        likeLargerImageView.isHidden = true
    }
    
    // MARK: - Private Method
    fileprivate func setupCoverImageView() {
        coverImageCollectionView.register(CoverImageCollectionViewCell.nib, forCellWithReuseIdentifier: CoverImageCollectionViewCell.indentifier)
        coverImageCollectionView.delegate = self
        coverImageCollectionView.dataSource = self
        
        guard let flowLayout = self.coverImageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width - 32, height: UIScreen.main.bounds.width - 32)
        flowLayout.minimumLineSpacing = 0
    }
    
    fileprivate func handleProductCount() {
        countLabel.text = String(productCount)
    }
    
    fileprivate func handleLikeCount(count: String, likeByUser: [String: Bool]? = nil) {
        countLikeLabel.text = count
        
        guard let uuid = UUID.uuid, let liked = likeByUser?[uuid] else {
            return
        }
        
        liked ? likeButton.setImage(UIImage(named: "iconLike"), for: .normal) : likeButton.setImage(UIImage(named: "iconUnlike"), for: .normal)
    }
    
    func animateButton() {
        likeButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        likeLargerImageView.isHidden = false
        likeLargerImageView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
            usingSpringWithDamping: 0.20,
            initialSpringVelocity: 6.0,
            options: .allowUserInteraction,
            animations: {
                self.likeButton.transform = .identity
                self.likeLargerImageView.transform = .identity
        }, completion: { _ in
                self.likeLargerImageView.isHidden = true
        })
    }
    
    // MARK: - IBAction
    @IBAction func coverageAreaButton(_ sender: Any) {
        thumbTableViewCellDelegate?.presentingCoverageAreaVC()
    }
    
    @IBAction func increaseButton(_ sender: Any) {
        if productCount < 10 {
            productCount += 1
        }
        handleProductCount()
    }
    
    @IBAction func decreaseButton(_ sender: Any) {
        if productCount >= 2 {
            productCount -= 1
        }
        handleProductCount()
    }
    
    @IBAction func viewAllCommentButton(_ sender: Any) {
        viewAllCommentDelegate?.viewAllComment()
    }
    
    @IBAction func likeButton(_ sender: Any) {
        thumbTableViewCellDelegate?.likeHandle()
        animateButton()
    }
    
    // MARK: - Target
    @objc func likeAction() {
        thumbTableViewCellDelegate?.likeHandle()
        animateButton()
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension ThumbTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heroImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoverImageCollectionViewCell.indentifier, for: indexPath)
        
        if let cell = cell as? CoverImageCollectionViewCell {
            if let url = URL(string: heroImages[indexPath.row].urlImg) {
                cell.coverImageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heroImageCountLabel.text = "\(indexPath.row+1)/\(heroImages.count)"
    }
    
}

// MARK: - AddToCartDelegate
extension ThumbTableViewCell: AddToCartButtonDelegate {
    func buttonIn() {
        guard let favicon = heroImages.first?.urlImg, let price = data.detail?.description?.price, let size = data.detail?.description?.size, let desc = data.detail?.composition else { return }
        addProductToCartDelegate?.saveProduct(data.id, String(self.productCount), favicon, name: data.name, price: price, size: size, desc: desc)
    }
}
