//
//  ProductViewController.swift
//  ThePeel
//
//  Created by Gone on 1/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ProductViewController: BaseViewController {
    // MARK: - Properties
    public var pageViewController: UIPageViewController!
    public var currentIndex: Int?
    public var pizzaData = ProductData<Pizza>()
    public var data: [Pizza?] = []
    private var pizzaFromNotifis: Pizza?
    private var indexFromNotifis: Int?
    fileprivate var homePresenter: HomePresenter!
    fileprivate var reloadDataObserve: (() -> ThumProductViewController)?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        setupBackButton()
        if isNetworkAvailable() == true {
            callAPI()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupUIs()
    }
    
    // MARK: - Initialization
    static func viewController() -> ProductViewController? {
        return Helper.getViewController(named: "ProductViewController", inSb: "Product")
    }
    
    fileprivate func setupUIs() {
        view.backgroundColor = .white
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(dismissViewWithGesture(sender:)))
        longGesture.minimumPressDuration = 0.2
        self.view.addGestureRecognizer(longGesture)
    }
    
    fileprivate func initPresenter() {
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
    }
    
    fileprivate func callAPI() {
        homePresenter.getAllPizza(isCall: true)
    }
    
    fileprivate func setupPageViewController() {
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.delegate = self
        pageViewController.dataSource = self
        self.addChild(pageViewController)
        view.addSubview(pageViewController.view)
        self.pageViewController?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.pageViewController?.didMove(toParent: self)
        guard let index = self.currentIndex else { return }
        pageViewController.setViewControllers([viewControllerAtIndex(index)], direction: .forward, animated: false, completion: nil)
        pageViewController.view.backgroundColor = .white
    }
    
    public func bindingFromNotifis(pizza: Pizza) {
        self.pizzaFromNotifis = pizza
    }
    
    // MARK: - Private Method
    private func viewControllerAtIndex(_ index: Int) -> ThumProductViewController {
        guard let thumVC = ThumProductViewController.viewController() else { return ThumProductViewController() }
        thumVC.setValue(index, self.pizzaData.data[index])
        thumVC.handlePassBackIndex = { () in
            self.currentIndex = thumVC.index
        }
        return thumVC
    }
    
    // MARK: - Target
    @objc func dismissViewWithGesture(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            
        }
    }
}

extension ProductViewController: HomeDelegate {
    func getAllPizza(pizza: [Pizza]) {
        self.pizzaData.equal(pizza)
        if self.pizzaFromNotifis != nil {
            let index = self.pizzaData.data.firstIndex(where: {$0.id == pizzaFromNotifis?.id})
            self.currentIndex = index
        }
        setupPageViewController()
    }
}

// MARK: - UIGestureRecognizerDelegate
extension ProductViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - UIPageViewControllerDelegate, UIPageViewControllerDataSource
extension ProductViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var index = (viewController as? ThumProductViewController)?.index else { return ThumProductViewController() }
        if index == 0 {
            return nil
        }
        index -= 1
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var index = (viewController as? ThumProductViewController)?.index else { return ThumProductViewController() }
        index += 1
        if index == self.pizzaData.data.count {
            return nil
        }
        return viewControllerAtIndex(index)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pizzaData.data.count
    }
}
