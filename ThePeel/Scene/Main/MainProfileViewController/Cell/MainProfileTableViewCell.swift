//
//  MainProfileTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class MainProfileTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var faviconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var data: IconMenu! {
        didSet {
            faviconImageView.image = data.iconImage
            titleLabel.text = data.title
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: indentifier, bundle: nil)
    }
}
