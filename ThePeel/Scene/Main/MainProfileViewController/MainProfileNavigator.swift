//
//  MainProfileNavigator.swift
//  ThePeel
//
//  Created by Gone on 3/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol MainProfileNavigator {
    func toMainProfile()
}

struct DefaultMainProfileNavigator: MainProfileNavigator {
    private weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toMainProfile() {
        guard let vc = MainProfileViewController.viewController() else { return }
        self.navigation?.pushViewController(vc, animated: true)
    }
}
