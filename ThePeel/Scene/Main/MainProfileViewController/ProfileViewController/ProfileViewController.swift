//
//  ProfileViewController.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Photos
import AudioToolbox
import MobileCoreServices
import Firebase

class ProfileViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet public weak var containCollectionView: UIView!
    @IBOutlet public weak var contentCollectionView: UICollectionView!
    
    // MARK: - Properties
    public var profilePresenter: ProfilePresenter!
    public var userInfo: CustomUser?
    public var imagePicker = UIImagePickerController()
    public var urlImage: String?
    public var profileDataRetrieve = [ProfileCell]()
    public var paymentData = [ItemPayment]()
    public var didChangeProfileVC: (() -> Void)?
    public var finishedButton = UIBarButtonItem()
    
    private var editProfilePresenter: EditProfilePresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPresenter()
        registerCell()
        imagePicker.delegate = self
        if isNetworkAvailable() == true {
            callAPI()
        }
        setupBackButton()
        navigationItem.title = Navigation.myProfileTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false
        setupLightNavigationBar()
        setupUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupCollectionViewFlow()
    }
    
    // MARK: - Initialization
    static func viewController() -> ProfileViewController? {
        return Helper.getViewController(named: "ProfileViewController", inSb: "Profile")
    }
    
    fileprivate func initPresenter() {
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
        
        editProfilePresenter = EditProfilePresenter()
        editProfilePresenter.editProfileDelegate = self
    }

    private func setupUI() {
        finishedButton = UIBarButtonItem(image: UIImage(named: "iconFinished"), style: .plain, target: self, action: #selector(finishedButotnAction))
        finishedButton.tintColor = .black
        navigationItem.rightBarButtonItem = finishedButton
    }
    
    public func setupCollectionViewFlow() {
        guard let flowLayout = self.contentCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.itemSize = CGSize(width: self.containCollectionView.bounds.width, height: self.containCollectionView.bounds.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
    }
    
    public func callAPI(){
        profilePresenter.getAllCheckoutedItem(isCall: true)
    }
    
    // MARK: - Private Method
    fileprivate func registerCell() {
        self.contentCollectionView.register(ProfileCollectionViewCell.nib, forCellWithReuseIdentifier: ProfileCollectionViewCell.indentifier)
    }
    
    // MARK: - Target
    @objc func finishedButotnAction() {
        for dataEdited in profileDataRetrieve where dataEdited.detailTitle != "" {
            if self.isNetworkAvailable() == true {
                updateData(data: dataEdited)
            }
        }
        self.popViewControler()
    }
}

// MARK: - ProfileDelegate, CustomProfileSegmentedControlDelegate, EditProfileDelegate
extension ProfileViewController: ProfileDelegate, EditProfileDelegate {

    func getCheckoutedItemFinished(item: [ItemPayment]) {
        self.paymentData = item
        self.contentCollectionView.reloadData()
    }

    func uploadAvatarImageFinish(urlImage: String) {
        self.showBlackLoadingSpinner()
        editProfilePresenter.editProfileInfo(true, childPath: Constants.avatarPath, value: urlImage)
    }
    
    func editProfileFinished(_ mess: String) {
        profilePresenter.getUserInfo(true)
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { _ in
            self.hideBlackLoadingSpinner()
        }
    }
    
    func getUserInfoFinished(info: CustomUser) {
        self.userInfo = info
        self.contentCollectionView.reloadData()
        
        for comments in info.comment {
            /// Update comment inside user table
            comments.ref?.updateChildValues([Constants.avatarPath: info.avatar])
            comments.ref?.updateChildValues([Constants.userNamePath: info.userName])
            
            /// Update comment inside product table
            guard let productPath = comments.keyProduct else { return }
            profilePresenter.updateUserAccessory(isCall: true, nameProductPath: productPath, idComment: comments.key, value: info)
        }
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileCollectionViewCell.indentifier, for: indexPath) as? ProfileCollectionViewCell else { return UICollectionViewCell()}
        cell.profileInfo = self.userInfo
        cell.driverDataDelegate = self
        
        cell.updateData = { [weak self] data in
            guard let `self` = self else { return }
            self.profileDataRetrieve = data
        }
        
        cell.handleError = { [weak self] bool in
            guard let `self` = self else { return }
            bool == 1 ? (self.finishedButton.isEnabled = false) : (self.finishedButton.isEnabled = true)
            bool == 0 ? (self.finishedButton.tintColor = .black) : (self.finishedButton.tintColor = UIColor(hexString: GlobalColor.gray))
        }
        return cell
    }
}

// MARK: - DriverDataDelegate
extension ProfileViewController: DriverDataDelegate {
    func presentingChangeProfileVC(needStringEdit: String) {
        guard let changeProfileVC = ChangeProfileViewController.viewController() else { return }
        
        if needStringEdit == Constants.profileNameEF {
            changeProfileVC.moveObject = needStringEdit
            changeProfileVC.oldProfileName = self.userInfo?.userName
        }
        
        changeProfileVC.didRetrieveUserData = { [weak self] in
            guard let `self` = self else { return }
            self.callAPI()
            self.profilePresenter.getUserInfo(true)
            self.didChangeProfileVC?()
        }
        
        self.pushViewController(viewController: changeProfileVC)
    }
    
    func passingImage() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            self.askForChooseImageType()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    self.askForChooseImageType()
                }
            })
        case .restricted:
            self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_UPLOAD_RESTRICTED, GlobalText.TITLE_BUTTON_OKAY)
        case .denied:
            self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_UPLOAD_DENIED, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            print(pickedImage)
            guard let uploadedImage = resizeImage(image: pickedImage, newWidth: 200) else { return }
            let ticks = Date().ticks
            dateFormatter.dateFormat = Constants.dateDetailFormat
            let uidImage = String(ticks)
            profilePresenter.uploadAvatarImage(true, image: uploadedImage, path: uidImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func askForChooseImageType() {
        let alert = UIAlertController(title: GlobalText.TITLE_CHOOSE_IMAGE, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: GlobalText.TITLE_CAMERA, style: .default, handler: { _ in
            self.openCamera() }))
        alert.addAction(UIAlertAction(title: GlobalText.TITLE_GALLERY, style: .default, handler: { _ in
            self.openGallary() }))
        alert.addAction(UIAlertAction.init(title: GlobalText.TITLE_CANCEL, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_NO_CAMERA, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true)
    }
}

extension ProfileViewController {
    public func updateData(data: ProfileCell) {
        switch data.largerTitle {
        case Constants.descriptionsPath:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.descriptionsPath, value: data.detailTitle)
        case Constants.addressEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.addressPath, value: data.detailTitle)
        case Constants.lastNameEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.lastNamePath, value: data.detailTitle)
        case Constants.firstNameEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.firstNamePath, value: data.detailTitle)
        case Constants.profileNameEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.profileNamePath, value: data.detailTitle)
        case Constants.emailEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.emailPath, value: data.detailTitle)
        case Constants.passwordEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.passwordPath, value: data.detailTitle)
        case Constants.phoneNumberEF:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.phoneNumberPath, value: data.detailTitle)
        default:
            self.editProfilePresenter.editProfileInfo(true, childPath: Constants.externalLinkPath, value: data.detailTitle)
        }
    }
}
