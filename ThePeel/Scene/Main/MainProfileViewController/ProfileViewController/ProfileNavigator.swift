//
//  ProfileNavigator.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol ProfileNavigator {
    func toProfile()
}

struct DefaultProfileNavigator: ProfileNavigator {
    private weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toProfile() {
        guard let vc = ProfileViewController.viewController() else {
            return
        }
        self.navigation?.pushViewController(vc, animated: true)
    }
}
