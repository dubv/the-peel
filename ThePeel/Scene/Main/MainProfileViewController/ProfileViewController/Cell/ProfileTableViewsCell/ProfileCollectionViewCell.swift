//
//  ContentCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol DriverDataDelegate: NSObjectProtocol {
    func passingImage()
    func presentingChangeProfileVC(needStringEdit: String)
}

class ProfileCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var contentTableView: UITableView!
    
    // MARK: - Properties
    weak var driverDataDelegate: DriverDataDelegate?
    public var userData = [ProfileCell]()
    public var updateData: (([ProfileCell]) -> Void)?
    public var handleError: ((Int) -> Void)?
    
    public var profileInfo: CustomUser? {
        didSet {
            guard let user = profileInfo else { return }
              userData = [ProfileCell(largerTitle: Constants.profileNameEF, detailTitle:  ""), ProfileCell(largerTitle: Constants.descriptionEF, detailTitle: user.descriptions), ProfileCell(largerTitle: Constants.externalLinkEF, detailTitle: user.externalLink), ProfileCell(largerTitle: Constants.firstNameEF, detailTitle: user.firstName), ProfileCell(largerTitle: Constants.lastNameEF, detailTitle: user.lastName),
            ProfileCell(largerTitle: Constants.emailEF, detailTitle: user.email),
            ProfileCell(largerTitle: Constants.phoneNumberEF, detailTitle: user.phoneNumber),
            ProfileCell(largerTitle: Constants.addressEF, detailTitle: user.address)]
            self.contentTableView.reloadData()
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTableView()
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.contentView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - Initialization
    private func setupTableView() {
        contentTableView.register(ProfileTableViewCell.nib, forCellReuseIdentifier: ProfileTableViewCell.indentifier)
        contentTableView.register(ProfileAvatarTableViewCell.nib, forCellReuseIdentifier: ProfileAvatarTableViewCell.indentifier)
        contentTableView.delegate = self
        contentTableView.dataSource = self
    }

    // MARK: - Target
    @objc func keyboardWillShow(notification: NSNotification )
    {
        guard let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt, let keyboardScreenEndFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        let keyboardViewEndFrame = self.contentView.convert(keyboardScreenEndFrame, from: contentView.window)
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
             self.contentTableView.contentInset = insets
        }, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.contentView.endEditing(true)
            self.contentTableView.contentInset.bottom = 0
        })
    }
}

// MARK: - UploadImageDelegate, PresentViewController
extension ProfileCollectionViewCell: UploadImageDelegate, PresentViewController {
    func uploading() {
        driverDataDelegate?.passingImage()
    }
    
    func presentingVC(needStringEdit: String) {
        driverDataDelegate?.presentingChangeProfileVC(needStringEdit: needStringEdit)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ProfileCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileAvatarTableViewCell.indentifier, for: indexPath) as? ProfileAvatarTableViewCell else { let cell = ProfileAvatarTableViewCell(style: .default, reuseIdentifier: ProfileAvatarTableViewCell.indentifier)
                return cell }
            cell.profiledInfo = profileInfo
            cell.uploadImageDelegate = self
            cell.presentViewDelegate = self
            return cell
        default :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.indentifier, for: indexPath) as? ProfileTableViewCell, indexPath.row < userData.count else { let cell = ProfileTableViewCell(style: .default, reuseIdentifier: ProfileTableViewCell.indentifier)
                return cell }
            cell.userData = userData[indexPath.row]
            
            cell.textChanged {[weak tableView] data in
                tableView?.beginUpdates() // the layout changes animated!
                tableView?.endUpdates()
                
                self.userData[indexPath.row] = data
                self.updateData?(self.userData)
            }
            
            if indexPath.row == 5 {
                cell.textChanged {[weak self] data in
                    guard let `self` = self else { return }

                    self.userData[indexPath.row] = data
                    self.updateData?(self.userData)
                    let mess = self.validation(data: data)
                    cell.handleUIChange(trigger: mess)
                    mess == 0 ? self.handleError?(mess) : self.handleError?(mess)
                }
            }
            
            if indexPath.row == 6 {
                cell.textChanged {[weak self] data in
                    guard let `self` = self else { return }
                    
                    self.userData[indexPath.row] = data
                    self.updateData?(self.userData)
                    let mess = self.validation(data: data)
                    cell.handleUIChange(trigger: mess)
                    mess == 0 ? self.handleError?(mess) : self.handleError?(mess)
                }
            }
            
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension ProfileCollectionViewCell {
    public func validation(data: ProfileCell) -> Int {
        var mess: Int!
        let reponseEmail = Validation.shared.validate(values: (ValidationType.email, data.detailTitle))
        
        let reponsePhoneNumber = Validation.shared.validate(values: (ValidationType.phoneNo, data.detailTitle))
        
        switch data.largerTitle {
        case Constants.emailEF:
            if !data.detailTitle.isEmpty && data.largerTitle == Constants.emailEF {
                switch reponseEmail {
                case .success:
                    mess = 0
                case .failure(_, let message):
                    print(message)
                    mess = 1
                }
            } else {
                return 1
            }
            
        default:
            if !data.detailTitle.isEmpty {
                switch reponsePhoneNumber {
                case .success:
                    mess = 0
                case .failure(_, let message):
                    print(message)
                    mess = 1
                }
            } else {
                return 1
            }
        }
        return mess
    }
}
