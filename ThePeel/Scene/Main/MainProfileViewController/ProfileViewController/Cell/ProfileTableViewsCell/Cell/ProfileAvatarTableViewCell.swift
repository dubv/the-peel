//
//  ProfileAvatarTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/17/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

protocol UploadImageDelegate: NSObjectProtocol {
    func uploading()
}

protocol PresentViewController: NSObjectProtocol {
    func presentingVC(needStringEdit: String)
}

class ProfileAvatarTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var profileNameLabel: UILabel!
    @IBOutlet private weak var changeButton: UIButton!
    @IBOutlet private weak var uploadButton: UIButton!
    @IBOutlet private weak var editButton: UIButton!
    
    // MARK: - Properties
    weak var uploadImageDelegate: UploadImageDelegate?
    weak var presentViewDelegate: PresentViewController?
    var profiledInfo: CustomUser! {
        didSet {
            guard let profile = profiledInfo else { return }
            profileNameLabel.text = profile.userName
            
            if profile.avatar != "" {
                uploadButton.isHidden = !editButton.isHidden
                guard let url = URL(string: profile.avatar) else { return }
                avatarImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            } else {
                editButton.isHidden = !uploadButton.isHidden
            }
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - IBAction
    @IBAction func changeButton(_ sender: Any) {
        presentViewDelegate?.presentingVC(needStringEdit: Constants.profileNameEF)
    }
    
    @IBAction func uploadButton(_ sender: Any) {
        uploadImageDelegate?.uploading()
    }
    
    @IBAction func editButton(_ sender: Any) {
        uploadImageDelegate?.uploading()
    }
}
