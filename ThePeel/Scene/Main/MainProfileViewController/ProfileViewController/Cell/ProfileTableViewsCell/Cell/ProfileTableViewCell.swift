//
//  ProfileTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var mainTitleLabel: UILabel!
    @IBOutlet private weak var childTitleTextView: UITextView!
    @IBOutlet private weak var errorNotifiButton: UIButton!
    
    // MARK: - Properties
    public var textChanged: ((ProfileCell) -> Void)?
    public var userData: ProfileCell? {
        didSet {
            guard let user = userData else { return }
            mainTitleLabel.text = user.largerTitle
            childTitleTextView.text = user.detailTitle
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        childTitleTextView.delegate = self
        childTitleTextView.setContentOffset(CGPoint.zero, animated: false)
        childTitleTextView.textContainerInset = .zero
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Initialization
    public func textChanged(action: @escaping (ProfileCell) -> Void) {
        self.textChanged = action
    }
    
    public func handleUIChange(trigger: Int) {
        trigger == 1 ? errorValidate(color: UIColor.red) : errorValidate(color: UIColor.white)
    }
    
    private func errorValidate(color: UIColor) {
        self.mainView.layer.borderColor = color.cgColor
        self.errorNotifiButton.setImage(UIImage(named: "iconTextViewError"), for: .normal)
        self.errorNotifiButton.tintColor = color
        self.mainView.layer.borderWidth = 1.0
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.userData?.detailTitle = textView.text
        guard let userData = self.userData else { return }
        self.textChanged?(userData)
    }
    
    // MARK: - IBAction
}

extension ProfileTableViewCell: UITextViewDelegate { }
