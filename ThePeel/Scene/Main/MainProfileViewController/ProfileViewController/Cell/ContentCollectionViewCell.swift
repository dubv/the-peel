//
//  ContentCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var contentTableView: UITableView!
    
    // MARK: - Properties
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTableView()
    }
    
    func test(viewColor: UIColor) {
        self.contentView.backgroundColor = viewColor
    }
    
    // MARK: - Initialization
    private func setupTableView() {
        contentTableView.register(ProfileTableViewCell.nib, forCellReuseIdentifier: ProfileTableViewCell.indentifier)
        
        contentTableView.register(PurchasingTableViewCell.nib, forCellReuseIdentifier: PurchasingTableViewCell.indentifier)
        
        contentTableView.delegate = self
        contentTableView.dataSource = self
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
}

extension ProfileCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.indentifier, for: indexPath) as? ProfileTableViewCell else { let cell = ProfileTableViewCell(style: .default, reuseIdentifier: ProfileTableViewCell.indentifier)
            return cell }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableView.automaticDimension
        return UITableView.automaticDimension
    }
}
