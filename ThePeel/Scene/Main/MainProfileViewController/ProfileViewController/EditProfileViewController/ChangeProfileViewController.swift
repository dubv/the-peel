//
//  EditPasswordViewController.swift
//  ThePeel
//
//  Created by Gone on 2/10/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ChangeProfileViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet public weak var oldValueTextField: CustomTextField!
    @IBOutlet private weak var validateOldValueLabel: UILabel!
    @IBOutlet private weak var newValueTextField: CustomTextField!
    @IBOutlet private weak var validateNewValueLabel: UILabel!
    @IBOutlet private weak var doneButton: CustomGlobalButton!
    
    // MARK: - Properties
    public var editProfilePresenter: EditProfilePresenter!
    public var moveObject: String!
    public var moveNoPhone: String!
    public var oldProfileName: String!
    public var didRetrieveUserData: (() -> Void)?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        setupUI()
        
        oldValueTextField.textField.text = oldProfileName
        setupBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false
        setupLightNavigationBar()
        
        switch moveObject {
        case Constants.profileNameEF:
            newValueTextField.textField.placeholder = GlobalText.TITLE_PLACEHOLDER_NEW_PROFILENAME
            doneButton.setTitle(GlobalText.TITLE_BUTTON_CHANGE_PROFILENAME, for: .normal)
        default:
            doneButton.setTitle(GlobalText.TITLE_BUTTON_CHANGE_PASSWORD, for: .normal)
            oldValueTextField.textField.placeholder = GlobalText.TITLE_PLACEHOLDER_OLD_PASSWORD
            newValueTextField.textField.placeholder = GlobalText.TITLE_PLACEHOLDER_NEW_PASSWORD
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        switch moveObject {
        case Constants.profileNameEF:
            validationProfileName()
        default:
            validationPassword()
        }
    }
    
    // MARK: - Initialization
    static func viewController() -> ChangeProfileViewController? {
        return Helper.getViewController(named: "ChangeProfileViewController", inSb: "Profile")
    }
    
    fileprivate func initPresenter() {
        editProfilePresenter = EditProfilePresenter()
        editProfilePresenter.editProfileDelegate = self
    }
    
    fileprivate func callAPI() {
        guard let oldValue = oldValueTextField.textField.text, let newValue = newValueTextField.textField.text else { return }
        moveObject == Constants.profileNameEF ? editProfilePresenter.editProfileInfo(true, childPath: Constants.profileNamePath, value: newValue) : editProfilePresenter.checkPassword(isCall: true, password: oldValue)
    }
    
    // MARK: - Private Method
    private func setupUI() {
        view.backgroundColor = .white
        newValueTextField.textField.delegate = self
    }
    
    // MARK: - IBAction
    @IBAction func doneButtonAction(_ sender: Any) {
        callAPI()
        didRetrieveUserData?()
        dismissKeyboard()
    }
    
    // MARK: - Target
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.endEditing(true)
        })
    }
}

extension ChangeProfileViewController: EditProfileDelegate {
    func verifyPasswordFinished(_ mess: String) {
        guard let newValue = self.newValueTextField.textField.text else { return }
        editProfilePresenter.editProfileInfo(true, childPath: Constants.passwordPath, value: newValue)
        editProfilePresenter.editGlobalUserInfo(true, path: Constants.userGlobalPath, childPath: moveNoPhone, value: newValue)
    }
    
    func verifyPasswordFailed(_ mess: String) {
        self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func editProfileFinished(_ mess: String) {
        self.showAlert(GlobalText.TITLE_FINISH_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
}

// MARK: - Validate TextField
extension ChangeProfileViewController {
    
    public func validationProfileName() {
        guard let newValueTextField = newValueTextField.textField.text else { return }
        
        let reponseNewValue = Validation.shared.validate(values: (ValidationType.alphabeticString, newValueTextField))
        
        if !newValueTextField.isEmpty {
            switch reponseNewValue {
            case .success:
                self.validateFinish(self.validateNewValueLabel)
                self.doneButton.buttonColorBG = GlobalColor.black
                self.doneButton.isEnabled = true
            case .failure(_, let message):
                self.doneButton.buttonColorBG = GlobalColor.gray
                self.doneButton.isEnabled = false
                self.validateFailed(message.localized(), self.validateNewValueLabel)
            }
        } else {
            self.validateFailed("", self.validateNewValueLabel)
            self.doneButton.buttonColorBG = GlobalColor.gray
            self.doneButton.isEnabled = false
        }
    }
    
    public func validationPassword() {
        guard let oldPasswordTF = oldValueTextField.textField.text, let newPasswordTF = newValueTextField.textField.text else { return }
        
        let reponseOldPassWord = Validation.shared.validate(values: (ValidationType.password, oldPasswordTF))
        let reponseNewPassWord = Validation.shared.validate(values: (ValidationType.password, newPasswordTF))
        let reponse = Validation.shared.validate(values: (ValidationType.password, oldPasswordTF), (ValidationType.password, newPasswordTF))
    
        switch reponse {
        case .success:
            self.doneButton.buttonColorBG = GlobalColor.black
            self.doneButton.isEnabled = true
        case .failure(_, let mess):
            print(mess)
            self.doneButton.buttonColorBG = GlobalColor.gray
            self.doneButton.isEnabled = false
        }
        
        if !oldPasswordTF.isEmpty {
            switch reponseOldPassWord {
            case .success:
                self.validateFinish(self.validateOldValueLabel)
            case .failure(_, let message):
                self.validateFailed(message.localized(), self.validateOldValueLabel)
            }
        } else {
            self.validateFailed("", self.validateOldValueLabel)
        }
        
        if !newPasswordTF.isEmpty {
            switch reponseNewPassWord {
            case .success:
                self.validateFinish(self.validateNewValueLabel)
            case .failure(_, let message):
                self.validateFailed(message.localized(), self.validateNewValueLabel)
            }
        } else {
            self.validateFailed("", self.validateNewValueLabel)
        }
    }
}
