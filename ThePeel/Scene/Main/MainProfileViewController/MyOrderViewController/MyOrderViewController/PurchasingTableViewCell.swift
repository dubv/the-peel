//
//  PurchasingTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol PurchasingTableViewCellDelegate: NSObjectProtocol {
    func handlePurchasingItem(time: UInt64, idCart: String, idItem: String)
}

class PurchasingTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContrainView: UIView!
    @IBOutlet private weak var productAvatarImageView: UIImageView!
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productStatusLabel: UILabel!
    @IBOutlet private weak var productPriceLabel: UILabel!
    @IBOutlet private weak var quanlityLabel: UILabel!
    @IBOutlet private weak var timesLiveLabel: UILabel!
    
    // MARK: - Properties
    weak var purchasingDelegate: PurchasingTableViewCellDelegate?
    
    public var cartPurchasesData: Cart! {
        didSet {
            guard let data = cartPurchasesData else { return }
            let url = URL(string: data.coverImage)
            productAvatarImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            productNameLabel.text = data.productName
            productPriceLabel.text = moneyLocale(input: data.price)
            
            guard let quanlity = Double(data.quanlity), let price = Double(data.price) else { return }
            let totalPayment = quanlity*price
            
            quanlity == 1 ? (quanlityLabel.text = "\(data.quanlity) item,  total:  \(moneyLocale(input: String(totalPayment)))") : (quanlityLabel.text = "\(data.quanlity) items,  total: \(moneyLocale(input: String(totalPayment)))")
            
            guard let cartTime = UInt64(data.idItem) else { return }
            timesLiveLabel.text = converTickToTimeslive(tickFromPass: cartTime)
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showActionSheet))
        mainContrainView.addGestureRecognizer(tapGesture)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    public func checkItemStatus(item: [ItemPayment]) {
        guard let index = item.firstIndex(where: {$0.idItem == self.cartPurchasesData.idItem}) else { return }
        self.productStatusLabel.text = item[index].status
    }
    
    // MARK: - IBAction
    @objc func showActionSheet() {
        let ticks = Date().ticks
        guard let cartTime = UInt64(cartPurchasesData.idItem) else { return }
        let afterTicks = ticks - cartTime
        purchasingDelegate?.handlePurchasingItem(time: afterTicks, idCart: cartPurchasesData.key, idItem: cartPurchasesData.idItem)
    }
}
