//
//  MyOrderViewController.swift
//  ThePeel
//
//  Created by Gone on 3/4/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class MyOrderViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var orderTableView: UITableView!
    
    // MARK: - Properties
    private var profilePresenter: ProfilePresenter!
    public var paymentData = [ItemPayment]()
    public var productStatus: String?
    public var cartDetail = [Cart]()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        setupLightNavigationBar()
        registerCell()
        initPresenter()
        callAPI()
        navigationItem.title = Navigation.myOrderTitle
    }
    
    // MARK: - Initialization
    static func viewController() -> MyOrderViewController? {
        return Helper.getViewController(named: "MyOrderViewController", inSb: "Profile")
    }
    
    private func registerCell() {
        orderTableView.register(PurchasingTableViewCell.nib, forCellReuseIdentifier: PurchasingTableViewCell.indentifier)
    }
    
    // MARK: - Private Method
    fileprivate func initPresenter() {
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
    }
    
    private func callAPI(){
        profilePresenter.getAllCheckoutedItem(isCall: true)
    }
    
    private func setupEmtyViews() {
        if cartDetail.isEmpty == true {
            orderTableView.tableFooterView = setupEmptyView()
        }
    }
}

extension MyOrderViewController: PurchasingTableViewCellDelegate, ProfileDelegate {
    func getCheckoutedItemFinished(item: [ItemPayment]) {
        self.paymentData = item
        
        var cartDummy = [Cart]()
        for purchases in item {
            for cart in purchases.cart {
                cartDummy.append(cart)
            }
        }
        cartDetail = cartDummy
        setupEmtyViews()
        orderTableView.reloadData()
    }
    
    func getCheckoutedItemFailed(mess: String) {
        setupEmtyViews()
    }
    
    func handlePurchasingItem(time: UInt64, idCart: String, idItem: String) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let viewDetailAction = UIAlertAction(title: GlobalText.TITLE_VIEW_DETAIL, style: .default, handler: nil)
        let deleteAction = UIAlertAction(title: GlobalText.TITLE_DELETE, style: .destructive) { _ -> Void in
            if let cartIndex = self.cartDetail.firstIndex(where: {$0.key == idCart}) {
                self.cartDetail[cartIndex].ref?.removeValue()
                self.cartDetail.remove(at: cartIndex)
                self.orderTableView.reloadData()
                self.setupEmtyViews()
                
//                if let itemIndex = self.paymentData.firstIndex(where: { $0.idItem == idItem}) {
//                    if self.paymentData[itemIndex].cart.isEmpty {
//                        self.paymentData[itemIndex].ref?.removeValue()
//                    }
//                }
            }
        }
        
        let cancelAction = UIAlertAction(title: GlobalText.TITLE_CANCEL, style: .cancel, handler: nil)
        
        // compare with 1 hour
        if time < UInt64(3.6*pow(10, 9)/0.1) {
            actionSheet.addAction(viewDetailAction)
            actionSheet.addAction(deleteAction)
            actionSheet.addAction(cancelAction)
        } else {
            actionSheet.addAction(viewDetailAction)
            actionSheet.addAction(cancelAction)
        }
        self.present(actionSheet, animated: true)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MyOrderViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PurchasingTableViewCell.indentifier, for: indexPath) as? PurchasingTableViewCell else { let cell = PurchasingTableViewCell(style: .default, reuseIdentifier: PurchasingTableViewCell.indentifier)
            return cell }
        cell.cartPurchasesData = self.cartDetail[indexPath.row]
        cell.checkItemStatus(item: self.paymentData)
        cell.purchasingDelegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
