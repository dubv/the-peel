//
//  MainProfileViewController.swift
//  ThePeel
//
//  Created by Gone on 3/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class MainProfileViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private var profileHeaderView: UIView!
    @IBOutlet public weak var profileTableView: UITableView!
    @IBOutlet private weak var coverContainView: UIView!
    @IBOutlet private weak var coverImageView: UIImageView!
    @IBOutlet private weak var avatarContainView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var sayHiLabel: UILabel!
    @IBOutlet private weak var profileNameLabel: UILabel!
    
    // MARK: - Properties
    private var profilePresenter: ProfilePresenter!
    public var userInfo: CustomUser?
    public var nonUserView: EmptyUserView!
    private let mainColor = UIColor(hexString: "F7F7F7")

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        registerCell()
        setupUI()
        callAPI()
        configNewNavigationUIs()
    }
    
    // MARK: - Initialization
    static func viewController() -> MainProfileViewController? {
        return Helper.getViewController(named: "MainProfileViewController", inSb: "Profile")
    }
    
    private func setupUI() {
        guard isViewLoaded else { return }
        
        profileTableView.tableHeaderView = profileHeaderView
        avatarContainView.layoutIfNeeded()
        avatarContainView.layer.cornerRadius = avatarContainView.frame.width/2
        avatarContainView.layer.borderWidth = 1
        avatarContainView.layer.borderColor = UIColor.white.cgColor
        
        nonUserView = EmptyUserView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        mainContainView.addSubview(nonUserView)
        
        profileTableView.isHidden = true
        nonUserView.isHidden = true
        navigationItem.title = Navigation.profileTitle
    }
    
    private func configNewNavigationUIs() {
        let navigationBarAppearance = self.navigationController?.navigationBar
        navigationBarAppearance?.barTintColor = mainColor
        
        navigationController?.toolbar.barTintColor = mainColor
        navigationController?.toolbar.isTranslucent = false
        profileTableView.backgroundColor = mainColor
    }
    
    public func binding() {
        profileNameLabel.text = userInfo?.userName
        guard let avatarImg = userInfo?.avatar, let url = URL(string: avatarImg) else { return }
        avatarImageView.kf.setImage(with: url, options: [.transition(.fade(0.1))])
    }
    
    // MARK: - Private Method
    private func registerCell() {
        profileTableView.register(MainProfileTableViewCell.nib, forCellReuseIdentifier: MainProfileTableViewCell.indentifier)
    }
    
    private func initPresenter() {
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
    }
    
    private func callAPI() {
        showBlackLoadingSpinner()
        profilePresenter.getUserInfo(true)
    }
}

extension MainProfileViewController: ProfileDelegate, HandleAuthenticationDelegate {
    func signIn() {
        self.showSignInAlert()
    }
    
    func signUp() {
        guard let registerVC = RegisterViewController.viewController() else { return }
        self.present(registerVC, animated: true)
    }

    func getUserInfoFinished(info: CustomUser) {
        self.userInfo = info
        hideBlackLoadingSpinner()
        profileTableView.reloadData()
        binding()
        nonUserView.isHidden = true
        profileTableView.isHidden = false
    }
    
    func getUserInfoFailed(mess: String) {
        hideBlackLoadingSpinner()
        profileTableView.isHidden = true
        nonUserView.isHidden = false
        nonUserView.handleAuthenticationDelegate = self
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MainProfileViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return Constants.profileSectionZero.count
        case 1:
            return Constants.profileSectionFirst.count
        case 2:
            return Constants.profileSectionSecond.count
        default:
            return Constants.profileSectionThird.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MainProfileTableViewCell.indentifier, for: indexPath) as? MainProfileTableViewCell else { let cell = MainProfileTableViewCell(style: .default, reuseIdentifier: MainProfileTableViewCell.indentifier)
            return cell }
        cell.selectionStyle = .none
        switch indexPath.section {
        case 0:
            cell.data = Constants.profileSectionZero[indexPath.row]
        case 1:
            cell.data = Constants.profileSectionFirst[indexPath.row]
        case 2:
            cell.data = Constants.profileSectionSecond[indexPath.row]
        default:
             cell.data = Constants.profileSectionThird[indexPath.row]
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0, indexPath.row == 0 {
            guard let myOrderVC = MyOrderViewController.viewController() else { return }
            self.pushViewController(viewController: myOrderVC)
        }
        
        if indexPath.section == 1, indexPath.row == 0 {
            guard let profileVC = ProfileViewController.viewController() else { return }
            profileVC.userInfo = self.userInfo
            
            profileVC.didChangeProfileVC = { [weak self] in
                guard let `self` = self else { return }
                self.profilePresenter.getUserInfo(true)
            }
            self.pushViewController(viewController: profileVC)
        }
        
        if indexPath.section == 2 {
            if indexPath.row == 2 {
                guard let changeProfileVC = ChangeProfileViewController.viewController() else { return }
                changeProfileVC.moveObject = Constants.passwordEF
                changeProfileVC.moveNoPhone = userInfo?.phoneNumber
                changeProfileVC.didRetrieveUserData = { [weak self] in
                    guard let `self` = self else { return }
                    self.callAPI()
                }
                self.pushViewController(viewController: changeProfileVC)
            }
        }
        
        if indexPath.section == 3, indexPath.row == 1 {
                self.showLogoutAlert()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 3 ? 0 : 13.5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
