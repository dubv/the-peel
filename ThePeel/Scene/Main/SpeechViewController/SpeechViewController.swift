//
//  SpeechViewController.swift
//  ThePeel
//
//  Created by Gone on 2/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Speech
import AVFoundation
import Pulsator

//swiftlint:disable force_unwrapping
class SpeechViewController: BaseViewController, SFSpeechRecognizerDelegate {
    // MARK: - IBOutlet
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var microphoneTapped: UIButton!
    @IBOutlet private weak var mainMicrophoneView: UIView!
    ///Response UIs
    @IBOutlet private weak var tapToEditButton: UIButton!
    @IBOutlet private weak var tipsLabel: UILabel!
    @IBOutlet private weak var tipsTextView: UITextView!
    @IBOutlet private weak var iconHelp: UIButton!
    ///Handle the timer
    fileprivate var timer: Timer?
    fileprivate var lastString: String?
    // MARK: - Properties
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: Constants.en_US))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    private let pulsator = Pulsator()
    public var bubbleSound: SystemSoundID!
    public var mostRecentlyProcessedSegmentDuration: TimeInterval = 0
    public var resultText: String?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        microphoneTapped.isEnabled = false
        speechRecognizer.delegate = self
        baseViewControllerDelegate = self
        mainMicrophoneView.layer.superlayer?.insertSublayer(pulsator, below: mainMicrophoneView.layer)
        tapToEditButton.isHidden = true
        setupAnimateButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUIs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        showTutorial()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        setupPeelerceButton()
        pulsator.position = mainMicrophoneView.layer.position
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        pulsator.stop()
        resultText = nil
        hideWhiteLoadingSpinner()
        
        audioEngine.stop()
        recognitionRequest?.endAudio()
        
        timer = nil
        lastString = nil
    }
    
    // MARK: - Initialization
    static func viewController() -> SpeechViewController? {
        return Helper.getViewController(named: "SpeechViewController", inSb: "Speech")
    }
    
    fileprivate func setupUIs() {
        setupDismissButton(dismissButton)
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
        tipsLabel.text = "Tips,"
        tipsTextView.text = GlobalText.MESS_REGCONIZE_TIPS
    }
    
    public func setupPeelerceButton() {
        pulsator.numPulse = 6
        pulsator.radius = 300.0
        pulsator.backgroundColor = UIColor(hexString: GlobalColor.speechBtn).cgColor
        pulsator.animationDuration = 4
    }
    
    public func setupAnimateButton() {
        microphoneTapped.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 6.0, options: .allowUserInteraction, animations: {
                self.microphoneTapped.transform = .identity
        }, completion: { _ in
                self.setupAnimateButton()
        })
    }
    
    // MARK: - Private Method
    fileprivate func startRecording() {
        if recognitionTask != nil {
            mostRecentlyProcessedSegmentDuration = 0
            recognitionTask?.cancel()
            recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
        do {
            if #available(iOS 11.0, *) {
                try audioSession.setCategory(.record, mode: .voiceChat, policy: .default, options: .defaultToSpeaker)
            } else {
                try audioSession.setCategory(.record, mode: .voiceChat, options: .defaultToSpeaker)
            }
            
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            
        } catch { }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else {
            return self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_RECOGNIZE_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            if result != nil {

                UIView.animate(withDuration: 1.2, animations: {
                    self.titleLabel.text = result?.bestTranscription.formattedString
                    self.tapToEditButton.setTitle("Tap to Edit", for: .normal)
                    self.tapToEditButton.isHidden = false
                })
                
                isFinal = (result?.isFinal)!
                self.resultText = result?.bestTranscription.formattedString
                self.lastString = self.resultText
                self.createTimerTimer(2)
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.timer?.invalidate()
                self.timer = nil
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.microphoneTapped.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {(buffer, _) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch { }
        
        titleLabel.text = GlobalText.TITLE_TEXTVIEW_RECOGNIZE
    }

    fileprivate func createTimerTimer(_ interval: Double) {
        OperationQueue.main.addOperation({[unowned self] in
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: false) { (_) in
                self.timer?.invalidate()
                if let strings = self.lastString?.count, strings > 0 {
                    self.toResultVC(resultString: self.resultText)
                }
            }
        })
    }
    
    fileprivate func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            microphoneTapped.isEnabled = true
        } else {
            microphoneTapped.isEnabled = false
        }
    }
    
    fileprivate func toResultVC(resultString: String?) {
        guard let resultVC = ProductResultViewController.viewController() else { return }
        resultVC.resultKeyString = resultString
        self.showWhiteLoadingSpinner()
        if self.isNetworkAvailable() == true {
            if resultString != nil {
                self.present(resultVC, animated: true, completion: nil)
            }
            self.hideWhiteLoadingSpinner()
        }
    }
    
    fileprivate func setupRecogAuth() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var isButtonEnabled = false
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
            case .denied:
                isButtonEnabled = false
                self.showAlert(GlobalText.TITLE_ERROR_RECOGNIZE, GlobalText.MESS_RECOGNIZE_DENIED, GlobalText.TITLE_BUTTON_OKAY)
            case .restricted:
                isButtonEnabled = false
                self.showAlert(GlobalText.TITLE_ERROR_RECOGNIZE, GlobalText.MESS_RECOGNIZE_RESTRICTED, GlobalText.TITLE_BUTTON_OKAY)
            case .notDetermined:
                isButtonEnabled = false
                self.showAlert(GlobalText.TITLE_ERROR_RECOGNIZE, GlobalText.MESS_RECOGNIZE_DENIED, GlobalText.MESS_RECOGNIZE_NOT_DETERMINED)
            }
            OperationQueue.main.addOperation() {
                self.microphoneTapped.isEnabled = isButtonEnabled
            }
        }
    }
    
    // MARK: - Public Method
    public func checkAudioEngine() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            timer?.invalidate()
            timer = nil

            pulsator.stop()
            microphoneTapped.isEnabled = false
            titleLabel.text = GlobalText.TITLE_TEXTVIEW_START_RECOGNIZE
            resultText = nil
        } else {
            startRecording()
            pulsator.start()
            self.lastString = nil
        }
    }
    
    fileprivate func showHelperView() {
        guard let categoryVC = SpeechCategoryViewController.viewController() else { return }
        categoryVC.modalPresentationStyle = .overFullScreen
        self.present(categoryVC, animated: true, completion: nil)
    }
    
    fileprivate func showTutorial() {
        if !UserDefaults.standard.bool(forKey: "k_SpeechTutorial") {
            self.showAlert("Hi, \(UUName.named ?? "")", "Welcome to The Peelerce, you can read the \("Tips") before starting, enjoy !", GlobalText.TITLE_BUTTON_OKAY)
            UserDefaults.standard.set(true, forKey: "k_SpeechTutorial")
        } else {
            setupRecogAuth()
        }
    }
    
    // MARK: - Target

    // MARK: - IBAction
    @IBAction func microphoneTapped(_ sender: Any) {
        checkAudioEngine()
    }
    
    @IBAction func iconHelper(_ sender: Any) {
        showHelperView()
    }
    
    @IBAction func tapToEditButton(_ sender: Any) {
        toResultVC(resultString: self.titleLabel.text)
    }
}

extension SpeechViewController: BaseViewControllerDelegate {
    func okayButtonFromAlertView() {
        setupRecogAuth()
    }
}
