//
//  ProductResultViewController.swift
//  ThePeel
//
//  Created by Gone on 2/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import AVFoundation

class ProductResultViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var productTableView: UITableView!
    
    // MARK: - Properties
    public var resultKeyString: String?
    public var resultCategoryString: String?
    public var pizzaData = ProductData<Pizza>()
    public let sectionView = UIView()
    public let searchField = CustomSearchTextField()
    public var cartData = [Cart]()
    public var songPlayer = AVAudioPlayer()
    private var categoryPresenter: CategoryPresenter!
    private var cartPresenter: CartPresenter!
    private var homePresenter: HomePresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDismissButton(dismissButton)
        
        initPresenter()
        registerCell()
        prepareSongAndSession()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        callAPI(resultKey: resultKeyString, resultCategory: resultCategoryString)
        setupUIs()
    }
    
    // MARK: - Initialization
    static func viewController() -> ProductResultViewController? {
        return Helper.getViewController(named: "ProductResultViewController", inSb: "Speech")
    }
    
    fileprivate func registerCell() {
        productTableView.register(PizzaTableViewCell.nib, forCellReuseIdentifier: PizzaTableViewCell.indentifier)
    }
    
    func prepareSongAndSession() {
        do {
            songPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "peelerce", ofType: "mp3")!))
            songPlayer.prepareToPlay()
            
            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setCategory(.playback, mode: .default, options: .defaultToSpeaker)
                try audioSession.setActive(true)
            } catch let sessionError {
                
                print(sessionError)
            }
        } catch let songPlayerError {
            print(songPlayerError)
        }
    }
    
    fileprivate func setupUIs() {
        searchField.textField.delegate = self
        searchField.searchTextFieldDelegate = self
        dismissButton.addTarget(self, action: #selector(customDismissVC), for: .touchUpInside)
    }
    
    private func initPresenter() {
        categoryPresenter = CategoryPresenter()
        categoryPresenter.categoryDelegate = self
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
        cartPresenter = CartPresenter()
        cartPresenter.cartDelegate = self
    }
    
    // MARK: - Private Method
    private func callAPI(resultKey: String?, resultCategory: String?) {
        self.showBlackLoadingSpinner()
        
        if let resultKey = resultKey {
            searchField.textField.text = resultKey
            categoryPresenter.queryByComposition(isCall: true, value: resultKey)
        }
        
        if let resultCategory = resultCategory {
            searchField.textField.text = resultCategory
            categoryPresenter.queryOrderedCategory(isCall: true, sortBy: Constants.sortByCategoryPath, value: resultCategory)
        }
        cartPresenter.getCartsBasedUserCurrent(true)
    }
    
    // MARK: - Text to Speech
    public func textToSpeech(transcription: String) {
        let utterance = AVSpeechUtterance(string: transcription)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.5
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
    
    // MARK: - Target
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.searchField.textField.resignFirstResponder()
        })
    }
    
    @objc func customDismissVC() {
        self.view.endEditing(true)
        self.dismissVCNormal()
    }
}

// MARK: - CategoryDelegate, AddProductToCartDelegate, SearchTextFieldDelegate
extension ProductResultViewController: CategoryDelegate, AddProductToCartDelegate, SearchTextFieldDelegate {
    func saveProduct(_ id: String, _ quanlity: String, _ favicon: String, name: String, price: String, size: String, desc: String) {
        let ticks = Date().ticks
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        let uidCart = String(ticks)
        let object = Cart(key: "", idItem: uidCart, idProduct: id, coverImage: favicon, price: price, size: size, quanlity: quanlity, desc: desc, dateTime: dateTime, userID: "", productName: name)
        
        if self.isNetworkAvailable() == true {
            UUID.uuid != nil ? checkCartAvailabel(object: object, favicon: favicon) : showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
    
    func getDataSortCategoryFinished(_ pizza: [Pizza]) {
        if pizza.isEmpty {
            textToSpeech(transcription: GlobalText.TITLE_SORRY_GLOBAL + GlobalText.TITLE_NOT_AVAILABEL)
            self.showAlert(GlobalText.TITLE_SORRY_GLOBAL, GlobalText.TITLE_NOT_AVAILABEL, GlobalText.TITLE_BUTTON_OKAY)
        } else {
            self.songPlayer.play()
            
            textToSpeech(transcription: "Hi \(UUName.named ?? ""), \(GlobalText.TITLE_AVAILABEL)")
            pizzaData.equal(pizza)
            productTableView.reloadData()
        }
        self.hideBlackLoadingSpinner()
    }
    
    func getDataSortCategoryFailed(_ mess: String) {
        self.showAlert(GlobalText.TITLE_NO_PRODUCT, mess, GlobalText.TITLE_BUTTON_OKAY)
        self.hideBlackLoadingSpinner()
    }
    
    func searching(searchKey: String) {
        categoryPresenter.queryByComposition(isCall: true, value: searchKey)
        dismissKeyboard()
    }
}

// MARK: - CartDelegate, HomeDelegate
extension ProductResultViewController: CartDelegate, HomeDelegate {
    
    func addOrUpdate(object: Cart, favicon: String) {
        self.setupPopupView(object: object, favicon: favicon)
        textToSpeech(transcription: "Thank \(UUName.named ?? ""), You have successfully ordered, Product name \(object.productName), Quanlity: \(object.quanlity)")
    }
    
    func addOrUpdateFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, mess, GlobalText.TITLE_BUTTON_OKAY)
        textToSpeech(transcription: "Sorry \(UUName.named ?? ""), \(mess)")
    }
    
    func getCartFinished(_ data: [Cart]) {
        self.cartData = data
    }
    
    func checkCartAvailabel(object: Cart, favicon: String) {
        if let index = self.cartData.firstIndex(where: {$0.idProduct == object.idProduct}) {
            object.idItem = self.cartData[index].idItem
            guard let currentCount = Int(self.cartData[index].quanlity) else { return }
            guard let newCount = Int(object.quanlity) else { return }
            let tempQuanlity = currentCount + newCount
            if tempQuanlity <= 10 {
                object.quanlity = String(tempQuanlity)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            } else {
                object.quanlity = String(currentCount)
                self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, GlobalText.MESS_CART_MAXIMUN, GlobalText.TITLE_BUTTON_OKAY)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            }
        } else {
            homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ProductResultViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pizzaData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PizzaTableViewCell.indentifier, for: indexPath) as? PizzaTableViewCell else { let cell = PizzaTableViewCell(style: .default, reuseIdentifier: PizzaTableViewCell.indentifier)
            return cell }
        
        let rawValue = self.pizzaData.data[indexPath.row]
        cell.pizza = rawValue
        cell.addProductToCartDelegate = self
        
        cell.didIncreaseVolume = { [weak self]  in
            self?.pizzaData.data[indexPath.row].counts = rawValue.counts + 1
        }
        
        cell.didDecreaseVolume = { [weak self]  in
            self?.pizzaData.data[indexPath.row].counts = rawValue.counts - 1
        }
        
        cell.count = rawValue.counts
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        setupSearchTextField(view: sectionView, searchField: searchField)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
