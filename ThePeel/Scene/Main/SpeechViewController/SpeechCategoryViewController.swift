//
//  SpeechCategoryViewController.swift
//  ThePeel
//
//  Created by Gone on 3/12/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class SpeechCategoryViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var helloTitleLabel: UILabel!
    @IBOutlet private weak var categotyTableView: UITableView!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture(sender:)))
        pan.delegate = self
        headerView.addGestureRecognizer(pan)
        registerCell()
    }
    
    // MARK: - Initialization
    static func viewController() -> SpeechCategoryViewController? {
        return Helper.getViewController(named: "SpeechCategoryViewController", inSb: "Speech")
    }
    
    // MARK: - Private Method
    private func registerCell() {
        categotyTableView.register(CategoriesTableViewCell.nib, forCellReuseIdentifier: CategoriesTableViewCell.indentifier)
    }
    
    // MARK: - Public Method
    public func toResultVCByCategory(resultCategoryString: String?) {
        guard let resultVC = ProductResultViewController.viewController() else { return }
        resultVC.resultCategoryString = resultCategoryString
        self.showWhiteLoadingSpinner()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in
            if self.isNetworkAvailable() == true {
                self.present(resultVC, animated: true, completion: nil)
                self.hideWhiteLoadingSpinner()
            }
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension SpeechCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return Constants.leftCategoriesActive.count
        default:
            return Constants.rightCategoriesActive.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesTableViewCell.indentifier, for: indexPath) as? CategoriesTableViewCell else { let cell = CategoriesTableViewCell(style: .default, reuseIdentifier: CategoriesTableViewCell.indentifier)
            return cell }
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.backgroundColor = .black
        
        switch indexPath.section {
        case 0:
            cell.data = Constants.leftCategoriesActive[indexPath.row]
        default:
            cell.data = Constants.rightCategoriesActive[indexPath.row]
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            toResultVCByCategory(resultCategoryString: Constants.leftCategoriesActive[indexPath.row].title)
        default:
            toResultVCByCategory(resultCategoryString: Constants.rightCategoriesActive[indexPath.row].title)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension SpeechCategoryViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
