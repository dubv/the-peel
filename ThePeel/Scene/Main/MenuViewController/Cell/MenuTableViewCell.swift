//
//  MenuTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

        // MARK: - IBOutlet
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    var data: IconMenu! {
        didSet {
            guard let data = data else { return }
            iconImageView.image = data.iconImage
            titleLabel.text = data.title
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
