//
//  MenuViewController.swift
//  ThePeel
//
//  Created by Gone on 1/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet public weak var menuTableViewCell: UITableView!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()
        setupDismissButton(dismissButton)
    }
    
    // MARK: - Initialization
    static func viewController() -> MenuViewController? {
        return Helper.getViewController(named: "MenuViewController", inSb: "Main")
    }
    
    private func setupUI() {
        view.backgroundColor = .black
        dismissButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        menuTableViewCell.backgroundColor = .black
    }
    
    // MARK: - Private Method
    private func registerCell() {
        menuTableViewCell.register(MenuTableViewCell.nib, forCellReuseIdentifier: MenuTableViewCell.indentifier)
    }
    
    fileprivate func toCoverageAreaVC() {
        guard let coverageAreaVC = CoverageAreaViewController.viewController() else { return }
        coverageAreaVC.modalPresentationStyle = .overFullScreen
        self.present(coverageAreaVC, animated: true)
    }
    
    fileprivate func toPeelVoicingVC() {
        guard let peelvoicingVC = SpeechViewController.viewController() else { return }
        peelvoicingVC.modalPresentationStyle = .overFullScreen
        self.present(peelvoicingVC, animated: true, completion: nil)
    }
    
    // MARK: - Public Method
    public func dialNumber(number : String) {
        guard let url = URL(string: "tel://\(number)") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.menuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.indentifier, for: indexPath) as? MenuTableViewCell else { let cell = MenuTableViewCell(style: .default, reuseIdentifier: MenuTableViewCell.indentifier)
            return cell }
        cell.data = Constants.menuData[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            toCoverageAreaVC()
        case 1:
           dialNumber(number: Constants.phoneNumber)
        case 2:
            print("Handle row 3")
        default:
            toPeelVoicingVC()
        }
    }
}
