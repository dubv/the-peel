//
//  DrinkTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class DrinkTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var coverImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var increaseButton: UIButton!
    @IBOutlet private weak var decreaseButton: UIButton!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var orderButton: CustomAddToCartButton!

    // MARK: - Properties
    var didIncreaseVolume:(() -> Void)?
    var didDecreaseVolume:(() -> Void)?
    
    var count: Int? {
        didSet {
            guard let count = self.count else { return }
            handleProductCount(count: count)
        }
    }
    
    weak var drinkTableViewDelegate: AddProductToCartDelegate?
    
    var drink: Drink! {
        didSet {
            guard let data = drink else { return }
            
            guard let url = URL(string: data.imageUrl) else { return }
            coverImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            titleLabel.text = data.name
            if let price = data.detail?.price {
                priceLabel.text = moneyLocale(input: price)
            }
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        orderButton.delegate = self
        setupUI()
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    fileprivate func setupUI() {
        countLabel.layer.borderColor = UIColor.lightGray.cgColor
        orderButton.layer.borderColor = UIColor.lightGray.cgColor
        increaseButton.layer.borderColor = UIColor.lightGray.cgColor
        decreaseButton.layer.borderColor = UIColor.lightGray.cgColor
        countLabel.layer.borderWidth = 0.6
        orderButton.layer.borderWidth = 0.6
        increaseButton.layer.borderWidth = 0.6
        decreaseButton.layer.borderWidth = 0.6
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Private Method
    fileprivate func handleProductCount(count: Int) {
        self.countLabel.text = String(count)
    }

    // MARK: - IBAction
    @IBAction func decreaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount > 1 {
            self.count = cellCount - 1
            didDecreaseVolume?()
        }
    }
    
    @IBAction func increaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount < 10 {
            self.count = cellCount + 1
            didIncreaseVolume?()
        }
    }
}

extension DrinkTableViewCell: AddToCartButtonDelegate {
    func buttonIn() {
        guard let quanlity = self.count, let price = drink.detail?.price else { return }
        drinkTableViewDelegate?.saveProduct(drink.id, String(quanlity), drink.imageUrl, name: drink.name, price: price, size: "", desc: "")
    }
}
