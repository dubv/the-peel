//
//  DessertTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

class DessertTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var orderButton: CustomAddToCartButton!
    @IBOutlet private weak var coverImage: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var positionTextView: UITextView!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var increaseButton: UIButton!
    @IBOutlet private weak var decreaseButton: UIButton!
    
    // MARK: - Properties
    var didIncreaseVolume:(() -> Void)?
    var didDecreaseVolume:(() -> Void)?
    
    weak var dessertTableViewDelegate: AddProductToCartDelegate?
    var count: Int? {
        didSet {
            guard let count = self.count else { return }
            handleProductCount(count: count)
        }
    }
    var dessert: Dessert! {
        didSet {
            guard let data = dessert else { return }
            guard let url = URL(string: data.imageUrl) else { return }
            coverImage.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            titleLabel.text = data.name
            positionTextView.text = data.detail?.composition
            
            if let weight = data.detail?.weight {
                weightLabel.text = String(describing: "Weight: " + weight)
            }
            
            if let price = data.detail?.price {
                priceLabel.text = moneyLocale(input: price)
            }
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        orderButton.delegate = self
        setupUI()
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    fileprivate func setupUI() {
        countLabel.layer.borderColor = UIColor.lightGray.cgColor
        orderButton.layer.borderColor = UIColor.lightGray.cgColor
        increaseButton.layer.borderColor = UIColor.lightGray.cgColor
        decreaseButton.layer.borderColor = UIColor.lightGray.cgColor
        countLabel.layer.borderWidth = 0.6
        orderButton.layer.borderWidth = 0.6
        increaseButton.layer.borderWidth = 0.6
        decreaseButton.layer.borderWidth = 0.6
    }
    
    // MARK: - Private Method
    fileprivate func handleProductCount(count: Int) {
        self.countLabel.text = String(count)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - IBAction
    @IBAction func increaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount < 10 {
            self.count = cellCount + 1
            didIncreaseVolume?()
        }
    }
    
    @IBAction func decreaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount > 1 {
            self.count = cellCount - 1
            didDecreaseVolume?()
        }
    }
}

extension DessertTableViewCell: AddToCartButtonDelegate {
    func buttonIn() {
        guard let quanlity = self.count, let price = dessert.detail?.price, let desc = dessert.detail?.composition else { return }
        dessertTableViewDelegate?.saveProduct(dessert.id, String(quanlity), dessert.imageUrl, name: dessert.name, price: price, size: "", desc: desc)
    }
}
