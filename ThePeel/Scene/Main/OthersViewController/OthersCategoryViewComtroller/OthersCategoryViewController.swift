//
//  OthersCategoryViewController.swift
//  ThePeel
//
//  Created by Gone on 3/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol OthersCategoryDelegate: NSObjectProtocol {
    func passIndexPath(section: Int)
}

class OthersCategoryViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    
    // MARK: - Properties
    public weak var otherCategoryDelegate: OthersCategoryDelegate?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupDismissButton(dismissButton)
    }
    
    // MARK: - Initialization
    static func viewController() -> OthersCategoryViewController? {
        return Helper.getViewController(named: "OtherCategoryViewController", inSb: "Others")
    }
    
    // MARK: - Private Method
    private func setupUI() {
        view.backgroundColor = .white
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture(sender:)))
        pan.delegate = self
        self.view.addGestureRecognizer(pan)
    }
    
    // MARK: - IBAction
    @IBAction func dessertButtonAction(_ sender: Any) {
        otherCategoryDelegate?.passIndexPath(section: 1)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func drinkButtonAction(_ sender: Any) {
        otherCategoryDelegate?.passIndexPath(section: 2)
        self.dismiss(animated: false, completion: nil)
    }
}

// MARK: - UIGestureRecognizerDelegate
extension OthersCategoryViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
