//
//  OthersViewController.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Reachability
import FSPagerView

class OthersViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var heroImageSliderView: FSPagerView!
    @IBOutlet private var heroImageView: UIView!
    @IBOutlet public weak var othersTableView: UITableView!

    // MARK: - Properties
    private let rowInsets = UIEdgeInsets(top: 3.0,
                                             left: 0.0,
                                             bottom: 3.0,
                                             right: 0.0)
    private var homePresenter: HomePresenter!
    private var cartPresenter: CartPresenter!
    public var drinkData = ProductData<Drink>()
    public var cartData = ProductData<Cart>()
    public var heroData = ProductData<HeroPost>()
    public var dessertData = ProductData<Dessert>()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setupUI()
        initPresenter()
        callAPI()
        showBlankView()
        self.tabBarController?.delegate = self
        didChangeNetwork = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setNavigationItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setNavigationItem()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.countTapped = 0
    }
    
    // MARK: - Initialization
    static func viewController() -> OthersViewController? {
        return Helper.getViewController(named: "OthersViewController", inSb: "Others")
    }
    
    private func setupUI() {
        cartButton = BadgeBarButtonItem(image: #imageLiteral(resourceName: "iconCartBlack"), target: self, action: #selector(toCart))
        cartButton.badgeBackgroundColor = UIColor(hexString: GlobalColor.darknight).withAlphaComponent(0.9)
        view.backgroundColor = .white
        othersTableView.tableHeaderView = heroImageView
        othersTableView.tableHeaderView?.backgroundColor = .white
        heroImageSliderView.delegate = self
        heroImageSliderView.dataSource = self
        setupHeroImageSliderView(sliderView: heroImageSliderView)
        setupLightNavigationBar()
    }
    
    public func setNavigationItem() {
        spinButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconSpin"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showMenu))
        setGlobalNavButton(leftButton: cartButton, rightButton: spinButton)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    // MARK: - Private Method
    private func registerCell() {
        othersTableView.register(DrinkTableViewCell.nib, forCellReuseIdentifier: DrinkTableViewCell.indentifier)
        othersTableView.register(CategoryButtonCell.nib, forCellReuseIdentifier: CategoryButtonCell.indentifier)
        othersTableView.register(DessertTableViewCell.nib, forCellReuseIdentifier: DessertTableViewCell.indentifier)
    }
    
    private func initPresenter() {
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
        cartPresenter = CartPresenter()
        cartPresenter.cartDelegate = self
    }
    
    private func callAPI() {
        homePresenter.getAllDessert(isCall: true)
        homePresenter.getAllDrink(isCall: true)
        cartPresenter.getCartsBasedUserCurrent(true)
        homePresenter.getAllHeroImage(isCall: true)
    }
}

extension OthersViewController: AddProductToCartDelegate {
    
    func saveProduct(_ id: String, _ quanlity: String, _ favicon: String, name: String, price: String, size: String, desc: String) {
        let ticks = Date().ticks
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        let uidCart = String(ticks)
        let object = Cart(key: "", idItem: uidCart, idProduct: id, coverImage: favicon, price: price, size: size, quanlity: quanlity, desc: desc, dateTime: dateTime, userID: "", productName: name)
        
        if self.isNetworkAvailable() == true {
            UUID.uuid != nil ? checkCartAvailabel(object: object, favicon: favicon) : showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
    
    func checkCartAvailabel(object: Cart, favicon: String) {
        if let index = self.cartData.data.firstIndex(where: {$0.idProduct == object.idProduct}) {
            object.idItem = self.cartData.data[index].idItem
            guard let currentCount = Int(self.cartData.data[index].quanlity) else { return }
            guard let newCount = Int(object.quanlity) else { return }
            let tempQuanlity = currentCount + newCount
            if tempQuanlity <= 10 {
                object.quanlity = String(tempQuanlity)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            } else {
                object.quanlity = String(currentCount)
                self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, GlobalText.MESS_CART_MAXIMUN, GlobalText.TITLE_BUTTON_OKAY)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            }
        } else {
            homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
        }
    }
}

extension OthersViewController: HomeDelegate, CartDelegate, CategoryButtonCellDelegate, OthersCategoryDelegate {
    
    func categoryAction() {
        guard let otherCateVC = OthersCategoryViewController.viewController() else { return }
        otherCateVC.modalPresentationStyle = .overFullScreen
        otherCateVC.otherCategoryDelegate = self
        self.present(otherCateVC, animated: true)
    }
    
    func passIndexPath(section: Int) {
        var indexPath = IndexPath()
        section == 1 ? (indexPath = IndexPath(row: 0, section: 1)) : (indexPath = IndexPath(row: 0, section: 2))
        othersTableView.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    func getAllHeroImage(images: [HeroPost]) {
        self.heroData.equal(images)
        heroImageSliderView.reloadData()
    }
    
    func getAllDrink(drink: [Drink]) {
        self.drinkData.equal(drink)
        othersTableView.reloadSections([2], with: .fade)
    }
    
    func getAllDessert(dessert: [Dessert]) {
        self.dessertData.equal(dessert)
        othersTableView.reloadSections([1], with: .fade)
        NotificationCenter.default.post(name: .toBlank, object: nil)
    }
    
    func getCartFinished(_ data: [Cart]) {
        self.cartData.equal(data)
        
        data.count != 0 ? (self.cartButton.badgeText = "\(data.count)") : (self.cartButton.badgeText = "")
        
        if UUID.uuid == nil {
            cartButton.badgeText = ""
        }
    }
    
    func getCartFailed(_ mess: String) {
        self.cartButton.badgeText = ""
    }
    
    func addOrUpdate(object: Cart, favicon: String) {
        self.setupPopupView(object: object, favicon: favicon)
    }
    
    func addOrUpdateFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension OthersViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return dessertData.data.count
        default:
            return drinkData.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryButtonCell.indentifier, for: indexPath) as? CategoryButtonCell
                else { let cell = CategoryButtonCell(style: .default, reuseIdentifier: CategoryButtonCell.indentifier)
                    return cell
            }
            cell.categoryButtonCellDelegate = self
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DessertTableViewCell.indentifier, for: indexPath) as? DessertTableViewCell, indexPath.row < self.dessertData.data.count
                else { let cell = DessertTableViewCell(style: .default, reuseIdentifier: DessertTableViewCell.indentifier)
                    return cell
            }
            let rawValue = self.dessertData.data[indexPath.row]
            cell.dessert = rawValue
            cell.dessertTableViewDelegate = self
            cell.didIncreaseVolume = { [weak self]  in
                self?.dessertData.data[indexPath.row].counts = rawValue.counts + 1
            }
            cell.didDecreaseVolume = { [weak self]  in
                self?.dessertData.data[indexPath.row].counts = rawValue.counts - 1
            }
            cell.count = rawValue.counts
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DrinkTableViewCell.indentifier, for: indexPath) as? DrinkTableViewCell, indexPath.row < self.drinkData.data.count else { let cell = DrinkTableViewCell(style: .default, reuseIdentifier: DrinkTableViewCell.indentifier)
                return cell }
            
            let rawValue = self.drinkData.data[indexPath.row]
            cell.drink = rawValue
            cell.drinkTableViewDelegate = self
            
            cell.didIncreaseVolume = { [weak self]  in
                self?.drinkData.data[indexPath.row].counts = rawValue.counts + 1
            }
            
            cell.didDecreaseVolume = { [weak self]  in
                self?.drinkData.data[indexPath.row].counts = rawValue.counts - 1
            }
            cell.count = rawValue.counts
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - FSPagerViewDelegate, FSPagerViewDataSource, UITabBarControllerDelegate
extension OthersViewController: FSPagerViewDelegate, FSPagerViewDataSource, UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        countTapped += 1
        if tabBarController.selectedIndex == 0 {
            if let navVC = viewController as? UINavigationController, let vc = navVC.viewControllers.first as? HomeViewController {
                if vc.isViewLoaded && (vc.view.window != nil), countTapped >= 2 {
                    vc.pizzaCollectionView.scrollToTop()
                    countTapped = 0
                }
            }
        } else {
            if let navVC = viewController as? UINavigationController, let vc = navVC.viewControllers.first as? OthersViewController {
                if vc.isViewLoaded && (vc.view.window != nil), countTapped >= 2 {
                    vc.othersTableView.scrollToTop()
                    countTapped = 0
                }
            }
        }
        return true
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.heroData.data.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = heroImageSliderView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.contentView.layer.shadowRadius = 0
        if let url = URL(string: heroData.data[index].coverImg) {
            cell.imageView?.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
        }
        return cell
    }
}

// MARK: - NetworkStatusListener
extension OthersViewController: DidChangeNetwork {
    func networkAvailable() {
        callAPI()
    }
}
