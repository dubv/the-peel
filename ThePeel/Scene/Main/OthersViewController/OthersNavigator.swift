//
//  OthersNavigator.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol OthersNavigator {
    func toOthers()
}

struct OthersDrinkNavigator: OthersNavigator {
    private weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toOthers() {
        guard let vc = OthersViewController.viewController() else { return }
        self.navigation?.pushViewController(vc, animated: true)
    }
}
