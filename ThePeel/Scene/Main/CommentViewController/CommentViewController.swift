//
//  CommentViewController.swift
//  ThePeel
//
//  Created by Gone on 2/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Social

class CommentViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private weak var mainCommentContrainView: UIView!
    @IBOutlet private weak var commentContainView: UIView!
    @IBOutlet private weak var commentTextView: UITextView!
    @IBOutlet private weak var commentTableView: UITableView!
    @IBOutlet private weak var postButton: UIButton!
    @IBOutlet private weak var mainContainPostView: UIView!
    @IBOutlet private weak var heightTextViewConstraints: NSLayoutConstraint!
    
    // MARK: - Properties
    public var commentMessData = [Comment]()
    public var pizzaData: Pizza?
    public var idPizza: String?
    public var trigger: Bool = true
    public var profileInfo: CustomUser?
    public var selectedIndexPath: IndexPath?
    private var productDetailPresenter: ProductDetailPresenter!
    private var homePresenter: HomePresenter!
    private var commentPresenter: CommentPresenter!
    private var profilePresenter: ProfilePresenter!
    private var triggerr: (() -> Void)?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextView.delegate = self
        registerCell()
        initPresenter()
        callAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupDismissButton(dismissButton)
        setupUI()
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Initialization
    static func viewController() -> CommentViewController? {
        return Helper.getViewController(named: "CommentViewController", inSb: "Product")
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
        commentContainView.layer.borderColor = UIColor.black.cgColor
        commentContainView.layer.borderWidth = 1.5
        commentContainView.layer.cornerRadius = 22.5
        
        commentTextView.placeholder = GlobalText.PLACEHOLDER_TEXTVIEW_COMMENT
        postButton.isHidden = true
        setupKeyboard()
    }
    
    fileprivate func initPresenter() {
        commentPresenter = CommentPresenter()
        commentPresenter.commentDelegate = self
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
        productDetailPresenter = ProductDetailPresenter()
        productDetailPresenter.productDetailDelegate = self
    }
    
    fileprivate func callAPI() {
        guard let idPizza = self.idPizza else { return }
        profilePresenter.getUserInfo(true)
        commentPresenter.queryOrdered(isCall: true, sortBy: Constants.sortByIdProductPath, value: idPizza)
    }
    
    // MARK: - Private Method
    fileprivate func setupKeyboard() {
        let tapGesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.direction = .down
        self.mainCommentContrainView.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func pushingComment(namedObj: String, content: String) {
        let ticks = Date().ticks
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        let uidComment = String(ticks)
        
        guard let userName = self.profileInfo?.userName, let avatar = self.profileInfo?.avatar, let keyProduct = pizzaData?.ref?.key else { return }
        let commentObject = Comment(key: "", avatar: avatar, userName: userName, commentContent: content, dateTime: dateTime, keyProduct: keyProduct)
        productDetailPresenter.addComment(isCall: true, object: commentObject, nameObject: namedObj, uidComment: uidComment)
    }
    
    // MARK: - Private Method
    fileprivate func registerCell() {
       commentTableView.register(CommentTableViewCell.nib, forCellReuseIdentifier: CommentTableViewCell.indentifier)
    }
    
    fileprivate func resetCommentView() {
        UIView.animate(withDuration: 0.5, animations: { () in
            self.heightTextViewConstraints.constant = 35
        })
    }
    
    // MARK: - Target
    @objc func keyboardWillShow(notification: NSNotification )
    {
        guard let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double, let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt, let curFrame = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue, let targetFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        let deltaY = targetFrame.origin.y - curFrame.origin.y
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.mainContainView.frame.origin.y += deltaY
            self.commentTableView.contentInset.top -= deltaY
        }, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.endEditing(true)
            self.commentTableView.contentInset.bottom = 0
            if self.commentTextView.text.isEmpty {
                self.commentTextView.placeholder = GlobalText.PLACEHOLDER_TEXTVIEW_COMMENT
                self.postButton.isHidden = true
            }
        })
    }
    
    // MARK: - IBAction
    @IBAction func postButtonAction(_ sender: Any) {
        guard let commentCount =  self.pizzaData?.comment.count else { return }
        print(commentCount)
        if commentCount > 3 {
            self.tableViewScrollToBottom(animated: true, tableView: self.commentTableView)
        }
        dismissKeyboard()
        guard let content = self.commentTextView.text, let objName = pizzaData?.ref?.key else { return }
        profileInfo?.key != nil ? pushingComment(namedObj: objName, content: content) : self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        
    }
}

// MARK: - CategoryDelegate, ProfileDelegate, ProductDetailDelegate
extension CommentViewController: CommentDelegate, ProfileDelegate, ProductDetailDelegate {
    func getCommentFinished(_ data: Pizza) {
        self.pizzaData = data
        self.commentTableView.reloadData()
    }
    
    func getUserInfoFinished(info: CustomUser) {
        self.profileInfo = info
    }
    
    func addCommentFinished() {
        commentTextView.text = ""
        postButton.isHidden = true
        resetCommentView()
        if let placeholderLabel = commentTextView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !commentTextView.text.isEmpty
        }
    }
    
    func addCommentFailed(_ mess: String) {
        self.showAlert(GlobalText.TITLE_COMMENT_FAILED, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
}

// MARK: - UITextViewDelegate, UITableViewDelegate, UITableViewDataSource
extension CommentViewController: UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    func textViewDidChange(_ textView: UITextView) {
        let size = CGSize(width: view.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        if let placeholderLabel = commentTextView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !commentTextView.text.isEmpty
        }
        
        UIView.animate(withDuration: 0.5, animations: { () in
            self.heightTextViewConstraints.constant = estimatedSize.height
            self.commentTextView.text.isEmpty ? (self.postButton.isHidden = true) : (self.postButton.isHidden = false)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.pizzaData?.comment.count else {return 0}
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.indentifier, for: indexPath) as? CommentTableViewCell else { let cell = CommentTableViewCell(style: .default, reuseIdentifier: CommentTableViewCell.indentifier)
            return cell }
        
        pizzaData?.comment.sort { a, b in
            a.key < b.key
        }
        
        cell.comment = self.pizzaData?.comment[indexPath.row]
        cell.commentTableDelegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableView.automaticDimension
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: - CommentTableDelegate
extension CommentViewController: CommentTableDelegate {
    func showActionSheets(commentId: String, uuid: String, content: String) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let replyAction = UIAlertAction(title: GlobalText.TITLE_REPLY, style: .default, handler: nil)
        let hideAction = UIAlertAction(title: GlobalText.TITLE_HIDE, style: .default, handler: nil)
        let deleteAction = UIAlertAction(title: GlobalText.TITLE_DELETE, style: .destructive) { _ -> Void in
            if let commentIndex = self.pizzaData?.comment.firstIndex(where: {$0.key == commentId}) {
                self.pizzaData?.comment[commentIndex].ref?.removeValue()
            }
            
            if let commentIndex = self.profileInfo?.comment.firstIndex(where: {$0.key == commentId }) {
                self.profileInfo?.comment[commentIndex].ref?.removeValue()
            }
        }
        
        let copyAction = UIAlertAction(title: GlobalText.TITLE_COPY, style: .default) { _ -> Void in
            UIPasteboard.general.string = content
        }
        
        let cancelAction = UIAlertAction(title: GlobalText.TITLE_CANCEL, style: .cancel) { _ -> Void in
            self.triggerr?()
        }
        
        let actionSheetsConformUserID = [replyAction, hideAction, copyAction, deleteAction, cancelAction]
        let actionSheetsNoneConformUserID = [replyAction, hideAction, copyAction, cancelAction]
        
        if uuid == self.profileInfo?.key {
            for action in actionSheetsConformUserID {
                actionSheet.addAction(action)
            }
        } else {
            for action in actionSheetsNoneConformUserID {
                actionSheet.addAction(action)
            }
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func hideKeyboard() {
        dismissKeyboard()
    }
}
