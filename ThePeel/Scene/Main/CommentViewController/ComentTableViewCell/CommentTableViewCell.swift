//
//  CommentTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

protocol CommentTableDelegate: NSObjectProtocol {
    func showActionSheets(commentId: String, uuid: String, content: String)
    func hideKeyboard()
}

class CommentTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var avatarMainContrainView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var commentContentLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var commentContentView: UIView!
    
    // MARK: - Properties
    var comment: Comment! {
        didSet {
            guard let comment = comment, let avatar = comment.avatar else { return }
            let url = URL(string: avatar)
            avatarImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            userNameLabel.text = comment.userName
            commentContentLabel.text = comment.commentContent
            guard let commentTime = UInt64(comment.key) else { return }
            dateLabel.text = converTickToTimeslive(tickFromPass: commentTime)
        }
    }
    weak var commentTableDelegate: CommentTableDelegate?
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(showActionSheet(sender:)))
        longGesture.minimumPressDuration = 0.25
        commentContentView.addGestureRecognizer(longGesture)
        
        let tapHideGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.contentView.addGestureRecognizer(tapHideGesture)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    fileprivate func setupUI() {
        commentContentView.layer.cornerRadius = 20.0
        commentContentView.backgroundColor = UIColor(hexString: GlobalColor.grayTableView)
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width/2
    }
    
    // MARK: - Target
    @objc func showActionSheet(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            commentContentView.backgroundColor = UIColor(hexString: GlobalColor.gray)
            guard let uid = comment.uidString, let content = comment.commentContent else { return }
            commentTableDelegate?.showActionSheets(commentId: comment.key, uuid: uid, content: content)
            commentTableDelegate?.hideKeyboard()
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            commentContentView.backgroundColor = UIColor(hexString: GlobalColor.grayTableView)
        }
    }
    
    @objc func dismissKeyboard() {
        commentTableDelegate?.hideKeyboard()
    }
}
