//
//  CommentTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/12/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentContentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentContentView: UIView!
    
    // MARK: - Properties
    var comment: Comment! {
        didSet {
            guard let comment = comment else {return}
            userNameLabel.text = comment.userName
            commentContentLabel.text = comment.commentContent
            dateLabel.text = comment.dateTime
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    fileprivate func setupUI() {
        commentContentView.layer.cornerRadius = 20.0
        commentContentView.backgroundColor = UIColor(hexString: ColorGlobal.gray)
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width/2
    }
    
}
