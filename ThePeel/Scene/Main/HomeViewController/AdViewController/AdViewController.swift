//
//  AdViewController.swift
//  ThePeel
//
//  Created by Gone on 3/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class AdViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var coverImageView: UIImageView!
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private weak var homeIndicatorView: UIView!
    @IBOutlet private weak var advertisementTableView: UITableView!
    @IBOutlet private var headerView: UIView!
    
    // MARK: - Properties
    fileprivate var mainViewAlpha: CGFloat = 0.3
    public var heroPost: HeroPost? {
        didSet {
            loadData()
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        transformViewBackgroud(alpha: 0.0)
        setupUI()
        registerCell()
        loadData()
    }
    
    // MARK: - Initialization
    static func viewController() -> AdViewController? {
        return Helper.getViewController(named: "AdViewController", inSb: "Home")
    }
    
    fileprivate func loadData() {
        guard isViewLoaded, let postData = heroPost, let url = URL(string: postData.coverImg) else { return }
        coverImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
    }
    
    fileprivate func transformViewBackgroud(alpha: CGFloat) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(alpha)
    }
    
    fileprivate func setupUI() {
        advertisementTableView.tableHeaderView = headerView
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture(sender:)))
        pan.delegate = self
        homeIndicatorView.addGestureRecognizer(pan)
    }
    
    // MARK: - Private Method
    fileprivate func registerCell() {
        advertisementTableView.register(AdImageContentTableViewCell.nib, forCellReuseIdentifier: AdImageContentTableViewCell.indentifier)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension AdViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let heroPost = self.heroPost else {
            return 0
        }
       return heroPost.imageUrl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AdImageContentTableViewCell.indentifier, for: indexPath) as? AdImageContentTableViewCell, let count = heroPost?.imageUrl.count, indexPath.row < count else { let cell = AdImageContentTableViewCell(style: .default, reuseIdentifier: AdImageContentTableViewCell.indentifier)
            return cell }
        if let heroPost = heroPost {
            cell.data = heroPost.imageUrl[indexPath.row].urlImg
        }
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViews = UIView()
        var constraints = [NSLayoutConstraint]()
        if section == 0 {
            headerViews.backgroundColor = UIColor(hexString: "F5F5F5")
            let titleLabel = UILabel()
            titleLabel.text = heroPost?.title
            titleLabel.numberOfLines = 0
            titleLabel.sizeToFit()
            if let font = Constants.OpenSans[3], let customFont = UIFont(name: font, size: 28) {
                titleLabel.font = customFont
            }
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            headerViews.addSubview(titleLabel)
            constraints.append(NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: headerViews, attribute: .leading, multiplier: 1.0, constant: 16.0))
            constraints.append(NSLayoutConstraint(item: titleLabel, attribute: .trailing, relatedBy: .equal, toItem: headerViews, attribute: .trailing, multiplier: 1.0, constant: -16.0))
            constraints.append(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: headerViews, attribute: .top, multiplier: 1.0, constant: 10.0))
              constraints.append(NSLayoutConstraint(item: titleLabel, attribute: .bottom, relatedBy: .equal, toItem: headerViews, attribute: .bottom, multiplier: 1.0, constant: -10.0))
            headerViews.addConstraints(constraints)
            return headerViews
        }
        return headerViews
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

// MARK: - UIScrollViewDelegate
extension AdViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 30 {
            UIView.transition(with: homeIndicatorView, duration: 0.5, options: .showHideTransitionViews, animations: {
                self.homeIndicatorView.alpha = 0
                self.homeIndicatorView.isHidden = true
            }, completion: nil)
        } else {
            UIView.transition(with: dismissButton, duration: 0.5, options: .showHideTransitionViews, animations: {
                self.homeIndicatorView.alpha = 1
                self.homeIndicatorView.isHidden = false
            }, completion: nil)
        }
    }
}

extension AdViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
