//
//  AdImageContentTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class AdImageContentTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private weak var contentImageView: UIImageView!
    
    // MARK: - Properties
    public var data: String? {
        didSet {
            guard let data = data, let url = URL(string: data) else { return }
            contentImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    private func setupUI() {
        mainContainView.layer.cornerRadius = 10
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
