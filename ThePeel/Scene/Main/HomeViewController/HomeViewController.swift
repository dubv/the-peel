//
//  HomeViewController.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Reachability
import FSPagerView

class HomeViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private var footerView: UIView!
    @IBOutlet private weak var heroImgSliderView: FSPagerView!
    @IBOutlet public weak var pizzaCollectionView: UICollectionView!
    
    // MARK: - Properties
    public var cartData = [Cart]()
    public var pizzaData = ProductData<Pizza>()
    public var heroData = ProductData<HeroPost>()
    private var homePresenter: HomePresenter!
    private var cartPresenter: CartPresenter!
    private var notifisPresenter: NotificationPresenter!
    private var profilePresenter: ProfilePresenter!
    private var onboardingPresenter: OnboardingPreseneter!
    private var categoryBtnState = CategoryBtnState.grid
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(top: 10.0,
                                             left: 12.0,
                                             bottom: 15.0,
                                             right: 12.0)
    private var mainContainView: OnboardingSubView!
    /// Key to save the gmcToken into globalTable
    fileprivate var phoneNumber: String?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotificationToPushToken()
        setTutorial()
        initPresenter()
        registerCells()
        setupUI()
        self.tabBarController?.delegate = self
        didChangeNetwork = self
        configNewNavigationUIs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setNavigationItem()
        NotificationCenter.default.addObserver(self, selector: #selector(toNewProductAppNotLaunching), name: .toHome, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toItemOrderedAppNotLaunching), name: .toHome, object: nil)
    }
    
    // MARK: - Initialization
    fileprivate func setTutorial() {
        if !UserDefaults.standard.bool(forKey: "HomeTutorial") {
            guard let onboardingVC = OnboardingViewController.viewController() else { return }
            onboardingVC.providesPresentationContextTransitionStyle = true
            onboardingVC.definesPresentationContext = true
            onboardingVC.modalTransitionStyle = .crossDissolve
            onboardingVC.modalPresentationStyle = .overFullScreen
            self.present(onboardingVC, animated: false)
            UserDefaults.standard.set(true, forKey: "HomeTutorial")
        } else {
            showBlankView()
        }
    }
    
    fileprivate func setupNotificationToPushToken() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushToken(_ :)), name: NSNotification.Name(rawValue: "FCMToken"), object: nil)
    }
    
    fileprivate func setupNotificationHandleProduct() {
        NotificationCenter.default.addObserver(self, selector: #selector(toNewProductAppForceBackground(_:)), name: .newProduct, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toItemOrderedAppForceBackground(_:)), name: .toItemOrdered, object: nil)
    }
    
    static func viewController() -> HomeViewController? {
        return Helper.getViewController(named: "HomeViewController", inSb: "Home")
    }
    
    public func setNavigationItem() {
        spinButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconSpin"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showMenu))
        setGlobalNavButton(leftButton: cartButton, rightButton: spinButton)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    private func configNewNavigationUIs() {
        let navigationBarAppearance = self.navigationController?.navigationBar
        navigationBarAppearance?.barTintColor = UIColor(hexString: GlobalColor.globalCustomNavColor)
        navigationController?.toolbar.barTintColor = UIColor(hexString: GlobalColor.globalCustomNavColor)
        navigationController?.toolbar.isTranslucent = false
    }

    private func setupUI() {
        cartButton = BadgeBarButtonItem(image: #imageLiteral(resourceName: "iconCartBlack"), target: self, action: #selector(toCart))
        cartButton.badgeBackgroundColor = UIColor(hexString: GlobalColor.darknight).withAlphaComponent(0.7)
        view.backgroundColor = .white
        heroImgSliderView.delegate = self
        heroImgSliderView.dataSource = self
        setupHeroImageSliderView(sliderView: heroImgSliderView)
        setupLightNavigationBar()
        setupPizzaCollectionView()
    }

    fileprivate func setupPizzaCollectionView() {
        if let flowLayout = pizzaCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            /// Pin headerView
            flowLayout.sectionHeadersPinToVisibleBounds = true
            /// Do layout transform
            switch categoryBtnState {
            case .grid:
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = view.frame.width - paddingSpace
                let widthPerItem = availableWidth / itemsPerRow
                flowLayout.estimatedItemSize = CGSize(width: widthPerItem, height: widthPerItem + (widthPerItem*0.1))
            case .vert:
                let widthPerItem = view.frame.width - 34
                flowLayout.estimatedItemSize = CGSize(width: widthPerItem, height: widthPerItem*1.35)
            default:
                let paddingSpace = sectionInsets.left
                let availableWidth = view.frame.width - paddingSpace
                flowLayout.estimatedItemSize = CGSize(width: availableWidth, height: availableWidth*1.35)
            }
        }
    }

    // MARK: - Private Method
    fileprivate func initPresenter() {
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
        cartPresenter = CartPresenter()
        cartPresenter.cartDelegate = self
        notifisPresenter = NotificationPresenter()
        notifisPresenter.notificationDelegate = self
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
        onboardingPresenter = OnboardingPreseneter()
        onboardingPresenter.onboardingDelegate = self
    }
    
    fileprivate func transformPizzaCollecitonView() {
        self.pizzaCollectionView.alpha = 0.5
        UIView.transition(with: pizzaCollectionView, duration: 0.5, options: .showHideTransitionViews, animations: {
            self.pizzaCollectionView.alpha = 1
        }, completion: nil)
        setupPizzaCollectionView()
        pizzaCollectionView.reloadData()
    }
    
    private func registerCells() {
        /// NormalRegister
        pizzaCollectionView.register(VerticalCollectionViewCell.nib, forCellWithReuseIdentifier: VerticalCollectionViewCell.indentifier)
        pizzaCollectionView.register(GridCollectionViewCell.nib, forCellWithReuseIdentifier: GridCollectionViewCell.indentifier)
        /// ElementKindSectionHeader
        pizzaCollectionView.register(CategoryCollectionViewCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CategoryCollectionViewCell.indentifier)
    }
    
    // MARK: - Public Method
    public func callAPI() {
        homePresenter.getAllPizza(isCall: true)
        homePresenter.getAllHeroImage(isCall: true)
        cartPresenter.getCartsBasedUserCurrent(true)
        notifisPresenter.getNotifisUnread(isCall: true, value: false)
        profilePresenter.getUserInfo(true)
    }
    
    public func categoriesAction() {
        guard let categoryVC = CategoryViewController.viewController() else { return }
        categoryVC.categoryDelegate = self
        self.present(categoryVC, animated: true)
    }
    
    // MARK: - Target
    @objc func pushToken(_ notification: Notification) {
        if let token = notification.userInfo as? [String: String] {
            UUToken.setToken = token["token"]
        }
    }
    
    @objc func toNewProductAppForceBackground(_ notification: Notification) {
        if let productId = notification.userInfo as? [String: String], let idUnwrapped = productId["productId"] {
            notifisPresenter.getPizzaBaseId(isCall: true, itemId: idUnwrapped)
        }
    }
    
    @objc func toNewProductAppNotLaunching() {
        guard let id = UserDefaults.standard.string(forKey: "SHOW_PRODUCT") else { return }
        notifisPresenter.getPizzaBaseId(isCall: true, itemId: id)
        UserDefaults.standard.removeObject(forKey: "SHOW_PRODUCT")
    }
    
    @objc func toItemOrderedAppForceBackground(_ notification: Notification) {
        if let itemId = notification.userInfo as? [String: String], let idUnwrapped = itemId["itemId"] {
            notifisPresenter.getItemCheckoutedBaseId(isCall: true, itemId: idUnwrapped)
        }
    }
    
    @objc func toItemOrderedAppNotLaunching() {
        guard let id = UserDefaults.standard.string(forKey: "SHOW_ITEM") else { return }
        notifisPresenter.getItemCheckoutedBaseId(isCall: true, itemId: id)
        UserDefaults.standard.removeObject(forKey: "SHOW_ITEM")
    }
}

// MARK: - HomeDelegate, CategoriesHandleDelegate, NotificationDelegate, ProfileDelegate, DidChangeNetwork, OnboardingDelegate
extension HomeViewController: HomeDelegate, CategoryButtonCellDelegate, NotificationDelegate, ProfileDelegate, DidChangeNetwork, OnboardingDelegate {
    func networkAvailable() {
        callAPI()
    }
    
    func getNotifisUnreadFinished(count: Int) {
        count != 0 ? self.tabBarController?.increaseBadge(indexOfTab: 2, num: "\(count)") : self.tabBarController?.removeBadge(indexOfTab: 2)
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func getAllHeroImage(images: [HeroPost]) {
        heroData.equal(images)
        heroImgSliderView.reloadData()
    }
    
    func getAllPizza(pizza: [Pizza]) {
        pizzaData.remove()
        pizzaData.equal(pizza)
        pizzaCollectionView.reloadData()
        NotificationCenter.default.post(name: .toBlank, object: nil)
        setupNotificationHandleProduct()
    }
    
    func addOrUpdate(object: Cart, favicon: String) {
        self.setupPopupView(object: object, favicon: favicon)
    }
    
    func addOrUpdateFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func categoryAction() {
        self.categoriesAction()
    }
    
    func getUserInfoFinished(info: CustomUser) {
        UUName.name = info.lastName
        phoneNumber = info.phoneNumber
        if let uid = UUID.uuid {
            onboardingPresenter.pushToken(isCall: true, uuid: uid, phoneNumber: info.phoneNumber)
        }
    }
    
    /// Hanle CloudMessage
    func getProductFinished(product: Any) {
        guard let pizza = product as? Pizza else { return }
        guard let productVC = ProductViewController.viewController() else { return }
        self.presentVC(viewController: productVC)
        productVC.bindingFromNotifis(pizza: pizza)
    }
    
    func getItemCheckoutedFinished(item: ItemPayment) {
        guard let checkoutVC = CheckoutViewController.viewController() else { return }
        self.pushViewController(viewController: checkoutVC)
        checkoutVC.binding(itemPayment: item)
    }
}

// MARK: - PassDataToHome, AddProductToCartDelegate, CartDelegate
extension HomeViewController: PassDataToHome, AddProductToCartDelegate, CartDelegate {
    func saveProduct(_ id: String, _ quanlity: String, _ favicon: String, name: String, price: String, size: String, desc: String) {
        let ticks = Date().ticks
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        let uidCart = String(ticks)
        let object = Cart(key: "", idItem: uidCart, idProduct: id, coverImage: favicon, price: price, size: size, quanlity: quanlity, desc: desc, dateTime: dateTime, userID: "", productName: name)
        if self.isNetworkAvailable() == true {
            UUID.uuid != nil ? checkCartAvailabel(object: object, favicon: favicon) : showAlert(GlobalText.TITLE_ERROR_GLOBAL, GlobalText.MESS_CHECK_USER_FAILED, GlobalText.TITLE_BUTTON_OKAY)
        }
    }
    
    // Passing data from CategoryViewController
    func passingData(_ pizza: [Pizza]) {
        self.pizzaData.remove()
        self.pizzaData.equal(pizza)
        pizzaCollectionView.reloadData()
    }
    
    func getCartFinished(_ data: [Cart]) {
        self.cartData = data
        data.count != 0 ? (self.cartButton.badgeText = "\(data.count)") : (self.cartButton.badgeText = "")
        if UUID.uuid == nil {
            cartButton.badgeText = ""
        }
        profilePresenter.getUserInfo(true)
    }
    
    func getCartFailed(_ mess: String) {
        self.cartButton.badgeText = ""
    }
    
    func checkCartAvailabel(object: Cart, favicon: String) {
        if let index = self.cartData.firstIndex(where: {$0.idProduct == object.idProduct}) {
            object.idItem = self.cartData[index].idItem
            guard let currentCount = Int(self.cartData[index].quanlity) else { return }
            guard let newCount = Int(object.quanlity) else { return }
            let tempQuanlity = currentCount + newCount
            if tempQuanlity <= 10 {
                object.quanlity = String(tempQuanlity)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            } else {
                object.quanlity = String(currentCount)
                self.showAlert(GlobalText.TITLE_POPUP_ADDCART_FAILED, GlobalText.MESS_CART_MAXIMUN, GlobalText.TITLE_BUTTON_OKAY)
                homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
            }
        } else {
            homePresenter.addProductToCart(isCall: true, object: object, favicon: favicon)
        }
    }
}

// MARK: - FSPagerViewDelegate, FSPagerViewDataSource, UITabBarControllerDelegate
extension HomeViewController: FSPagerViewDelegate, FSPagerViewDataSource, UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        countTapped += 1
        if tabBarController.selectedIndex == 0 {
            if let navVC = viewController as? UINavigationController, let vc = navVC.viewControllers.first as? HomeViewController {
                if vc.isViewLoaded && (vc.view.window != nil), countTapped >= 2 {
                    vc.pizzaCollectionView.scrollToTop()
                    countTapped = 0
                }
            }
        } else {
            if let navVC = viewController as? UINavigationController, let vc = navVC.viewControllers.first as? OthersViewController {
                if vc.isViewLoaded && (vc.view.window != nil), countTapped >= 2 {
                    vc.othersTableView.scrollToTop()
                    countTapped = 0
                }
            }
        }
        return true
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return heroData.data.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = heroImgSliderView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.contentView.layer.shadowRadius = 0
        if let url = URL(string: heroData.data[index].coverImg) {
            cell.imageView?.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
        }
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        guard let adVC = AdViewController.viewController() else { return }
        adVC.modalPresentationStyle = .overFullScreen
        self.present(adVC, animated: true, completion: nil)
        adVC.heroPost = self.heroData.data[index]
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pizzaData.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch categoryBtnState {
        case .grid:
            guard let item = collectionView.dequeueReusableCell(withReuseIdentifier: GridCollectionViewCell.indentifier, for: indexPath) as? GridCollectionViewCell else {return UICollectionViewCell()}
            item.pizza = pizzaData.data[indexPath.row]
            if let likeByUser = pizzaData.data[indexPath.row].likeByUser {
                item.handleLikeCount(count: String(pizzaData.data[indexPath.row].likeCount), likeByUser: likeByUser)
            } else {
                item.handleLikeCount(count: String(pizzaData.data[indexPath.row].likeCount))
            }
            return item
        case .vert:
            guard let item = collectionView.dequeueReusableCell(withReuseIdentifier: VerticalCollectionViewCell.indentifier, for: indexPath) as? VerticalCollectionViewCell else {return UICollectionViewCell()}
            let rawValue = pizzaData.data[indexPath.row]
            item.pizza = rawValue
            item.addProductToCartDelegate = self
            item.didIncreaseVolume = { [weak self]  in
                self?.pizzaData.data[indexPath.row].counts = rawValue.counts + 1
            }
            item.didDecreaseVolume = { [weak self]  in
                self?.pizzaData.data[indexPath.row].counts = rawValue.counts - 1
            }
            item.count = rawValue.counts
            return item
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CategoryCollectionViewCell.indentifier, for: indexPath)
            if let cell = headerView as? CategoryCollectionViewCell {
                cell.categoryCollectionDelegate = self
            }
            headerView.frame.size.height = 50
            let shadowSize: CGFloat = headerView.frame.size.height
            let contactRect = CGRect(x: 0, y: shadowSize*0.2, width: headerView.frame.size.width, height: shadowSize*0.82)
            headerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
            headerView.layer.shadowPath = UIBezierPath(roundedRect: contactRect, cornerRadius: 0).cgPath
            headerView.layer.shadowOpacity = 0.4
            headerView.layer.shadowRadius = 4
            headerView.clipsToBounds = false
            return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let productVC = ProductViewController.viewController() else { return }
        pushViewController(viewController: productVC)
        productVC.currentIndex = indexPath.row
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

// MARK: - CategoryCollectionDelegate, UIGestureRecognizerDelegate
extension HomeViewController: CategoryCollectionDelegate, UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func getCategories(trigger: Int) {
        if trigger != categoryBtnState.rawValue {
            switch trigger {
            case 1:
                self.categoryBtnState = CategoryBtnState.grid
                transformPizzaCollecitonView()
            case 2:
                self.categoryBtnState = CategoryBtnState.vert
                transformPizzaCollecitonView()
            default:
                guard let categoryVC = CategoryViewController.viewController() else { return }
                categoryVC.modalPresentationStyle = .overFullScreen
                categoryVC.categoryDelegate = self
                self.present(categoryVC, animated: true, completion: nil)
            }
        }
    }
}
