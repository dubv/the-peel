//
//  CategoryViewController.swift
//  ThePeel
//
//  Created by Gone on 1/17/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var leftCategoriesTableView: UITableView!
    @IBOutlet private weak var rightCategoriesTableView: UITableView!
    
    // MARK: - Properties
    public var pizzaData: [Pizza] = []
    private var categoryPresenter: CategoryPresenter!
    private var homePresenter: HomePresenter!
    public weak var categoryDelegate: PassDataToHome?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        registerCell()
        setupDismissButton(dismissButton)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    // MARK: - Initialization
    static func viewController() -> CategoryViewController? {
        return Helper.getViewController(named: "CategoryViewController", inSb: "Home")
    }
    
    fileprivate func initPresenter() {
        categoryPresenter = CategoryPresenter()
        categoryPresenter.categoryDelegate = self
        homePresenter = HomePresenter()
        homePresenter.homeDelegate = self
    }
    
    fileprivate func callAPI(_ isCall: Bool, value: String) {
        categoryPresenter.queryOrderedCategory(isCall: isCall, sortBy: Constants.sortByCategoryPath, value: value)
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
    }
    
    func registerCell() {
        leftCategoriesTableView.register(CategoryTableViewCell.nib, forCellReuseIdentifier: CategoryTableViewCell.indentifier)
        rightCategoriesTableView.register(CategoryTableViewCell.nib, forCellReuseIdentifier: CategoryTableViewCell.indentifier)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.leftCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == leftCategoriesTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.indentifier, for: indexPath) as? CategoryTableViewCell else { let cell = CategoryTableViewCell(style: .default, reuseIdentifier: CategoryTableViewCell.indentifier)
                return cell }
            cell.data = Constants.leftCategories[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        
        if tableView == rightCategoriesTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.indentifier, for: indexPath) as? CategoryTableViewCell else { let cell = CategoryTableViewCell(style: .default, reuseIdentifier: CategoryTableViewCell.indentifier)
                return cell }
            cell.data = Constants.rightCategories[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == leftCategoriesTableView {
            if let value = Constants.leftCategories[indexPath.row].title {
                value == "All" ? self.homePresenter.getAllPizza(isCall: true) : callAPI(true, value: value)
                self.dismissVCNormal()
            }
        }
        
        if tableView == rightCategoriesTableView {
            if let value = Constants.rightCategories[indexPath.row].title {
                callAPI(true, value: value)
                self.dismissVCNormal()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
}

// MARK: - CategoryDelegate, HomeDelegate
extension CategoryViewController: CategoryDelegate, HomeDelegate {
    func getAllPizza(pizza: [Pizza]) {
        categoryDelegate?.passingData(pizza)
    }
    
    func getDataSortCategoryFinished(_ pizza: [Pizza]) {
        categoryDelegate?.passingData(pizza)
    }
}
