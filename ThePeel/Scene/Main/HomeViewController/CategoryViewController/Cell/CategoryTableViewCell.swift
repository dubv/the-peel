//
//  CategoryTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/17/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var iconCategory: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var data: IconMenu! {
        didSet {
            iconCategory.image = data.iconImage
            titleLabel.text = data.title
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
