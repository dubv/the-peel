//
//  ProductTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/12/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

class PizzaTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var mainContainImageView: UIView!
    @IBOutlet fileprivate weak var orderButton: CustomAddToCartButton!
    @IBOutlet fileprivate weak var countLabel: UILabel!
    @IBOutlet fileprivate weak var increaseButton: UIButton!
    @IBOutlet fileprivate weak var decreaseButton: UIButton!
    @IBOutlet fileprivate weak var pizzaImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var compsitionTextView: UITextView!
    @IBOutlet fileprivate weak var sizeLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    
    // MARK: - Properties
    fileprivate var isZooming = false
    fileprivate var originalImageCenter: CGPoint?
    public var didIncreaseVolume:(() -> Void)?
    public var didDecreaseVolume:(() -> Void)?
    public var count: Int? {
        didSet {
            guard let count = self.count else { return }
            handleProductCount(count: count)
        }
    }
    weak var addProductToCartDelegate: AddProductToCartDelegate?
    weak var pinchingImageViewDelegate: PinchingImageViewDelegate?
    public var productCount: Int = 0
    public var pizza: Pizza! {
        didSet {
            guard let pizza = pizza else { return }
            if let link = pizza.image.first?.urlImg {
                guard let url = URL(string: link) else { return }
                pizzaImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            }
            titleLabel.text = pizza.name
            compsitionTextView.text = pizza.detail?.composition
            if let price = pizza.detail?.description?.price {
                priceLabel.text = moneyLocale(input: price)
            }
            if let size = pizza.detail?.description?.size {
                sizeLabel.text = String(describing: "Size: " + size)
            }
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        orderButton.delegate = self
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    fileprivate func setupUI() {
        self.clipsToBounds = false
        self.selectionStyle = .none
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pincher(sender:)))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(sender:)))
        pinch.delegate = self
        pan.delegate = self
        pizzaImageView.addGestureRecognizer(pinch)
        pizzaImageView.addGestureRecognizer(pan)
        pizzaImageView.isUserInteractionEnabled = true
        countLabel.layer.borderColor = UIColor.lightGray.cgColor
        orderButton.layer.borderColor = UIColor.lightGray.cgColor
        increaseButton.layer.borderColor = UIColor.lightGray.cgColor
        decreaseButton.layer.borderColor = UIColor.lightGray.cgColor
        countLabel.layer.borderWidth = 0.6
        orderButton.layer.borderWidth = 0.6
        increaseButton.layer.borderWidth = 0.6
        decreaseButton.layer.borderWidth = 0.6
    }
    
    // MARK: - Private Method
    fileprivate func handleProductCount(count: Int) {
        self.countLabel.text = String(count)
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    // MARK: - Public Method
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Target
    @objc func pan(sender: UIPanGestureRecognizer) {
        if self.isZooming && sender.state == .began {
            self.originalImageCenter = sender.view?.center
        } else if self.isZooming && sender.state == .changed {
            let translation = sender.translation(in: mainContainImageView)
            if let view = sender.view {
                view.center = CGPoint(x:view.center.x + translation.x,
                                      y:view.center.y + translation.y)
            }
            sender.setTranslation(CGPoint.zero, in: self.pizzaImageView.superview)
        }
    }
    
    @objc func pincher(sender:UIPinchGestureRecognizer) {
        if sender.state == .began {
            let currentScale = self.pizzaImageView.frame.size.width / self.pizzaImageView.bounds.size.width
            let newScale = currentScale*sender.scale
            if newScale > 1 {
                self.isZooming = true
            }
        } else if sender.state == .changed {
            let currentScale = self.pizzaImageView.frame.size.width / self.pizzaImageView.bounds.size.width
            var newScale = currentScale*sender.scale
            if newScale < 1 {
                newScale = 1
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                self.pizzaImageView.transform = transform
                sender.scale = 1
            }
            if newScale > 1.8 {
                newScale = 1.8
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                self.pizzaImageView.transform = transform
                sender.scale = 1

            }
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            guard let center = self.originalImageCenter else { return }
            UIView.animate(withDuration: 0.3, animations: {
                self.pizzaImageView.transform = CGAffineTransform.identity
                self.pizzaImageView.center = center
                self.pizzaImageView.frame = CGRect(x: 0, y: 0, width: self.mainContainImageView.frame.width, height: self.mainContainImageView.frame.height)
            }, completion: { _ in
                self.isZooming = false
            })
        }
    }
    
    // MARK: - IBAction
    @IBAction func increaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount < 10 {
            self.count = cellCount + 1
            didIncreaseVolume?()
        }
    }
    
    @IBAction func decreaseButton(_ sender: Any) {
        if let cellCount = self.count, cellCount > 1 {
            self.count = cellCount - 1
            didDecreaseVolume?()
        }
    }
}

// MARK: - AddToCartButtonDelegate
extension PizzaTableViewCell: AddToCartButtonDelegate {
    func buttonIn() {
        guard let quanlity = self.count, let price = pizza.detail?.description?.price, let size = pizza.detail?.description?.size, let desc = pizza.detail?.composition else { return }
        
        if let link = pizza.image.first?.urlImg {
            addProductToCartDelegate?.saveProduct(pizza.id, String(quanlity), link, name: pizza.name, price: price, size: size, desc: desc)
        }
    }
}
