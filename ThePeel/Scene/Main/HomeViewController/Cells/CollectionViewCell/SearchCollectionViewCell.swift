//
//  SearchCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
