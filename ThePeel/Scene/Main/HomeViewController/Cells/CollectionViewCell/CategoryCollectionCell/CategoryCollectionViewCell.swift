//
//  CategoryCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialButtons_ButtonThemer
import MaterialComponents.MaterialButtons_ColorThemer

protocol CategoryCollectionDelegate: NSObjectProtocol {
    func getCategories(trigger: Int)
}

enum CategoryBtnState: Int {
    case grid = 1
    case vert
    case sear
}

class CategoryCollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlet
    @IBOutlet weak var mainContainView: UIView!
    @IBOutlet weak var gridCategoryButton: MDCButton!
    @IBOutlet private weak var verticalCategoryButton: MDCButton!
    @IBOutlet private weak var searchCategoryButton: MDCButton!
    public weak var categoryCollectionDelegate: CategoryCollectionDelegate?

    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Private Method
    private func setupUI() {
        gridCategoryButton.setImage(UIImage(named : "iconCategoryGridNormal"), for: .normal)
        gridCategoryButton.setImage(UIImage(named : "iconCategoryGridSelected"), for: .selected)
        verticalCategoryButton.setImage(UIImage(named : "iconCategoryVerticalNormal"), for: .normal)
        verticalCategoryButton.setImage(UIImage(named : "iconCategoryVerticalSelected"), for: .selected)
        searchCategoryButton.setImage(UIImage(named : "iconCategorySearchNormal"), for: .normal)
        searchCategoryButton.setImage(UIImage(named : "iconCategorySearchSelected"), for: .selected)
        gridCategoryButton.isSelected = true
        
        let MDCbuttons: [MDCButton] = [gridCategoryButton, verticalCategoryButton, searchCategoryButton]
        let buttonScheme = MDCButtonScheme()
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.primaryColor = UIColor(hexString: "4D4C4C")
        
        for button in MDCbuttons {
            button.setElevation(ShadowElevation(rawValue: 4), for: .highlighted)
            MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: button)
            MDCOutlinedButtonColorThemer.applySemanticColorScheme(colorScheme, to: button)
            button.setBorderColor(.white, for: .normal)
            button.setBorderWidth(0, for: .normal)
        }
    }
    
    // MARK: - Public Method
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }

    @IBAction func globalButtonAction(_ sender: UIButton) {
        sender.isSelected = true
        let tags = Array(1...3).filter({$0 != sender.tag})
        let buttons = tags.map{ self.contentView.viewWithTag($0) as? UIButton }
        buttons.forEach{ $0?.isSelected = false }
        categoryCollectionDelegate?.getCategories(trigger: sender.tag)
    }
}
