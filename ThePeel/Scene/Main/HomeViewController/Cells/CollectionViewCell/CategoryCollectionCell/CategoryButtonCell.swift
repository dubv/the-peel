//
//  CategoryButtonCell.swift
//  ThePeel
//
//  Created by Gone on 2/20/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialButtons_ButtonThemer
import MaterialComponents.MaterialButtons_ColorThemer

protocol CategoryButtonCellDelegate: NSObjectProtocol {
    func categoryAction()
}

class CategoryButtonCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var mainContainButtonView: UIView!
    @IBOutlet public weak var categoriesButton: MDCButton!
    
    // MARK: - Properties
    weak var categoryButtonCellDelegate: CategoryButtonCellDelegate?
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    private func setupUI() {
        let shadowSize: CGFloat = mainContainButtonView.frame.size.height
        let contactRect = CGRect(x: 0, y: shadowSize*0.2, width: mainContainButtonView.frame.size.width*2, height: shadowSize*0.82)
        mainContainButtonView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        mainContainButtonView.layer.shadowPath = UIBezierPath(roundedRect: contactRect, cornerRadius: 0).cgPath
        mainContainButtonView.layer.shadowOpacity = 0.4
        mainContainButtonView.layer.shadowRadius = 4
        self.clipsToBounds = false
        let buttonScheme = MDCButtonScheme()
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.primaryColor = UIColor(hexString: "4D4C4C")
        categoriesButton.setElevation(ShadowElevation(rawValue: 4), for: .highlighted)
        MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: categoriesButton)
        MDCOutlinedButtonColorThemer.applySemanticColorScheme(colorScheme, to: categoriesButton)
        categoriesButton.setBorderColor(.white, for: .normal)
        categoriesButton.setBorderWidth(0, for: .normal)
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.indentifier, bundle: nil)
    }
    
    // MARK: - IBAction
    @IBAction func categoriesButton(_ sender: Any) {
        categoryButtonCellDelegate?.categoryAction()
    }
}
