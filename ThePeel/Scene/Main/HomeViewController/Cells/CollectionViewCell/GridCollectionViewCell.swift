//
//  GridCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainView: UIView!
    @IBOutlet private weak var mainContainImageView: UIView!
    @IBOutlet private weak var pizzaImageView: UIImageView!
    @IBOutlet private weak var mainContainTitleView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var likeCountLabel: UILabel!
    @IBOutlet private weak var likeIcon: UIImageView!
    
    // MARK: - Properties
    var pizza: Pizza! {
        didSet {
            guard let pizza = pizza else { return }
            if let link = pizza.image.first?.urlImg {
                guard let url = URL(string: link) else { return }
                pizzaImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            }
            titleLabel.text = pizza.name
            if let price = pizza.detail?.description?.price {
                priceLabel.text = moneyLocale(input: price)
            }
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = false
        mainContainView.layer.cornerRadius = 10
        mainContainTitleView.clipsToBounds = true
        mainContainView.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        mainContainView.layer.shadowOffset = CGSize(width: 2, height: 2)
        mainContainView.layer.shadowOpacity = 0.15
        mainContainView.layer.shadowRadius = 3
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        mainContainTitleView.layer.insertSublayer(gradientLayer, at: 0)
    }

    // MARK: - Private Method
    public func handleLikeCount(count: String, likeByUser: [String: Bool]? = nil) {
        likeCountLabel.text = count
        if let uuid = UUID.uuid, likeByUser?[uuid] == true {
            likeIcon.image = UIImage(named: "iconLike")
        } else {
            likeIcon.image = UIImage(named: "iconUnlike")
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        
        // Specify you want _full width_
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        
        // Calculate the size (height) using Auto Layout
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        
        // Assign the new size to the layout attributes
        autoLayoutAttributes.frame = autoLayoutFrame
        return autoLayoutAttributes
    }
}
