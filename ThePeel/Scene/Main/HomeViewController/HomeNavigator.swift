//
//  HomeNavigator.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol HomeNavigator {
    func toHome()
}

struct DefaultHomeNavigator: HomeNavigator {
    private weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toHome() {
        guard let vc = HomeViewController.viewController() else { return }
        navigation?.pushViewController(vc, animated: true)
    }
}
