//
//  NotificationNavigator.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol NotificationNavigator {
    func toNotification()
}

struct DefaultNotificationNavigator: NotificationNavigator {
    private weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toNotification() {
        guard let vc = NotificationViewController.viewController() else {
            return
        }
//        vc.callAPI()
        self.navigation?.pushViewController(vc, animated: true)
    }
}
