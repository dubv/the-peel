//
//  NotifisTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/4/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class NotifisTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var topicImageView: UIImageView!
    @IBOutlet private weak var titleNotifisLabel: UILabel!
    @IBOutlet private weak var contentNotifisLabel: UILabel!
    @IBOutlet private weak var dateTimeLabel: UILabel!
    
    // MARK: - Properties
    public var notifis: Notifications! {
        didSet {
            notifis.topic == notificationsTopic[1] ? (topicImageView.image = UIImage(named: "iconNotifisDelivered")) : (topicImageView.image = UIImage(named: "iconNotifisNews"))
            
            notifis.read ? setContentColor(color: "FFFFFF") : setContentColor(color: "EAF5FF")
            
            titleNotifisLabel.text = notifis.title
            contentNotifisLabel.text = notifis.content
            
            guard let notifisTime = UInt64(notifis.dateTime) else { return }
            dateTimeLabel.text = converTickToTimeslive(tickFromPass: notifisTime)
        }
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    // MARK: - Initialization
    private func setContentColor(color: String) {
        self.contentView.backgroundColor = UIColor(hexString: color)
    }
    
    // MARK: - Life Cycle
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
