//
//  NotificationViewController.swift
//  ThePeel
//
//  Created by Gone on 2/27/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var notifisTableView: UITableView!
    
    // MARK: - Properties
    public var notifis = [Notifications]()
    private var notifisPresenter: NotificationPresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        registerCell()
        initPresenter()
        setupEmtyViews()
        navigationItem.title = Navigation.notifisTitle
        if isNetworkAvailable() == true {}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callAPI()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .never
            self.navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    // MARK: - Initialization
    static func viewController() -> NotificationViewController? {
        return Helper.getViewController(named: "NotificationViewController", inSb: "Notification")
    }
    
    private func setupUI() {
        setupLightNavigationBar()
    }
    
    private func initPresenter() {
        notifisPresenter = NotificationPresenter()
        notifisPresenter.notificationDelegate = self
    }
    
    public func callAPI() {
        showBlackLoadingSpinner()
        notifisPresenter.getAllNotifis(isCall: true)
        notifisPresenter.getNotifisUnread(isCall: true, value: false)
    }
    
    // MARK: - Private Method
    fileprivate func registerCell() {
        notifisTableView.register(NotifisTableViewCell.nib, forCellReuseIdentifier: NotifisTableViewCell.indentifier)
    }
    
    private func setupEmtyViews() {
        if UUID.uuid == nil {
            notifisTableView.tableFooterView = setupEmptyNotifiView()
        }
    }
    
    // MARK: - Public Method
    public func getItemCheckoutedFromNotifisId(idItem: String) {
        showBlackLoadingSpinner()
        notifisPresenter.getItemCheckoutedBaseId(isCall: true, itemId: idItem)
    }
    
    public func getProductFromNotifisId(idItem: String) {
        showBlackLoadingSpinner()
        notifisPresenter.getPizzaBaseId(isCall: true, itemId: idItem)
    }
}

// MARK: - NotificationDelegate
extension NotificationViewController: NotificationDelegate {
    func getNotifisUnreadFinished(count: Int) {
        count != 0 ? self.tabBarController?.increaseBadge(indexOfTab: 2, num: "\(count)") : self.tabBarController?.removeBadge(indexOfTab: 2)
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func getNotifisFinished(notifis: [Notifications]) {
        let dataReversed = Array(notifis.reversed())
        self.notifis = dataReversed
        let reView = UIView()
        notifisTableView.tableFooterView = reView
        notifisTableView.tableFooterView?.isHidden = true
        if self.notifis.isEmpty == true {
            notifisTableView.tableFooterView?.isHidden = false
            notifisTableView.tableFooterView = setupEmptyNotifiView()
        }
        notifisTableView.reloadData()
        hideBlackLoadingSpinner()
    }
    
    func getNotifisFailed() {
        self.notifis.removeAll()
        notifisTableView.reloadData()
        notifisTableView.tableFooterView?.isHidden = false
        notifisTableView.tableFooterView = setupEmptyNotifiView()
        hideBlackLoadingSpinner()
        self.tabBarController?.removeBadge(indexOfTab: 2)
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func getItemCheckoutedFinished(item: ItemPayment) {
        guard let checkoutVC = CheckoutViewController.viewController() else { return }
        self.pushViewController(viewController: checkoutVC)
        checkoutVC.binding(itemPayment: item)
        hideBlackLoadingSpinner()
    }
    
    func getItemCheckoutedFailed(mess: String) {
        hideBlackLoadingSpinner()
        print("NotificationViewController -- get itemCheckouted failed")
        self.showAlert(GlobalText.TITLE_SORRY_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func getProductFinished(product: Any) {
        guard let pizza = product as? Pizza else { return }
        guard let productVC = ProductViewController.viewController() else { return }
        self.presentVC(viewController: productVC)
        productVC.bindingFromNotifis(pizza: pizza)
        hideBlackLoadingSpinner()
    }
    
    func getProductFailed() {
        hideBlackLoadingSpinner()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
//swiftlint:disable force_unwrapping
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotifisTableViewCell.indentifier, for: indexPath) as? NotifisTableViewCell else { let cell = NotifisTableViewCell(style: .default, reuseIdentifier: NotifisTableViewCell.indentifier)
            return cell }
        cell.selectionStyle = .none
        cell.notifis = self.notifis[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        notifis[indexPath.row].ref?.updateChildValues(["read": true])
        switch notifis[indexPath.row].topic {
        case notificationsTopic[1]:
            self.getItemCheckoutedFromNotifisId(idItem: self.notifis[indexPath.row].topicId)
        case notificationsTopic[2]:
            self.getProductFromNotifisId(idItem: self.notifis[indexPath.row].topicId)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .normal, title: notificationsStatus[1]) { _, indexPath in
            self.notifis[indexPath.row].ref?.removeValue()
        }
        delete.backgroundColor = UIColor(hexString: notificationsColors[1]!)
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
