//
//  CoverageAreaViewController.swift
//  ThePeel
//
//  Created by Gone on 2/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import GoogleMaps
import GooglePlaces

class CoverageAreaViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainContainAreaView: UIView!
    @IBOutlet private weak var customLargerNavigation: UIView!
    @IBOutlet private var infoView: UIView!
    @IBOutlet private var userCurrView: UIView!
    @IBOutlet private weak var distanceLabel: UILabel!
    // MARK: - Properties
    fileprivate var centerMapCoordinate: CLLocationCoordinate2D!
    fileprivate var peelPostion = Location(x: 16.062644, y: 108.220892)
    private var polyline: GMSPolyline!
    private var sessionManager: SessionManager!
    private var locationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    private var mapView: GMSMapView!
    private var placesClient: GMSPlacesClient!
    private var zoomLevel: Float = 15.0
    private var likelyPlaces: [GMSPlace] = []
    private var selectedPlace: GMSPlace?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = GMSMapView()
        sessionManager = SessionManager()
        mapView.delegate = self
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        placesClient = GMSPlacesClient.shared()
        setupUIs()
    }
    
    // MARK: - Initialization
    static func viewController() -> CoverageAreaViewController? {
        return Helper.getViewController(named: "CoverageAreaViewController", inSb: "CoverageArea")
    }
    
    fileprivate func setupUIs() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture(sender:)))
        pan.delegate = self
        customLargerNavigation.addGestureRecognizer(pan)
        let camera = GMSCameraPosition.camera(withLatitude: peelPostion.x,
                                              longitude: peelPostion.y,
                                              zoom: zoomLevel)
        constraintMapView(camera: camera)
        // Add the map to the view, hide it until we've got a location update.
        mapView.isHidden = true
    }
    
    private func constraintMapView(camera: GMSCameraPosition) {
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //Settings
        mapView.settings.compassButton = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        mapView.settings.zoomGestures = true
        mapView.settings.tiltGestures = true
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.isBuildingsEnabled = true
        //Constraint
        mainContainAreaView.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.leadingAnchor.constraint(equalTo: mainContainAreaView.leadingAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: mainContainAreaView.rightAnchor).isActive = true
        mapView.topAnchor.constraint(equalTo: mainContainAreaView.topAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: mainContainAreaView.bottomAnchor).isActive = true
        showMarker()
    }
    
    fileprivate func showMarker() {
        let position = CLLocationCoordinate2D(latitude: peelPostion.x, longitude: peelPostion.y)
        DispatchQueue.main.async {
            let marker = GMSMarker()
            marker.position = position
            marker.map = self.mapView
        }
    }
    
    fileprivate func drawPolyline(currentLocation: CLLocationCoordinate2D) {
        let end = CLLocationCoordinate2D(latitude: peelPostion.x, longitude: peelPostion.y)
        sessionManager.requestDirections(from: currentLocation, to: end, completionHandler: { (path, error) in
            if let error = error{
                print("Something went wrong, abort drawing!\nError: \(error)")
            } else {
                if let paths = path  {
                    self.polyline = GMSPolyline(path: paths)
                    self.sessionManager.set(polyline: self.polyline, on: self.mapView)
                }
            }
        })
    }
    
    fileprivate func isInDeliveryRange(location: CLLocation) {
        let coordinate = location
        let coordinateFrom = CLLocation(latitude: peelPostion.x, longitude: peelPostion.y)
        let distanceInMeters = coordinate.distance(from: coordinateFrom)
        let distance = String(format: "%.2f", Double(round(100*Double(distanceInMeters))/100) / 1000)
        self.distanceLabel.text = distance + " km"
    }
}

extension CoverageAreaViewController: GMSMapViewDelegate {
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways:
            print("Location status is OK.")
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension CoverageAreaViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - CLLocationManagerDelegate
extension CoverageAreaViewController: CLLocationManagerDelegate {
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = locations.last else { return }
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            constraintMapView(camera: camera)
        } else {
            mapView.animate(to: camera)
        }
        drawPolyline(currentLocation: camera.target)
        isInDeliveryRange(location: location)
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
