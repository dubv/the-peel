//
//  OnboardingViewController.swift
//  ThePeel
//
//  Created by Gone on 3/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseAuth

class OnboardingViewController: UIViewController {
    // MARK: - Properties
    fileprivate var mainContainView: OnboardingSubView!
    fileprivate var onboardingPresenter: OnboardingPreseneter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupOnboarding()
        initPresenter()
    }
    
    // MARK: - Initialization
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func initPresenter() {
        onboardingPresenter = OnboardingPreseneter()
        onboardingPresenter.onboardingDelegate = self
    }
    
    static func viewController() -> OnboardingViewController? {
        return Helper.getViewController(named: "OnboardingViewController", inSb: "Onboarding")
    }
    
    fileprivate func setupOnboarding() {
        guard isViewLoaded else { return }
        mainContainView = OnboardingSubView()
        mainContainView.onboardingSubViewDelegate = self
        mainContainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(mainContainView)
    }

    // MARK: - Target
}

// MARK: - OnboardingSubViewDelegate, OnboardingDelegate
extension OnboardingViewController: OnboardingSubViewDelegate, OnboardingDelegate {
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
}
