//
//  OnboardingSubView.swift
//  ThePeel
//
//  Created by Gone on 3/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialButtons_ButtonThemer
import MaterialComponents.MaterialButtons_ColorThemer

protocol OnboardingSubViewDelegate: NSObjectProtocol {
    func dismiss()
}

class OnboardingSubView: UIView {
    @IBOutlet private weak var mainContainOnboardingView: UIView!
    @IBOutlet private weak var mainContainCollectionView: UIView!
    @IBOutlet private weak var mainContainCollectionViewCell: UIView!
    @IBOutlet private weak var mainContainButtonView: UIView!
    @IBOutlet private weak var onboardingCollectionView: UICollectionView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    private var indexPath: IndexPath?
    private var index: Int = 0
    private var trigger: Bool = false
    weak var onboardingSubViewDelegate: OnboardingSubViewDelegate?
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainContainCollectionView.alpha = 0.0
        setupCollectionView()
        setupUI()
    }
    
    // MARK: - Private Method
    private func setupCollectionView() {
        onboardingCollectionView.register(OnboardingCollectionViewCell.nib, forCellWithReuseIdentifier: OnboardingCollectionViewCell.indentifier)
        onboardingCollectionView.delegate = self
        onboardingCollectionView.dataSource = self
        
        guard let flowLayout = onboardingCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        let height = UIScreen.main.bounds.height*0.57
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width*0.75, height: height*0.6)
        flowLayout.minimumLineSpacing = 0
    }
    
    private func setupUI() {
        self.clipsToBounds = true
        transformUI()
        mainContainCollectionView.layer.cornerRadius = 10
        mainContainButtonView.layer.cornerRadius = 4
        
        let shadowSize: CGFloat = mainContainCollectionView.frame.size.height
        let contactRect = CGRect(x: 0, y: shadowSize*0.2, width: mainContainCollectionView.frame.size.width, height: shadowSize*0.82)
        mainContainCollectionView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        mainContainCollectionView.layer.shadowPath = UIBezierPath(roundedRect: contactRect, cornerRadius: 0).cgPath
        mainContainCollectionView.layer.shadowOpacity = 0.4
        mainContainCollectionView.layer.shadowRadius = 4
        
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { [weak self] _ in
            guard let `self` = self else { return }
            self.mainContainCollectionView.alpha = 1
        }
    }
    
    private func transformUI() {
        titleLabel.text = Constants.onboardingData[index].title
        contentLabel.text = Constants.onboardingData[index].content
    }
    
    private func scrollOnboardingCollectionView(indexPath: IndexPath) {
        onboardingCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        transformUI()
        if indexPath.item == 2, trigger == false {
            trigger = !trigger
            print("hh")
            nextButton.setTitle("DONE", for: .normal)
            nextButton.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        }
    }

    // MARK: - IBAction
    @IBAction func nextButton(_ sender: Any) {
        if index < Constants.onboardingData.count, trigger == false  {
            index += 1
            scrollOnboardingCollectionView(indexPath: IndexPath(item: index, section: 0))
        }
    }
    
    // MARK: - Target
    @objc func dismiss() {
        onboardingSubViewDelegate?.dismiss()
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate
extension OnboardingSubView: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return Constants.onboardingData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnboardingCollectionViewCell.indentifier, for: indexPath) as? OnboardingCollectionViewCell else {return UICollectionViewCell()}
        cell.images = Constants.onboardingData[indexPath.row].image
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = onboardingCollectionView.contentOffset
        visibleRect.size = onboardingCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = onboardingCollectionView.indexPathForItem(at: visiblePoint) else { return }
        self.indexPath = indexPath
        index = indexPath.item
        scrollOnboardingCollectionView(indexPath: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
