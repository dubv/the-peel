//
//  OnboardingCollectionViewCell.swift
//  ThePeel
//
//  Created by Gone on 3/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    
    // MARK: - Properties
    public var images: UIImage? {
        didSet {
            guard let image = images else { return }
            coverImageView.image = image
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    private func setupUI() {
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
