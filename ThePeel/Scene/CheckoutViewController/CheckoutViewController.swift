//
//  CheckoutViewController.swift
//  ThePeel
//
//  Created by Gone on 2/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class CheckoutViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var checkoutTableView: UITableView!
    @IBOutlet private var totalView: UIView!
    @IBOutlet private weak var subtotalLabel: UILabel!
    @IBOutlet private weak var priceTotalLabel: UILabel!
    @IBOutlet private weak var toPaymentButton: ToPaymentButton!
    // MARK: - Properties
    private var personalData = [ProfileCell]()
    private var userInfo: CustomUser?
    private var profilePresenter: ProfilePresenter!
    private var editProfilePresenter: EditProfilePresenter!
    private var checkoutPresenter: CheckoutPresenter!
    public var cartData = [Cart]()
    public var notifications: Notifications!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        callAPI()
        baseViewControllerDelegate = self
        checkPasswordViewDelegate = self
        setupBackButton()
        registerCell()
    }
    
    // MARK: - Initialization
    static func viewController() -> CheckoutViewController? {
        return Helper.getViewController(named: "CheckoutViewController", inSb: "Checkout")
    }
    
    func setupSectionBG(view: UIView) {
        view.backgroundColor = UIColor(hexString: GlobalColor.grayTableView)
    }
    
    func setupUI() {
        checkoutTableView.tableFooterView = totalView
        totalPayment()
    }
    
    ///For pushView from Notifications Screen
    public func binding(itemPayment: ItemPayment?) {
        guard let itemPay = itemPayment else { return }
        self.userInfo = CustomUser(key: itemPay.key, avatar: "", descriptions: "", externalLink: "", firstName: itemPay.customerName, lastName: "", userName: "", email: itemPay.email, password: "", phoneNumber: itemPay.phoneNumber, address: itemPay.address, cart: nil, comment: [Comment]())
        self.personalData = [ProfileCell(largerTitle: Constants.profileNameEF, detailTitle: itemPay.customerName), ProfileCell(largerTitle: Constants.contactEmaiL, detailTitle: itemPay.email)]
        for cart in itemPay.cart {
            self.cartData.append(cart)
        }
        navigationItem.title = "\(itemPay.totalPayment)"
        navigationItem.prompt = "Status: \(itemPay.status)"
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .automatic
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.tabBarItem.title = nil
        }
        totalView.isHidden = true
    }
    
    public func totalPayment() {
        var totalProductInArray = 0.0
        var itemSubtotal = 0
        for cart in self.cartData {
            guard let quanlity = Double(cart.quanlity), let price = Double(cart.price) else { return }
            itemSubtotal += Int(quanlity)
            totalProductInArray += price*quanlity
        }
        self.priceTotalLabel.text = "\(moneyCalculateConvert(input: totalProductInArray))"
        subtotalLabel.text = "Subtotal (\(itemSubtotal == 1 ? " item" : " items"))"
    }
    
    private func initPresenter() {
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
        checkoutPresenter = CheckoutPresenter()
        checkoutPresenter.checkoutDelegate = self
        editProfilePresenter = EditProfilePresenter()
        editProfilePresenter.editProfileDelegate = self
    }

    // MARK: - Private Method
    private func callAPI() {
        profilePresenter.getUserInfo(true)
    }
    
    private func binding(){
        let ticks = Date().ticks
        let idItemPayment = String(ticks)
        let date = Date(ticks: ticks)
        dateFormatter.dateFormat = Constants.dateDetailFormat
        let dateTime = dateFormatter.string(from: date)
        guard let firstName = userInfo?.firstName, let lastName = userInfo?.lastName, let address = userInfo?.address, let phoneNumber = userInfo?.phoneNumber, let email = userInfo?.email, let priceTotal = priceTotalLabel.text, let defaultStatus = orderItemStatus[1] else { return }
        for cartDataChange in 0..<self.cartData.count {
            self.cartData[cartDataChange].idItem = idItemPayment
        }
        let itemPayment = ItemPayment(key: idItemPayment, customerName: firstName+lastName, address: address, phoneNumber: phoneNumber, email: email, cart: self.cartData, totalPayment: priceTotal, status: defaultStatus, dateTime: dateTime)
        checkoutPresenter.addItemPayment(isCall: true, object: itemPayment)
    }
    
    fileprivate func pushNotifis(id: String, object: ItemPayment) {
        guard let uid = UUID.uuid else { return }
        self.notifications = Notifications(id: id, uid: uid, topic: "newOrder", topicId: id, title: "New Order", content: "\(object.customerName) have just ordered an item #\(id)", dateTime: id, read: false)
        self.checkoutPresenter.pushNotification(isCall: true, object: self.notifications)
    }
    
    private func registerCell() {
        checkoutTableView.register(ShipToTableViewCell.nib, forCellReuseIdentifier: ShipToTableViewCell.indentifier)
        checkoutTableView.register(PersonalDetailTableViewCell.nib, forCellReuseIdentifier: PersonalDetailTableViewCell.indentifier)
        checkoutTableView.register(ItemCheckoutTableViewCell.nib, forCellReuseIdentifier: ItemCheckoutTableViewCell.indentifier)
    }
    
    // MARK: - IBAction
    @IBAction func toPaymentButton(_ sender: Any) {
        if self.isNetworkAvailable() == true {
            self.showCheckPassAlert()
        }
    }
}

// MARK: - ProfileDelegate, CheckoutDelegate, PopViewFromBaseViewDelegate, CheckPasswordViewDelegate
extension CheckoutViewController: ProfileDelegate, CheckoutDelegate, BaseViewControllerDelegate, CheckPasswordViewDelegate, EditProfileDelegate {
    func checkPass(pass: String) {
        editProfilePresenter.checkPassword(isCall: true, password: pass)
    }
    
    func verifyPasswordFailed(_ mess: String) {
        self.showAlert(GlobalText.TITLE_ERROR_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func verifyPasswordFinished(_ mess: String) {
        binding()
    }
    
    func popView() {
        self.popViewControler()
    }

    func addItemPaymentFinished(mess: String, object: ItemPayment) {
        self.showAlert(GlobalText.TITLE_PAYMENT_FINISHED, mess, GlobalText.TITLE_BUTTON_OKAY)
        pushNotifis(id: object.key, object: object)
    }
    
    func addItemPaymentFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_SORRY_GLOBAL, mess, GlobalText.TITLE_BUTTON_OKAY)
    }
    
    func getUserInfoFinished(info: CustomUser) {
        self.userInfo = info
        self.personalData = [ProfileCell(largerTitle: Constants.profileNameEF, detailTitle: info.userName), ProfileCell(largerTitle: Constants.contactEmaiL, detailTitle: info.email)]
        setupUI()
        checkoutTableView.reloadData()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension CheckoutViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.personalData.count : self.cartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ShipToTableViewCell.indentifier, for: indexPath) as? ShipToTableViewCell else { let cell = ShipToTableViewCell(style: .default, reuseIdentifier: ShipToTableViewCell.indentifier)
                    return cell }
                cell.shipDetail = userInfo
                cell.selectionStyle = .none
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: PersonalDetailTableViewCell.indentifier, for: indexPath) as? PersonalDetailTableViewCell, indexPath.row < personalData.count else { let cell = PersonalDetailTableViewCell(style: .default, reuseIdentifier: PersonalDetailTableViewCell.indentifier)
                    return cell }
                cell.personalData = personalData[indexPath.row]
                cell.selectionStyle = .none
                return cell
            }
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemCheckoutTableViewCell.indentifier, for: indexPath) as? ItemCheckoutTableViewCell, indexPath.row < cartData.count else { let cell = ItemCheckoutTableViewCell(style: .default, reuseIdentifier: ItemCheckoutTableViewCell.indentifier)
                return cell }
            cell.selectionStyle = .none
            cell.cartInfo = self.cartData[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableView.automaticDimension
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView()
        setupSectionBG(view: sectionView)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return !(indexPath.section == 0)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cartData.remove(at: indexPath.row)
            self.checkoutTableView.reloadData()
            self.totalPayment()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 10
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.checkoutSectionTitle.count
    }
}
