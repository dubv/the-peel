//
//  PersonalDetailTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class PersonalDetailTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var largerTitleLabel: UILabel!
    @IBOutlet fileprivate weak var detailTitleLabel: UILabel!
    
    // MARK: - Properties
    var personalData: ProfileCell! {
        didSet {
            guard let data = personalData else { return }
            largerTitleLabel.text = data.largerTitle
            detailTitleLabel.text = data.detailTitle
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
    
}
