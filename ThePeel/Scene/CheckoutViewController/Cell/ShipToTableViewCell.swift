//
//  ShipToTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ShipToTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var surrNameLabel: UILabel!
    @IBOutlet fileprivate weak var addressLabel: UILabel!
    @IBOutlet fileprivate weak var phoneNoLabel: UILabel!
    
    // MARK: - Properties
    var shipDetail: CustomUser! {
        didSet {
            guard let shipDetail = shipDetail else { return }
            let surrName = "\(shipDetail.firstName) " + "\(shipDetail.lastName)"
            surrNameLabel.text = surrName
            addressLabel.text = shipDetail.address
            phoneNoLabel.text = shipDetail.phoneNumber
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
}
