//
//  ItemCheckoutTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 2/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ItemCheckoutTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var coverProductImageView: UIImageView!
    @IBOutlet fileprivate weak var totalLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    @IBOutlet fileprivate weak var dateTimeLabel: UILabel!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var cartInfo: Cart! {
        didSet {
            guard let url = URL(string: cartInfo.coverImage) else { return }
            coverProductImageView.kf.setImage(with: url, options: [.transition(.fade(0.3))])
            
            titleLabel.text = cartInfo.productName
            priceLabel.text = moneyLocale(input: cartInfo.price)
            dateTimeLabel.text = cartInfo.dateTime
            
            guard let quanlity = Double(cartInfo.quanlity), let price = Double(cartInfo.price) else { return }
            let totalPayment = quanlity*price
            
            quanlity == 1 ? (totalLabel.text = "\(cartInfo.quanlity) item,  total:  \(moneyLocale(input: String(totalPayment)))") : (totalLabel.text = "\(cartInfo.quanlity) items,  total: \(moneyLocale(input: String(totalPayment)))")
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
