//
//  SignInViewController.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var mainScrollView: UIScrollView!
    @IBOutlet private weak var mainFormContainView: UIView!
    @IBOutlet private weak var formContainView: UIView!
    @IBOutlet public weak var prefixButton: UIButton!
    @IBOutlet public weak var prefixCountryPickerView: UIPickerView!
    @IBOutlet public weak var phoneNumberTextField: CustomTextField!
    @IBOutlet public weak var validateNumberPhoneLabel: UILabel!
    @IBOutlet public weak var passwordTextField: CustomTextField!
    @IBOutlet public weak var validatePasswordLabel: UILabel!
    @IBOutlet public weak var signInButton: UIButton!
    @IBOutlet public weak var fogortButton: UIButton!
    
    // MARK: - Properties
    private var signinPresenter: SignInPresenter!
    private var registerPresenter: RegisterPresenter!
    private var suffCode: String?
    public var verificationID: String?
    public var pickerData : [String] = []
    public var suffixesData: [CountrySuffixes] = []
    public var authen: Any?

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboard()
        initPresenter()
        callLocalData()
        self.prefixCountryPickerView.delegate = self
        self.prefixCountryPickerView.dataSource = self
        setupDismissButton(dismissButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()
        setupPickerView(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        dismissKeyboard()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        validation()
    }
    
    // MARK: - Initialization
    static func viewController() -> SignInViewController? {
        return Helper.getViewController(named: "SignInViewController", inSb: "Authorization")
    }
    
    fileprivate func initPresenter() {
        signinPresenter = SignInPresenter()
        signinPresenter.signinDelegate = self
    }
    
    fileprivate func setupKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.phoneNumberTextField.textField.keyboardType = .numberPad
        phoneNumberTextField.textField.delegate = self
        passwordTextField.textField.delegate = self
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
    }
    
    private func setupPickerView(_ isHidden: Bool) {
        self.prefixCountryPickerView.isHidden = isHidden
    }

    // MARK: - Private Method
    fileprivate func callAPI() {
        showBlackLoadingSpinner()
        guard let password = passwordTextField.textField.text, let phoneNumber = phoneNumberTextField.textField.text else { return }
        signinPresenter.siginWithPassAndNoPhone(isCall: true, phoneNumber: phoneNumber, password: password)
    }
    
    fileprivate func callLocalData() {
        signinPresenter.getPhoneNumberSuffixes(true)
    }

    // MARK: - Target
    @objc func keyboardWillShow( note:NSNotification )
    {
        guard let keyboardFrame = (note.userInfo?[ UIResponder.keyboardFrameEndUserInfoKey ] as? NSValue)?.cgRectValue else { return }
        mainScrollView.contentInset.bottom = view.convert(keyboardFrame, from: nil).size.height
        setupPickerView(true)
    }
    
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.endEditing(true)
            self.mainScrollView.contentInset.bottom = 0
        })
    }
    
    // MARK: - IBAction
    @IBAction func signInAction(_ sender: Any) {
        callAPI()
    }
    
    @IBAction func fogortPasswordAction(_ sender: Any) {
        print("Forgot password")
    }
    
    @IBAction func prefixButtonAction(_ sender: Any) {
        setupPickerView(false)
        dismissKeyboard()
    }
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension SignInViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = self.pickerData[row]
        
        if let customFont =  Constants.OpenSans[2] {
            pickerLabel.font = UIFont(name: customFont, size: 13)
        }
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.prefixButton.setTitle(self.suffixesData[row].dial_code, for: .normal)
    }
}

// MARK: - SignInDelegate
extension SignInViewController: SignInDelegate {
    func siginFinished() {
        self.hideBlackLoadingSpinner()
        guard let phoneNumber = phoneNumberTextField.textField.text, let prefixPhoneNumber = self.prefixButton.titleLabel?.text else { return }
        signinPresenter.verifyNoPhone(isCall: true, phoneNumber: prefixPhoneNumber+phoneNumber)
    }
    
    func siginFailed(mess: String) {
        self.hideBlackLoadingSpinner()
        self.showAlert(GlobalText.TITLE_SIGNIN_FAILED, mess, "Try again")
    }
    
    func getSuffixesFinished(suffixes: [CountrySuffixes]) {
        self.suffixesData = suffixes
        for data in self.suffixesData {
            self.pickerData.append("\(data.code) - \(data.name) [\(data.dial_code)]")
        }
    }
    
    func signInVerificationID(_ id: String) {
        self.verificationID = id
        guard let verifyVC = VerifyViewController.viewController() else { return }
        guard let authen = self.authen as? Authen else { return }
        verifyVC.authen = authen
        verifyVC.verificationID = self.verificationID
        self.hideBlackLoadingSpinner()
        self.present(verifyVC, animated: true)
        verifyVC.didSignIn = { [weak self] in
            guard let `self` = self else { return }
            self.dismissVCNormal()
        }
    }
    
    func signInVerificationIDFailed(_ mess: String) {
        self.hideBlackLoadingSpinner()
        self.showAlert(GlobalText.TITLE_VERIFI_FAILED, mess, "Try again")
    }
}
