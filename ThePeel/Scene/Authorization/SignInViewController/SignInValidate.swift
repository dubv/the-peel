//
//  SignInValidate.swift
//  ThePeel
//
//  Created by Gone on 1/30/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

extension SignInViewController {
    public func validation() {
        guard let password = passwordTextField.textField.text, let phoneNumber = phoneNumberTextField.textField.text, let prefixPhoneNumber = self.prefixButton.titleLabel?.text else { return }
        
        let responsePassword = Validation.shared.validate(values: (ValidationType.password, password))
        let responsePhoneNumber = Validation.shared.validate(values: (ValidationType.phoneNo, phoneNumber))
        self.authen = Authen(phoneNumber: prefixPhoneNumber+phoneNumber, password: password)
        
        let reponsee = Validation.shared.validate(values: (ValidationType.password, password), (ValidationType.phoneNo, phoneNumber))
        
        switch reponsee {
        case .success:
            self.signInButton.backgroundColor = UIColor(hexString: GlobalColor.black)
            self.signInButton.isEnabled = true
        case .failure(_, let mess):
            print(mess)
            self.signInButton.backgroundColor = UIColor(hexString: GlobalColor.gray)
            self.signInButton.isEnabled = false
        }
        
        if !password.isEmpty {
            switch responsePassword {
            case .success:
                self.validateFinish(self.validatePasswordLabel)
            case .failure(_, let message):
                self.validateFailed(message.localized(), self.validatePasswordLabel)
            }
        } else {
            self.validateFailed("", self.validatePasswordLabel)
        }
        
        if !phoneNumber.isEmpty {
            switch responsePhoneNumber {
            case .success:
                self.validateFinish(self.validateNumberPhoneLabel)
            case .failure(_, let message):
                self.validateFailed(message.localized(), self.validateNumberPhoneLabel)
            }
        } else {
            self.validateFailed("", self.validateNumberPhoneLabel)
        }
        
    }
}
