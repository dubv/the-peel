//
//  VerifyViewController.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class VerifyViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var verifyView: UIView!
    @IBOutlet private weak var verifyButton: CustomToAuthButton!
    
    // MARK: - Properties
    lazy var pinCodeView: PinCodeView = {
        let pincode = PinCodeView(frame: CGRect.zero, pinCode: 0)
        return pincode
    }()
    
    lazy var dismissBtn: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "dismissBtnBlack"), for: .normal)
        button.addTarget(self, action: #selector(dissmissView), for: .touchUpInside)
        return button
    }()
    
    public var user: CustomUser!
    public var authen: Authen!
    public var verificationID: String?
    public var pinCode: String?
    public var didSignIn: (() -> Void)?
    public var dontSignIn: (() -> Void)?
    private var verifyPresenter: VerifyPresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        setupDismissButton(dismissBtn)
        self.pinCodeView.pinCodeCount = 6
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.verifyView.addSubview(pinCodeView)
        self.pinCodeView.frame = CGRect(x: 0, y: 0, width: self.verifyView.frame.width, height: self.verifyView.frame.height)
        
        self.pinCodeView.inputComplete = { (completed, pinCode) in
            self.pinCode = pinCode
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupUI()
    }
    
    // MARK: - Initialization
    static func viewController() -> VerifyViewController? {
        return Helper.getViewController(named: "VerifyViewController", inSb: "Authorization")
    }
    
    fileprivate func initPresenter() {
        verifyPresenter = VerifyPresenter()
        verifyPresenter.verifyDelegate = self
        verifyPresenter.registerDelegate = self
        verifyPresenter.signinDelegate = self
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        dismissBtn.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
    }
    
    fileprivate func callAPI() {
        guard let verificationID = self.verificationID, let pinCode = self.pinCode else { return }
        verifyPresenter.verifyNumberPhone(true, verificationID: verificationID, verificationCode: pinCode)
    }
    
    // MARK: - Target
    @objc func dissmissView() {
        self.dismissVCNormal()
    }
    
    // MARK: - IBAction
    @IBAction func verifyButtonAction(_ sender: Any) {
        showBlackLoadingSpinner()
        callAPI()
    }
}

// MARK: - VerifyDelegate, RegisterDelegate, SignInDelegate
extension VerifyViewController: VerifyDelegate, RegisterDelegate, SignInDelegate {
    func verifyFinished(_ uid: String) {
        print(self.user)
        
        if self.user == nil {
            self.hideBlackLoadingSpinner()
            self.dismissVCNormal()
            didSignIn?()
        } else {
            verifyPresenter.register(isCall: true, uid: uid, userName: user.userName, email: user.email, phoneNumber: user.phoneNumber, password: user.password)
        }
    }
    
    func verifyFailed(_ mess: String) {
        self.hideBlackLoadingSpinner()
        self.showAlert(GlobalText.TITLE_VERIFI_FAILED, mess, GlobalText.TITLE_BUTTON_TRY)
    }
    
    func registerFinished(_ email: String) {
        self.hideBlackLoadingSpinner()
        self.dismissDoubleVCNormal()
    }
    
    func registerFailed(mess: String) {
        self.hideBlackLoadingSpinner()
        self.showAlert(GlobalText.TITLE_SIGNUP_FAILED, mess, GlobalText.TITLE_BUTTON_TRY)
    }
}
