//
//  RegisterViewController.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var formSignUpView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var emailValidateLabel: UILabel!
    @IBOutlet weak var userNameTextField: CustomTextField!
    @IBOutlet weak var userValidateLabel: UILabel!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var passwordValidateLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: CustomTextField!
    @IBOutlet weak var prefixPhoneNumberButton: UIButton!
    @IBOutlet weak var phoneNumberValidateLabel: UILabel!
    @IBOutlet weak var prefixPhoneNPickerView: UIPickerView!
    @IBOutlet weak var signupButton: CustomToAuthButton!
    
    // MARK: - Properties
    fileprivate var registerPresenter: RegisterPresenter!
    fileprivate var signinPresenter: SignInPresenter!
    private var pickerData : [String] = []
    private var suffixesData: [CountrySuffixes] = []
    private var suffCode: String?
    public var verificationID: String?
    public var users: Any?
    weak var verifyDelegate: VerifyDelegate?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPresenter()
        setupKeyboard()
        
        callLocalData()
        self.prefixPhoneNPickerView.delegate = self
        self.prefixPhoneNPickerView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupBackButton()
        self.setupLightNavigationBar()
        setupDismissButton(dismissButton)
        setupUI()
        
        setupPickerView(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        dismissKeyboard()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        validation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    // MARK: - Initialization
    static func viewController() -> RegisterViewController? {
        return Helper.getViewController(named: "RegisterViewController", inSb: "Authorization")
    }
    
    fileprivate func initPresenter() {
        registerPresenter = RegisterPresenter()
        registerPresenter.registerDelegate = self
        
        signinPresenter = SignInPresenter()
        signinPresenter.signinDelegate = self
    }
    
    fileprivate func setupKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        emailTextField.textField.delegate = self
        userNameTextField.textField.delegate = self
        passwordTextField.textField.delegate = self
        phoneNumberTextField.textField.delegate = self
        self.phoneNumberTextField.textField.keyboardType = .numberPad
    }
    
    fileprivate func setupPickerView(_ isHidden: Bool) {
        self.prefixPhoneNPickerView.isHidden = isHidden
    }
    
    fileprivate func setupUI() {
        view.backgroundColor = .white
        dismissButton.addTarget(self, action: #selector(dismissVCNormal), for: .touchUpInside)
    }
    
    // MARK: - Private Method
    fileprivate func callAPI() {
        guard let phoneNumber = phoneNumberTextField.textField.text, let prefixPhoneNumber = self.prefixPhoneNumberButton.titleLabel?.text else { return }
        registerPresenter.verifyNumberPhone(isCall: true, phoneNumber: prefixPhoneNumber + phoneNumber)
    }
    
    fileprivate func callLocalData() {
        signinPresenter.getPhoneNumberSuffixes(true)
    }

    // MARK: - IBAction
    @IBAction func signUpButton(_ sender: Any) {
        callAPI()
    }
    
    @IBAction func prefixButtonAction(_ sender: Any) {
        setupPickerView(false)
        dismissKeyboard()
    }

    // MARK: - Target
    @objc func keyboardWillShow( note:NSNotification )
    {
        guard let keyboardFrame = (note.userInfo?[ UIResponder.keyboardFrameEndUserInfoKey ] as? NSValue)?.cgRectValue else { return }
        mainScrollView.contentInset.bottom = view.convert(keyboardFrame, from: nil).size.height
        setupPickerView(true)
    }
    
    @objc func dismissKeyboard() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.endEditing(true)
            self.mainScrollView.contentInset.bottom = 0
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        validation()
        return true
    }
}

extension RegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = self.pickerData[row]
        
        if let customFont = Constants.OpenSans[2] {
            pickerLabel.font = UIFont(name: customFont, size: 13)
        }
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.prefixPhoneNumberButton.setTitle(self.suffixesData[row].dial_code, for: .normal)
    }
}

extension RegisterViewController: RegisterDelegate, SignInDelegate {
    func getSuffixesFinished(suffixes: [CountrySuffixes]) {
        self.suffixesData = suffixes
        for data in self.suffixesData {
            self.pickerData.append("\(data.code) - \(data.name) [\(data.dial_code)]")
        }
    }
    
    func verificationID(_ verificationID: String) {
        self.verificationID = verificationID
        guard let verifyVC = VerifyViewController.viewController() else { return }
        guard let user = self.users as? CustomUser else { return }
        verifyVC.user = user
        verifyVC.verificationID = self.verificationID
        self.present(verifyVC, animated: true)
    }
    
    func verificationIDFailed(_ verificationID: String) {
        self.showAlert(GlobalText.TITLE_SIGNUP_FAILED, verificationID, GlobalText.TITLE_BUTTON_TRY)
    }
    
    func registerFailed(mess: String) {
        self.showAlert(GlobalText.TITLE_SIGNUP_FAILED, mess, GlobalText.TITLE_BUTTON_TRY)
    }
}
