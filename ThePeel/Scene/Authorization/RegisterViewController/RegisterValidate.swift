//
//  RegisterValidate.swift
//  ThePeel
//
//  Created by Gone on 1/28/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

extension RegisterViewController {
    public func validation() {
        guard let email = emailTextField.textField.text, let password = passwordTextField.textField.text, let userName = userNameTextField.textField.text, let phoneNumber = phoneNumberTextField.textField.text else { return }
        
        let responseEmail = Validation.shared.validate(values: (ValidationType.email, email))
        let responsePassword = Validation.shared.validate(values: (ValidationType.password, password))
        let responseUsername = Validation.shared.validate(values: (ValidationType.alphabeticString, userName))
        let responsePhoneNumber = Validation.shared.validate(values: (ValidationType.phoneNo, phoneNumber))
        
        self.users = CustomUser(key: "", avatar: "", descriptions: "", externalLink: "", firstName: "", lastName: "", userName: userName, email: email, password: password, phoneNumber: phoneNumber, address: "", cart: nil, comment: [Comment]())
        
        let reponsee = Validation.shared.validate(values: (ValidationType.email, email), (ValidationType.password, password), (ValidationType.alphabeticString, userName), (ValidationType.phoneNo, phoneNumber))
        
        switch reponsee {
        case .success:
            self.signupButton.backgroundBtn = GlobalColor.black
            self.signupButton.isEnabled = true
        case .failure(_, let mess):
            print(mess)
            self.signupButton.backgroundBtn = GlobalColor.gray
            self.signupButton.isEnabled = false
        }
        
        if !email.isEmpty {
            switch responseEmail {
            case .success:
                validateFinish(self.emailValidateLabel)
            case .failure(_, let message):
                validateFailed(message.localized(), self.emailValidateLabel)
            }
        } else {
            validateFailed("", self.emailValidateLabel)
        }
        
        if !userName.isEmpty {
            switch responseUsername {
            case .success:
                validateFinish(self.userValidateLabel)
            case .failure(_, let message):
                validateFailed(message.localized(), self.userValidateLabel)
            }
        } else {
            validateFailed("", self.userValidateLabel)
        }
        
        if !password.isEmpty {
            switch responsePassword {
            case .success:
                validateFinish(self.passwordValidateLabel)
            case .failure(_, let message):
                validateFailed(message.localized(), self.passwordValidateLabel)
            }
        } else {
            validateFailed("", self.passwordValidateLabel)
        }
        
        if !phoneNumber.isEmpty {
            switch responsePhoneNumber {
            case .success:
                validateFinish(self.phoneNumberValidateLabel)
            case .failure(_, let message):
                validateFailed(message.localized(), self.phoneNumberValidateLabel)
            }
        } else {
            validateFailed("", self.phoneNumberValidateLabel)
        }
        
    }
}
