//
//  CartViewController.swift
//  ThePeel
//
//  Created by Gone on 1/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Reachability
import Kingfisher

class CartViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var cartTableView: UITableView!
    @IBOutlet private var emptyCartView: UIView!
    @IBOutlet private var totalView: UIView!
    @IBOutlet private weak var totalLabel: UILabel!
    @IBOutlet private weak var miniumLabel: UILabel!
    @IBOutlet private weak var miniumOrderLabel: UILabel!
    @IBOutlet private var authButtonView: UIView!
    @IBOutlet private weak var authButton: ToPaymentButton!
    @IBOutlet private weak var toPaymentView: UIView!
    
    // MARK: - Properties
    public var pizzaData = ProductData<Pizza>()
    public var drinkData = ProductData<Drink>()
    public var dessertData = ProductData<Dessert>()
    public var cartData = [Cart]()
    public var profilePresenter: ProfilePresenter!
    public var userInfo: CustomUser?
    private var cartPresenter: CartPresenter!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        callAPI()
        registerCell()
        setupCheckoutButtonView()
        setupEmtyViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupBackButtonAndIconLogo()
        self.setupLightNavigationBar()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: - Initialization
    static func viewController() -> CartViewController? {
        return Helper.getViewController(named: "CartViewController", inSb: "Cart")
    }
    
    fileprivate func registerCell() {
        cartTableView.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.indentifier)
    }
    
    public func setupCheckoutButtonView() {
        toPaymentView.addSubview(authButtonView)
        authButtonView.translatesAutoresizingMaskIntoConstraints = false
        authButtonView.heightAnchor.constraint(equalTo: toPaymentView.heightAnchor, multiplier: 1).isActive = true
        authButtonView.widthAnchor.constraint(equalTo: toPaymentView.widthAnchor, multiplier: 1).isActive = true
        authButtonView.centerXAnchor.constraint(equalTo: toPaymentView.centerXAnchor).isActive = true
    }
    
    private func setupEmtyViews() {
        if UUID.uuid == nil {
            cartTableView.tableFooterView = setupEmptyView()
        }
    }
    
    fileprivate func initPresenter() {
        cartPresenter = CartPresenter()
        cartPresenter.cartDelegate = self
        profilePresenter = ProfilePresenter()
        profilePresenter.profileDelegate = self
    }
    
    public func callAPI() {
        self.showBlackLoadingSpinner()
        profilePresenter.getUserInfo(true)
    }
    
    // MARK: - Public Method
    public func totalPayment() {
        var totalProductInArray: Double = 0.0
        for cart in self.cartData {
            guard let quanlity = Double(cart.quanlity), let price = Double(cart.price) else { return }
                totalProductInArray += price*quanlity
        }
        self.totalLabel.text = "\(GlobalText.TITLE_LABEL_PRODUCT_TOTAL) \(moneyCalculateConvert(input: totalProductInArray))"
        /// Handle emptyView
        if cartData.isEmpty == true {
            cartTableView.tableFooterView = setupEmptyView()
        }
    }
    
    // MARK: - IBAction
    @IBAction func authButton(_ sender: Any) {
        guard let registerVC = RegisterViewController.viewController() else { return }
        guard let checkoutVC = CheckoutViewController.viewController() else { return }
        if UUID.uuid != nil {
            self.cartData.isEmpty ? self.showAlert(GlobalText.TITLE_POPUP_PAYMENT_FAILED, GlobalText.TITLE_POPUP_EMPTY_CART, GlobalText.TITLE_BUTTON_OKAY) : self.pushViewController(viewController: checkoutVC)
            checkoutVC.cartData = self.cartData
        } else {
            self.present(registerVC, animated: true)
        }
    }

    // MARK: - Target
    @objc public func toProfile() {
        guard let profileVC = ProfileViewController.viewController() else { return }
        UUID.uuid == nil ? self.showSignInAlert() : self.present(profileVC, animated: true)
        profileVC.userInfo = self.userInfo 
    }
}

// MARK: - AddToCart, CartDelegate
extension CartViewController: CartDelegate, ProfileDelegate {
    func getUserInfoFinished(info: CustomUser) {
        self.userInfo = info
        authButton.titleButton = GlobalText.TITLE_BUTTON_TO_CHECKOUT
        miniumLabel.text = GlobalText.TITLE_MINIUM_ORDER
        authButton.image = Constants.iconOrder
        cartPresenter.getCartObserver(true)
        self.hideBlackLoadingSpinner()
    }
    
    func getUserInfoFailed(mess: String) {
        authButton.titleButton = GlobalText.MESS_BUTTON_AUTH_CART
        authButton.image = Constants.signIn
        self.hideBlackLoadingSpinner()
    }
    
    func getCartFinished(_ data: [Cart]) {
        cartData = data
        totalPayment()
        cartTableView.reloadData()
    }
    
    func getCartFailed(_ mess: String) {
        self.hideBlackLoadingSpinner()
    }
    
    func updateCartFinished() {
        self.hideBlackLoadingSpinner()
        totalPayment()
    }
}

// MARK: - UpdateCell, DeleteCartDelegate
extension CartViewController: UpdateCellDelegate, DeleteCartDelegate {
    func deleteCartAction(id: String, indexPath: IndexPath) {
        if let indexCart = cartData.firstIndex(where: {$0.idItem == id}) {
            if self.isNetworkAvailable() == true {
                cartData[indexCart].ref?.removeValue()
            }
        }
        
        if cartData.count == 0 {
            cartTableView.reloadData()
        }
    }
    
    func updateInc(id: String, quanlity: Int?) {
        guard let cartIndex = self.cartData.firstIndex(where: {$0.idItem == id }) else { return }
        let cart = self.cartData[cartIndex]
        guard let quanlities = quanlity else { return }
        self.cartData[cartIndex].quanlity = String(quanlities)
        
        if self.isNetworkAvailable() == true {
            cartPresenter.updateCart(isCall: true, object: cart)
        }
    }
    
    func updateDec(id: String, quanlity: Int?) {
        guard let cartIndex = self.cartData.firstIndex(where: {$0.idItem == id }) else { return }
        let cart = self.cartData[cartIndex]
        guard let quanlities = quanlity else { return }
        self.cartData[cartIndex].quanlity = String(quanlities)
        if self.isNetworkAvailable() == true{
            cartPresenter.updateCart(isCall: true, object: cart)
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.indentifier, for: indexPath) as? CartTableViewCell, indexPath.row < self.cartData.count else { let cell = CartTableViewCell(style: .default, reuseIdentifier: CartTableViewCell.indentifier)
            return cell }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.cartTableViewCellDelegate = self
        cell.deleteCartDelegate = self
        
        let data = self.cartData[indexPath.row]
        
        cell.showDetail(title: data.productName, coverImage: data.coverImage, priceLabel: data.price, sizeLabel: data.size, id: data.idItem, indexPath: indexPath, quanlity: data.quanlity)
        cell.count = Int(data.quanlity)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableView.automaticDimension
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
