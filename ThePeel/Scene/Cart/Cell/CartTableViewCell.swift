//
//  CartTableViewCell.swift
//  ThePeel
//
//  Created by Gone on 1/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

protocol UpdateCellDelegate: NSObjectProtocol {
    func updateInc(id: String, quanlity: Int?)
    func updateDec(id: String, quanlity: Int?)
}

protocol DeleteCartDelegate: NSObjectProtocol {
    func deleteCartAction(id: String, indexPath: IndexPath)
}

// swiftlint:disable force_unwrapping
class CartTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var deleteCartButton: UIButton!
    @IBOutlet public weak var coverImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var sizeLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    @IBOutlet fileprivate weak var traditionLabel: UILabel!
    @IBOutlet fileprivate weak var volumeView: VolumeView!
    
    // MARK: - Properties
    public var didIncreaseVolume:(() -> Void)?
    public var didDecreaseVolume:(() -> Void)?
    
    public var total: Int = 0
    public var id: String?
    
    public var indexPath: IndexPath?
    public var section: Int?
    
    weak var cartTableViewCellDelegate: UpdateCellDelegate?
    weak var deleteCartDelegate: DeleteCartDelegate?
    
    var count: Int? {
        didSet {
            guard let count = self.count else { return }
            handleProductCount(count: count)
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupVolumeButton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    public func handleProductCount(count: Int) {
        self.volumeView.countLabel.text = String(count)
    }
    
    // MARK: - Private Method
    fileprivate func setupVolumeButton() {
        self.volumeView.increaseBtn.addTarget(self, action: #selector(increaseButton), for: .touchUpInside)
        self.volumeView.decreaseBtn.addTarget(self, action: #selector(decreaseButton), for: .touchUpInside)
    }
    
    // MARK: - Public Method
    public func showDetail(title: String, coverImage: String, priceLabel: String, sizeLabel: String, id: String, indexPath: IndexPath, quanlity: String) {
        self.titleLabel.text = title
        self.priceLabel.text = moneyLocale(input: priceLabel)
        self.sizeLabel.text = "Size: " + sizeLabel
        self.id = id
        
        let processor = RoundCornerImageProcessor(cornerRadius: 20)
        guard let url = URL(string: coverImage) else { return }
        coverImageView.kf.setImage(with: url, options: [.processor(processor)])
        
        self.indexPath = indexPath
    }
    
    // MARK: - Target
    @IBAction func increaseButton(_ sender: Any) {
        if let empty = self.count, empty < 10, let id = self.id {
            let cellCount = empty + 1
            self.count = cellCount
            cartTableViewCellDelegate?.updateInc(id: id, quanlity: self.count!)
            handleProductCount(count: self.count!)
        }
    }
    
    // swiftlint:disable force_unwrapping
    @IBAction func decreaseButton(_ sender: Any) {
        if let empty = self.count, empty > 1, let id = self.id {
            let cellCount = empty - 1
            self.count = cellCount
            cartTableViewCellDelegate?.updateDec(id: id, quanlity: self.count!)
            handleProductCount(count: self.count!)
        }
    }
    
    @IBAction func deleteCartButton(_ sender: Any) {
        guard let id = self.id, let indexPath = self.indexPath else { return }
        self.deleteCartDelegate?.deleteCartAction(id: id, indexPath: indexPath)
    }
}
