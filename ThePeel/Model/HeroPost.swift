//
//  HeroImage.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class HeroPost: NSObject {
    var ref: DatabaseReference?
    var id: String
    var title: String
    var coverImg: String
    var imageUrl: [HeroImage]
    
    init(id: String, coverImg: String, title: String, imageUrl: [HeroImage]) {
        self.id = id
        self.coverImg = coverImg
        self.title = title
        self.imageUrl = imageUrl
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let id = value["id"] as? String,
            let coverImg = value["coverImg"] as? String,
            let title = value["title"] as? String else { return nil }
        
        var imageList = [HeroImage]()
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot{
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let image = HeroImage(snapshot: snapshot) {
                        imageList.append(image)
                    }
                }
            }
        }
        
        self.ref = snapshot.ref
        self.id = id
        self.title = title
        self.coverImg = coverImg
        self.imageUrl = imageList
    }
    
    func toAnyObject() -> Any {
        return [
            "id": id,
            "coverImg": coverImg,
            "title": title,
            "image_url": imageUrl
        ]
    }
}

class HeroImage {
    var ref: DatabaseReference?
    var key: String
    var urlImg: String
    var dateTime: String
    
    init(key: String, urlImg: String, dateTime: String) {
        self.key = key
        self.urlImg = urlImg
        self.dateTime = dateTime
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let urlImg = value["urlImg"] as? String,
            let dateTime = value["dateTime"] as? String else { return nil }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.urlImg = urlImg
        self.dateTime = dateTime
    }
    
    func toAnyObject() -> Any {
        return [
            "dateTime": dateTime,
            "urlImg": urlImg
        ]
    }
}
