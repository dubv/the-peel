//
//  Dessert.swift
//  ThePeel
//
//  Created by Gone on 1/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Dessert: NSObject {
    let ref: DatabaseReference?
    let category: String
    var detail: DessertDetail?
    let id: String
    let name: String
    let imageUrl: String
    var counts: Int
    
    init(category: String, id: String, detail: DessertDetail?, name: String, imageUrl: String, counts: Int) {
        self.ref = nil
        self.category = category
        self.id = id
        self.detail = detail
        self.name = name
        self.imageUrl = imageUrl
        self.counts = counts
    }
    
    init?(snapshot: DataSnapshot){
        guard let value = snapshot.value as? [String: Any], let category = value["category"] as? String, let id = value["id"] as? String, let name = value["name"] as? String, let imageUrl = value["image_url"] as? String else { return nil }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let data = DessertDetail(snapshot: snapshot) {
                self.detail = data
            }
        }
        
        self.ref = snapshot.ref
        self.category = category
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        self.counts = 1
    }
}

class DessertDetail {
    let composition: String
    let weight: String
    let price: String
    
    init(composition: String, weight: String, price: String) {
        self.composition = composition
        self.weight = weight
        self.price = price
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: Any],
            let composition = value["composition"] as? String, let weight = value["weight"] as? String, let price = value["price"] as? String else { return nil }
        
        self.composition = composition
        self.weight = weight
        self.price = price
    }
}
