//
//  Drink.swift
//  ThePeel
//
//  Created by Gone on 1/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Drink: NSObject {
    let ref: DatabaseReference?
    let category: String
    var detail: DrinkDetail?
    let id: String
    let name: String
    let imageUrl: String
    var counts: Int
    
    init(category: String, id: String, name: String, urlImage: String, counts: Int) {
        self.ref = nil
        self.category = category
        self.id = id
        self.name = name
        self.imageUrl = urlImage
        self.counts = counts
    }
    
    init?(snapshot: DataSnapshot){
        guard let value = snapshot.value as? [String: Any], let category = value["category"] as? String, let id = value["id"] as? String, let name = value["name"] as? String, let imageUrl = value["image_url"] as? String else { return nil }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let data = DrinkDetail(snapshot: snapshot) {
                self.detail = data
            }
        }
        
        self.ref = snapshot.ref
        self.category = category
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        self.counts = 1
    }
}

class DrinkDetail {
    let price: String
    let volume: String
    
    init(price: String, volume: String) {
        self.price = price
        self.volume = volume
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: Any], let price = value["price"] as? String, let volume = value["volume"] as? String else { return nil }
        
        self.price = price
        self.volume = volume
    }
}
