//
//  OrderItem.swift
//  ThePeel
//
//  Created by Gone on 1/28/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Cart: NSObject {
    let ref: DatabaseReference?
    let key: String
    var idItem: String
    var idProduct: String
    var coverImage: String
    var price: String
    var size: String
    var quanlity: String
    var desc: String
    var dateTime: String
    var userID: String
    var productName: String
    
    init(key: String, idItem: String, idProduct: String, coverImage: String, price: String, size: String, quanlity: String, desc: String, dateTime: String, userID: String, productName: String) {
        self.ref = nil
        self.key = key
        self.idItem = idItem
        self.idProduct = idProduct
        self.coverImage = coverImage
        self.price = price
        self.size = size
        self.quanlity = quanlity
        self.desc = desc
        self.dateTime = dateTime
        self.userID = userID
        self.productName = productName
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let idItem = value["idItem"] as? String,
            let idProduct = value["idProduct"] as? String,
            let coverImage = value["coverImage"] as? String,
            let price = value["price"] as? String,
            let size = value["size"] as? String,
            let quanlity = value["quanlity"] as? String,
            let desc = value["desc"] as? String,
            let dateTime = value["dateTime"] as? String,
            let userID = value["userID"] as? String,
            let productName = value["productName"] as? String
            else { return nil }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.idItem = idItem
        self.idProduct = idProduct
        self.coverImage = coverImage
        self.price = price
        self.size = size
        self.quanlity = quanlity
        self.desc = desc
        self.dateTime = dateTime
        self.userID = userID
        self.productName = productName
    }
    
    func toAnyObject() -> Any {
        return [
            "key": key,
            "idItem": idItem,
            "idProduct": idProduct,
            "coverImage": coverImage,
            "size": size,
            "price": price,
            "quanlity": quanlity,
            "desc": desc,
            "dateTime": dateTime,
            "userID": userID,
            "productName": productName
        ]
    }
}
