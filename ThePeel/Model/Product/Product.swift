//
//  Product.swift
//  ThePeel
//
//  Created by Gone on 1/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Product: NSObject {
    let ref: DatabaseReference?
//    let key: String
    var pizza: Pizza?
    var dessert: Dessert?
    var drink: Drink?
    var other: HeroPost?
    
    init(pizza: Pizza?, dessert: Dessert?, drink: Drink?, other: HeroPost?) {
        self.ref = nil
//        self.key = key
        self.pizza = pizza
        self.dessert = dessert
        self.drink = drink
        self.other = other
    }
    
    init?(snapshot: DataSnapshot) {
        for child in snapshot.children {
            if let snapshots = child as? DataSnapshot,
                let data = Pizza(snapshot: snapshots) {
                self.pizza = data
            }
        }
        
        for child in snapshot.children {
            if let snapshots = child as? DataSnapshot,
                let data = Drink(snapshot: snapshots) {
                self.drink = data
            }
        }
        
        for child in snapshot.children {
            if let snapshots = child as? DataSnapshot,
                let data = Dessert(snapshot: snapshots) {
                self.dessert = data
            }
        }
        
        for child in snapshot.children {
            if let snapshots = child as? DataSnapshot,
                let data = HeroPost(snapshot: snapshots) {
                self.other = data
            }
        }
        
        self.ref = snapshot.ref
//        self.key = snapshot.key
    }
    
    func toAnyObject() -> Any {
        return [
            "pizza": [pizza],
            "drink": [drink],
            "dessert": [dessert],
            "other": [other]
        ]
    }
}
