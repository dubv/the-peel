//
//  PizzaDetail.swift
//  ThePeel
//
//  Created by Gone on 1/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Detail {
    var ref: DatabaseReference?
    var composition: String
    var description: Description?
    var nutritional: Nutritional?
    var state: String
    
    init(composition: String, description: Description?, nutritional: Nutritional?, state: String) {
        self.composition = composition
        self.description = description
        self.nutritional = nutritional
        self.state = state
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let composition = value["composition"] as? String,
            let state = value["state"] as? String else { return nil }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let description = Description(snapshot: snapshot) {
                self.description = description
            }
        }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let nutritional = Nutritional(snapshot: snapshot) {
                self.nutritional = nutritional
            }
        }
        
        self.ref = snapshot.ref
        self.composition = composition
        self.state = state
    }
    
    func toAnyObject() -> Any {
        return [
            "composition": composition,
            "description": [description],
            "nutritional": [nutritional],
            "state": state
        ]
    }
}

class Description {
    var ref: DatabaseReference?
    var price: String
    var size: String
    var weight: String
    
    init(price: String, size: String, weight: String) {
        self.ref = nil
        self.price = price
        self.size = size
        self.weight = weight
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let price = value["price"] as? String,
            let size = value["size"] as? String,
            let weight = value["weight"] as? String else { return nil }
        
        self.ref = snapshot.ref
        self.price = price
        self.size = size
        self.weight = weight
    }
    
    func toAnyObject() -> Any {
        return [
            "price": price,
            "size": size,
            "weight": weight
        ]
    }
}

class Nutritional {
    var ref: DatabaseReference?
    var calories: String
    var carbohydrate: String
    var fats: String
    var proteins: String
    
    init(calories: String, carbohydrate: String, fats: String, proteins: String) {
        self.calories = calories
        self.carbohydrate = carbohydrate
        self.fats = fats
        self.proteins = proteins
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let calories = value["calories"] as? String,
            let carbohydrate = value["carbohydrate"] as? String,
            let fats = value["fats"] as? String,
            let proteins = value["proteins"] as? String
            else { return nil }
        
        self.ref = snapshot.ref
        self.calories = calories
        self.carbohydrate = carbohydrate
        self.fats = fats
        self.proteins = proteins
    }
    
    func toAnyObject() -> Any {
        return [
            "calories": calories,
            "carbohydrate": carbohydrate,
            "fats": fats,
            "proteins": proteins
        ]
    }
}
