//
//  Pizza.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Pizza: NSObject {
    let ref: DatabaseReference?
    var id: String
    var name: String
    var category: String
    var detail: Detail?
    var counts: Int
    var comment: [Comment]
    var image: [ProductImage]
    var likeCount: Int
    var likeByUser: [String: Bool]?
    
    init(id: String, name: String, category: String, detail: Detail?, counts: Int, comment: [Comment], image: [ProductImage], likeCount: Int, likeByUser: [String: Bool]?) {
        self.ref = nil
        self.id = id
        self.name = name
        self.category = category
        self.detail = detail
        self.counts = counts
        self.comment = comment
        self.image = image
        self.likeCount = likeCount
        self.likeByUser = likeByUser
    }
    
    init?(snapshot: DataSnapshot) {
        
        guard let value = snapshot.value as? [String: AnyObject],
        let name = value["name"] as? String,
            let id = value["id"] as? String,
            let category = value["category"] as? String,
            let likeCount = value["likeCount"] as? Int,
            let likeByUser = value["likeByUser"] as? [String: Bool]?
             else { return nil }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let detail = Detail(snapshot: snapshot) {
                self.detail = detail
            }
        }
        
        var commentList = [Comment]()
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot{
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let comment = Comment(snapshot: snapshot) {
                        commentList.append(comment)
                    }
                }
            }
        }
        
        var imageList = [ProductImage]()
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot{
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let image = ProductImage(snapshot: snapshot) {
                        imageList.append(image)
                    }
                }
            }
        }
        
        self.ref = snapshot.ref
        self.category = category
        self.id = id
        self.name = name
        self.counts = 1
        self.comment = commentList
        self.image = imageList
        self.likeCount = likeCount
        self.likeByUser = likeByUser
    }
    
    func toAnyObject() -> Any {
        return [
            "category": category,
            "id": id,
            "detail": [detail],
            "name": name,
            "comment": [comment],
            "image": [image],
            "likeCount": likeCount
//            "likeByUser": likeByUser
        ]
    }
}

class ProductImage {
    var ref: DatabaseReference?
    var key: String
    var urlImg: String
    var dateTime: String
    
    init(key: String, urlImg: String, dateTime: String) {
        self.key = key
        self.urlImg = urlImg
        self.dateTime = dateTime
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let urlImg = value["urlImg"] as? String,
            let dateTime = value["dateTime"] as? String else { return nil }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.urlImg = urlImg
        self.dateTime = dateTime
    }
    
    func toAnyObject() -> Any {
        return [
            "dateTime": dateTime,
            "urlImg": urlImg
        ]
    }
}
