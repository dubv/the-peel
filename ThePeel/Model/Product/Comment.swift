//
//  Comment.swift
//  ThePeel
//
//  Created by Gone on 2/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Comment {
    let ref: DatabaseReference?
    let key: String
    var avatar: String?
    var userName: String?
    var uidString = UUID.uuid
    var commentContent: String?
    @objc dynamic var dateTime: String
    var keyProduct: String?
    
    init(key: String, avatar: String, userName: String, commentContent: String, dateTime: String, keyProduct: String) {
        self.ref = nil
        self.key = key
        self.avatar = avatar
        self.userName = userName
        self.commentContent = commentContent
        self.dateTime = dateTime
        self.keyProduct = keyProduct
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject], let avatar = value["avatar"] as? String, let userName = value["userName"] as? String, let uuid = value["uidString"] as? String, let commentContent = value["commentContent"] as? String, let dateTime = value["dateTime"] as? String, let keyProduct = value["keyProduct"] as? String else {
            return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.avatar = avatar
        self.userName = userName
        self.commentContent = commentContent
        self.dateTime = dateTime
        self.uidString = uuid
        self.keyProduct = keyProduct
    }
    
    func toAnyObject() -> Any {
        return [
            "idComment": key,
            "avatar": avatar,
            "userName": userName,
            "commentContent": commentContent,
            "dateTime": dateTime,
            "uidString": uidString,
            "keyProduct": keyProduct
        ]
    }
}
