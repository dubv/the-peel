//
//  CustomUser.swift
//  ThePeel
//
//  Created by Gone on 1/25/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class CustomUser: NSObject {
    let ref: DatabaseReference?
    let key: String
    var avatar: String
    var descriptions: String
    var externalLink: String
    var firstName: String
    var lastName: String
    var userName: String
    var email: String
    var password: String
    var phoneNumber: String
    var address: String
    var cart: Cart?
    var comment: [Comment]
        
    init(key: String, avatar: String, descriptions: String, externalLink: String, firstName: String, lastName: String, userName: String, email: String, password: String, phoneNumber: String, address: String, cart: Cart?, comment: [Comment]) {
        self.ref = nil
        self.key = key
        self.avatar = avatar
        self.descriptions = descriptions
        self.externalLink = externalLink
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.email = email
        self.password = password
        self.phoneNumber = phoneNumber
        self.address = address
        self.cart = cart
        self.comment = comment
        }
        
        init?(snapshot: DataSnapshot) {
            guard let value = snapshot.value as? [String: AnyObject],
                let avatar = value["avatar"] as? String,
                let descriptions = value["descriptions"] as? String,
                let externalLink = value["externalLink"] as? String,
                let firstName = value["firstName"] as? String,
                let lastName = value["lastName"] as? String,
                let userName = value["userName"] as? String,
                let email = value["email"] as? String,
                let password = value["password"] as? String,
                let phoneNumber = value["phoneNumber"] as? String,
                let address = value["address"] as? String
                else { return nil }
            
            for child in snapshot.children {
                if let snapshots = child as? DataSnapshot,
                    let data = Cart(snapshot: snapshots) {
                    self.cart = data
                }
            }
            
            var commentList = [Comment]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot{
                    for child in snapshot.children {
                        if let snapshot = child as? DataSnapshot, let comment = Comment(snapshot: snapshot) {
                            commentList.append(comment)
                        }
                    }
                }
            }
            
            self.ref = snapshot.ref
            self.key = snapshot.key
            self.avatar = avatar
            self.descriptions = descriptions
            self.externalLink = externalLink
            self.firstName = firstName
            self.lastName = lastName
            self.userName = userName
            self.email = email
            self.password = password
            self.phoneNumber = phoneNumber
            self.address = address
            self.comment = commentList
        }
        
        func toAnyObject() -> Any {
            return [
                "avatar": avatar ,
                "descriptions": descriptions ,
                "externalLink": externalLink ,
                "firstName": firstName ,
                "lastName": lastName ,
                "userName": userName,
                "email": email,
                "password": password,
                "phoneNumber": phoneNumber,
                "address": address ,
                "comment": [comment],
                "cart": [cart]
            ]
        }
}
