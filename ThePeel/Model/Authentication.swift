//
//  Auth.swift
//  ThePeel
//
//  Created by Gone on 1/30/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Authen: NSObject {
    var phoneNumber: String
    var password: String
    
    init(phoneNumber: String, password: String) {
        self.phoneNumber = phoneNumber
        self.password = password
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject], let phoneNumber = value["phoneNumber"] as? String, let password = value["password"] as? String else {
            return nil
        }
        
        self.phoneNumber = phoneNumber
        self.password = password
    }
    
    func toAnyObject() -> Any {
        return [
            "phoneNumber": phoneNumber,
            "password": password
        ]
    }
}
