//
//  ItemPayment.swift
//  ThePeel
//
//  Created by Gone on 2/20/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class ItemPayment: NSObject {
    let ref: DatabaseReference?
    let key: String
    var uid = UUID.uuid
    var idItem: String
    var customerName: String
    var address: String
    var phoneNumber: String
    var email: String
    var cart: [Cart]
    var totalPayment: String
    var status: String
    var dateTime: String
    
    init(key: String, customerName: String, address: String, phoneNumber: String, email: String, cart: [Cart], totalPayment: String, status: String, dateTime: String) {
        self.ref = nil
        self.key = key
        self.idItem = key
        self.customerName = customerName
        self.address = address
        self.phoneNumber = phoneNumber
        self.email = email
        self.cart = cart
        self.totalPayment = totalPayment
        self.status = status
        self.dateTime = dateTime
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let idItem = value["idItem"] as? String,
            let customerName = value["customerName"] as? String,
            let address = value["address"] as? String,
            let phoneNumber = value["phoneNumber"] as? String,
            let email = value["email"] as? String,
            let totalPayment = value["totalPayment"] as? String,
            let status = value["status"] as? String,
            let dateTime = value["dateTime"] as? String else {return nil}
        
        var cartList = [Cart]()
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot {
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let cart = Cart(snapshot: snapshot) {
                        cartList.append(cart)
                    }
                }
            }
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.idItem = idItem
        self.customerName = customerName
        self.address = address
        self.phoneNumber = phoneNumber
        self.email = email
        self.totalPayment = totalPayment
        self.cart = cartList
        self.status = status
        self.dateTime = dateTime
    }
    
    func toAnyObject() -> Any {
        return [
            "key": key,
            "uid": uid,
            "idItem": idItem,
            "customerName": customerName,
            "address": address,
            "phoneNumber": phoneNumber,
            "email": email,
            "totalPayment": totalPayment,
            "status": status,
            "dateTime": dateTime
//            "cart": [cart]
        ]
    }
}
