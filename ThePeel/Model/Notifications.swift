//
//  Notifications.swift
//  ThePeel
//
//  Created by Gone on 3/5/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Notifications: NSObject {
    var ref: DatabaseReference?
    var id: String
    var uid: String
    var topic: String
    var topicId: String
    var title: String
    var content: String
    var dateTime: String
    var read: Bool
    
    init(id: String, uid: String, topic: String, topicId: String, title: String, content: String, dateTime: String, read: Bool) {
        self.id = id
        self.uid = uid
        self.topic = topic
        self.topicId = topicId
        self.title = title
        self.content = content
        self.dateTime = dateTime
        self.read = read
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let id = value["id"] as? String,
            let topic = value["topic"] as? String,
            let uid = value["uid"] as? String,
            let topicId = value["topicId"] as? String,
            let title = value["title"] as? String,
            let content = value["content"] as? String,
            let dateTime = value["dateTime"] as? String,
            let read = value["read"] as? Bool
            else {return nil}
        
        self.ref = snapshot.ref
        self.id = id
        self.uid = uid
        self.topic = topic
        self.topicId = topicId
        self.title = title
        self.content = content
        self.dateTime = dateTime
        self.read = read
    }
    
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "id": id,
            "topic": topic,
            "topicId": topicId,
            "content": content,
            "title": title,
            "dateTime": dateTime,
            "read": read
        ]
    }
}
