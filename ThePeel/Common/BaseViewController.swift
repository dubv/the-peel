//
//  BaseViewController.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Reachability
import FSPagerView

class BaseViewController: UIViewController {
    // MARK: - Properties
    public var currentDateTime = Date()
    public var dateFormatter = DateFormatter()
    public var indexStatus = userStatus()
    public weak var timerForShowPopup: Timer?
    public var viewCenterY: CGFloat?
    public var countTapped: Int = 0
    /// Delegate
    public weak var baseViewControllerDelegate: BaseViewControllerDelegate?
    public weak var checkPasswordViewDelegate: CheckPasswordViewDelegate?
    public weak var didChangeNetwork: DidChangeNetwork?
    /// UIs
    public var cartButton: BadgeBarButtonItem!
    public var spinButton: UIBarButtonItem!
    public var emptyView: EmptyCartView!
    public var emptyNotifisView: EmptyNotificationView!
    public var sheetView: CustomSheetView!
    public var scrollY: CGFloat = 0
    public var tabBarHiding = false
    lazy var popupView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    fileprivate var whiteIndicator = MyIndicator(frame: CGRect(x: 0, y: 0, width: 27, height: 27), color: "iconLoadingWhite")
    fileprivate var blackIndicator = MyIndicator(frame: CGRect(x: 0, y: 0, width: 27, height: 27), color: "iconLoadingBlack")
    lazy var backEclipseButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "iconBackOutLine"), for: .normal)
        return button
    }()
    lazy var dismissDownButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "dismissDownButton"), for: .normal)
        return button
    }()
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "iconCancel"), for: .normal)
        return button
    }()
    lazy var iconLogoButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "iconLogoBlack"), for: .normal)
        return button
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Network.sharedInstance.addListener(listener: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    // MARK: - Initialization
    public func setTitleFont(title: String, textColor: String) {
        guard let customFont = UIFont(name: Constants.PFLight, size: 20) else { return }
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: title, attributes:[
            NSAttributedString.Key.foregroundColor: UIColor(hexString: textColor),
            NSAttributedString.Key.font: customFont])
        navTitle.addAttribute(NSAttributedString.Key.kern, value: 1.7, range: NSRange(location: 0, length: navTitle.length - 1))
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
    }
    
    public func setupEmptyView() -> EmptyCartView {
        emptyView = EmptyCartView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.8))
        return emptyView
    }
    
    public func setupEmptyNotifiView() -> EmptyNotificationView {
        emptyNotifisView = EmptyNotificationView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.8))
        return emptyNotifisView
    }
    
    func setupHeroImageSliderView(sliderView: FSPagerView) {
        sliderView.itemSize = CGSize(width: 190, height: 70)
        sliderView.isInfinite = true
        sliderView.automaticSlidingInterval = 2.5
        sliderView.interitemSpacing = 5
        sliderView.decelerationDistance = 0
        sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    public func setupDefaultNavigationBar() {
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.setTitleVerticalPositionAdjustment(0.0, for: .default)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
    }
    
    public func setupLightNavigationBar() {
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.setTitleVerticalPositionAdjustment(0.0, for: .default)
        navigationController?.navigationBar.barStyle = UIBarStyle.default
    }
    
    public func setGlobalNavButton(leftButton: UIBarButtonItem, rightButton: UIBarButtonItem) {
        navigationItem.leftBarButtonItems = [leftButton, UIBarButtonItem(customView: iconLogoButton)]
        leftButton.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = rightButton
        rightButton.tintColor = UIColor.black
    }
    
    public func setupLeftItemsButton(leftButton: UIBarButtonItem) {
        navigationItem.leftBarButtonItem = leftButton
        leftButton.tintColor = UIColor.black
    }
    
    public func setupBackButton() {
        let backButton = backEclipseButton
        backButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(popViewControler), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.tintColor = UIColor(hexString: GlobalColor.gray)
    }
    
    public func setupBackButtonAndIconLogo() {
        let backButton = backEclipseButton
        backButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(popViewControler), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), UIBarButtonItem(customView: iconLogoButton)]
        backButton.tintColor = UIColor(hexString: GlobalColor.gray)
    }
    
    public func setupDismissButton(_ dismissButton: UIButton) {
        view.addSubview(dismissButton)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        dismissButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        dismissButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        dismissButton.contentHorizontalAlignment = .right
        if #available(iOS 11.0, *) {
            dismissButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        } else {
            dismissButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
        }
        UIView.animate(withDuration: 0.5) { () -> Void in
            dismissButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }

    public func setupSearchTextField(view: UIView, searchField: CustomSearchTextField) {
        view.addSubview(searchField)
        view.backgroundColor = UIColor.clear
        searchField.checked = true
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.heightAnchor.constraint(equalToConstant: 60).isActive = true
        searchField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        searchField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        searchField.backgroundColor = UIColor(hexString: "EDEDED")
        searchField.textField.leftAnchor.constraint(equalTo: searchField.leftAnchor, constant: 16).isActive = true
        searchField.rightButtonImage = "iconSearch"
    }
    
    func setupCollectionViewFlow(_ collectionView: UICollectionView) {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    public func showBlankView() {
        guard let blankVC = BlankViewController.viewController() else { return }
        blankVC.providesPresentationContextTransitionStyle = true
        blankVC.definesPresentationContext = true
        blankVC.modalTransitionStyle = .crossDissolve
        blankVC.modalPresentationStyle = .overFullScreen
        self.present(blankVC, animated: false)
    }
    
    public func showUnderPopup(productName: String, productImage: String) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        popupView.layer.add(transition, forKey: kCATransition)
        if let tabbarHeight = navigationController?.tabBarController?.tabBar.frame.height {
            popupView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -(tabbarHeight + 10)).isActive = true
        } else {
            popupView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
        }
        /// Setup Popup UIs
        popupView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        popupView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        popupView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.07).isActive = true
        popupView.backgroundColor = UIColor.white
        popupView.layer.cornerRadius = 9
        popupView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        popupView.layer.shadowOffset = CGSize(width: 3, height: 3)
        popupView.layer.shadowOpacity = 0.5
        popupView.layer.shadowRadius = 3
        /// Setup SheetView UIs
        sheetView = CustomSheetView()
        popupView.addSubview(sheetView)
        sheetView.translatesAutoresizingMaskIntoConstraints = false
        sheetView.heightAnchor.constraint(equalTo: self.popupView.heightAnchor, multiplier: 1).isActive = true
        sheetView.widthAnchor.constraint(equalTo: self.popupView.widthAnchor, multiplier: 1).isActive = true
        sheetView.centerYAnchor.constraint(equalTo: self.popupView.centerYAnchor).isActive = true
        sheetView.product = productName
        sheetView.image = productImage
        sheetView.layer.cornerRadius = 9
        sheetView.sheetViewDelegate = self
    }

    // MARK: - Public Method
    public func pushViewController (viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    public func presentVC(viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        present(viewController, animated: false, completion: nil)
    }
    
    public func showCheckPassAlert() {
        let alertController = UIAlertController(title: "Type your password", message: nil, preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { (_) in
            if let txtField = alertController.textFields?.first, let text = txtField.text {
                self.checkPasswordViewDelegate?.checkPass(pass: text)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        alertController.addTextField { (textField) in
            textField.placeholder = "Password"
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func setBlurBackground() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
    }
    
    public func isNetworkAvailable() -> Bool {
        if Network.isConnectedToNetwork() == false {
            self.showAlert(GlobalText.TITLE_NO_INTERNET, GlobalText.MESSAGE_NO_INTERNET, GlobalText.TITLE_BUTTON_OKAY)
            return false
        }
        return true
    }
    
    public func showAlert(_ title: String, _ mess: String, _ buttonTitle: String) {
        guard let alertVC = AlertViewController.viewController() else { return }
        alertVC.providesPresentationContextTransitionStyle = true
        alertVC.definesPresentationContext = true
        alertVC.modalPresentationStyle = .overFullScreen
        alertVC.modalTransitionStyle = .crossDissolve
        self.present(alertVC, animated: true)
        alertVC.alertViewControllerDelegate = self
        let data = AlertView(title: title, message: mess, buttonTitle: buttonTitle)
        alertVC.data = data
    }
    
    func showWhiteLoadingSpinner() {
        self.view.addSubview(self.whiteIndicator)
        whiteIndicator.center = self.view.center
        whiteIndicator.startAnimating()
    }
    
    func showBlackLoadingSpinner() {
        self.view.addSubview(self.blackIndicator)
        blackIndicator.center = self.view.center
        blackIndicator.startAnimating()
    }
    
    func hideWhiteLoadingSpinner() {
        whiteIndicator.stopAnimating()
        whiteIndicator.removeFromSuperview()
    }
    
    func hideBlackLoadingSpinner() {
        UIView.animate(withDuration: 1.5) {
            self.blackIndicator.stopAnimating()
            self.blackIndicator.removeFromSuperview()
        }
    }
    
    // MARK: ImageView
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    // MARK: - Validate func
    public func validateFinish(_ label: UILabel) {
        label.text = ""
    }
    
    public func validateFailed(_ error: String, _ label: UILabel) {
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                label.text = error
            })
        }
    }
    
    // MARK: - TableView
    public func scrollTableViewToBottom(tableView: UITableView) {
        let scrollPoint = CGPoint(x: 0, y: tableView.contentSize.height - tableView.frame.size.height)
        tableView.setContentOffset(scrollPoint, animated: true)
    }
    
    public func tableViewScrollToBottom(animated: Bool, tableView: UITableView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = tableView.numberOfSections
            let numberOfRows = tableView.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }
    
    public func setupPopupView(object: Cart, favicon: String) {
        self.view.addSubview(self.popupView)
        self.showUnderPopup(productName: object.productName + " +\(object.quanlity)", productImage: favicon)
        self.popupView.isHidden = false
        timerForShowPopup?.invalidate()
        timerForShowPopup = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(dismissPopupOrderView), userInfo: nil, repeats: false)
    }
    
    // MARK: - Target
    @objc public func showMenu() {
        guard let menuVC = MenuViewController.viewController() else { return }
        presentVC(viewController: menuVC)
    }

    @objc func panGesture(sender: UIPanGestureRecognizer) {
        if sender.state == .began {
        } else if sender.state == .changed {
            let translation = sender.translation(in: self.view)
            if let view = self.view, view.frame.minY >= 0 {
                view.center = CGPoint(x: view.center.x,
                                      y: view.center.y + translation.y)
                viewCenterY = view.center.y
            }
            sender.setTranslation(CGPoint.zero, in: self.view)
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            UIView.animate(withDuration: 0.3, animations: {
                if let viewCenter = self.viewCenterY, viewCenter > self.view.frame.height {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                }
            }, completion: { _ in
            })
        }
    }

    @objc public func dismissVC() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc public func dismissDoubleVC() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
    }
    
    @objc public func dismissDoubleVCNormal() {
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc public func dismissPopupOrderView() {
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        self.popupView.layer.add(transition, forKey: kCATransition)
        popupView.isHidden = true
        timerForShowPopup?.invalidate()
        sheetView.removeFromSuperview()
        popupView.willRemoveSubview(sheetView)
        popupView.removeFromSuperview()
    }
    
    @objc public func toCart() {
        guard let cartVC = CartViewController.viewController() else { return }
        self.pushViewController(viewController: cartVC)
    }
    
    @objc public func dismissVCNormal() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc public func popViewControler() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc public func showSignInAlert() {
        guard let alertSignInVC = SignInViewController.viewController() else { return }
        alertSignInVC.modalPresentationStyle = .overFullScreen
        self.present(alertSignInVC, animated: true)
    }
    
    @objc public func showLogoutAlert() {
        guard let alertLogoutVC = LogoutViewController.viewController() else { return }
        alertLogoutVC.providesPresentationContextTransitionStyle = true
        alertLogoutVC.definesPresentationContext = true
        alertLogoutVC.modalPresentationStyle = .overFullScreen
        alertLogoutVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(alertLogoutVC, animated: true)
    }
}

extension BaseViewController: AlertViewControllerDelegate {
    func popingView() {
        baseViewControllerDelegate?.popView()
    }
    
    func okayButton() {
        baseViewControllerDelegate?.okayButtonFromAlertView()
    }
}

// MARK: - UITextFieldDelegate, NetworkStatusListener, SheetViewDelegate
extension BaseViewController: UITextFieldDelegate, NetworkStatusListener, SheetViewDelegate {
    func isPushed() {
        guard let cartVC = CartViewController.viewController() else { return }
        pushViewController(viewController: cartVC)
    }

    func networkStatusDidChange(status: Reachability.Connection) {
        if status == .none {
            didChangeNetwork?.noNetwork()
            showAlert(GlobalText.TITLE_NO_INTERNET, GlobalText.MESSAGE_NO_INTERNET, GlobalText.TITLE_BUTTON_OKAY)
        } else {
            didChangeNetwork?.networkAvailable()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
