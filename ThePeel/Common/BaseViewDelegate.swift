//
//  BaseViewDelegate.swift
//  ThePeel
//
//  Created by Gone on 3/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol DidChangeNetwork: NSObjectProtocol {
    func noNetwork()
    func networkAvailable()
}

extension DidChangeNetwork {
    public func noNetwork() {}
    public func networkAvailable() {}
}

protocol BaseViewControllerDelegate: NSObjectProtocol {
    func popView()
    func okayButtonFromAlertView()
}

extension BaseViewControllerDelegate {
    public func popView() {}
    public func okayButtonFromAlertView() {}
}
