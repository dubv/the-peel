//
//  User.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class Users: NSObject {
    
    var id: String
    var userName: String
    var phoneNumber: String
    var password: String
    var fullName: String
    var address: String
    var baseAddress: String
    var dayOfBirth: String
    var gender: String
    var itemOrdered: Product?
    var voucher: String
    var possition: String
    
    init(id: Firebase.User, userName: String, phoneNumber: String, password: String, fullName: String, address: String, baseAddress: String, dayOfBirth: String, gender: String, itemOrdered: Product?, voucher: String, possition: String) {
        self.id = id.uid
        self.userName = userName
        self.phoneNumber = phoneNumber
        self.password = password
        self.fullName = fullName
        self.address = address
        self.baseAddress = baseAddress
        self.dayOfBirth = dayOfBirth
        self.gender = gender
        self.itemOrdered = itemOrdered
        self.voucher = voucher
        self.possition = possition
    }
    
    init?(snapshot: DataSnapshot) {
        guard let f_value = snapshot.value as? [String: AnyObject], let id = f_value["id"] as? String, let userName = f_value["username"] as? String, let phoneNumber = f_value["phoneNumber"] as? String, let password = f_value["password"] as? String, let fullName = f_value["fullName"] as? String else {
            return nil
        }
        
        guard let s_value = snapshot.value as? [String: AnyObject], let address = s_value["address"] as? String, let baseAddress = s_value["baseAddress"] as? String, let dayOfBirth = s_value["dayOfBirth"] as? String, let gender = s_value["gender"] as? String, let voucher = s_value["voucher"] as? String, let possition = s_value["possition"] as? String else {
            return nil
        }
        
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot, let product = Product(snapshot: snapshot) {
            self.itemOrdered = product
            }
        }
        
        self.id = id
        self.userName = userName
        self.phoneNumber = phoneNumber
        self.password = password
        self.fullName = fullName
        self.address = address
        self.baseAddress = baseAddress
        self.dayOfBirth = dayOfBirth
        self.gender = gender
        self.voucher = voucher
        self.possition = possition
    }
    
    func toAnyObject() -> Any {
        return [
            "id": id,
            "userName": userName,
            "phoneNumber": phoneNumber,
            "password": password,
            "fullName": fullName,
            "address": address,
            "baseAddress": baseAddress,
            "dayOfBirth": dayOfBirth,
            "gender": gender,
            "voucher": voucher,
            "possition": possition,
            "itemOrdered": [itemOrdered]
        ]
    }
}
