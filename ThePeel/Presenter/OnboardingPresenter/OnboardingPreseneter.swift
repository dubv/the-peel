//
//  OnboardingPreseneter.swift
//  ThePeel
//
//  Created by Gone on 3/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class OnboardingPreseneter: NSObject {
    weak var onboardingDelegate: OnboardingDelegate?
    
    func pushToken(isCall: Bool, uuid: String, phoneNumber: String) {
        if isCall {
            DataManager.shared.addOrUpdateNotSpecific(completionBlock: { [weak self] ref in
                guard let `self` = self else  { return }
                
                if let token = UUToken.getToken {
                    ref.reference(withPath: Constants.adminPath).child(Constants.userTokenPath).child(uuid).setValue(token)
                    ref.reference(withPath: Constants.userGlobalPath).child(phoneNumber).child(Constants.userTokenPath).setValue(token)
                }
                
                self.onboardingDelegate?.tokenPublished()
            }) { (_) in
                self.onboardingDelegate?.tokenPublishFailed()
            }
        }
    }
}
