//
//  OnboardingDelegate.swift
//  ThePeel
//
//  Created by Gone on 3/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol OnboardingDelegate: NSObjectProtocol {
    func tokenPublished()
    func tokenPublishFailed()
}

extension OnboardingDelegate {
    public func tokenPublished() {}
    public func tokenPublishFailed() {}
}
