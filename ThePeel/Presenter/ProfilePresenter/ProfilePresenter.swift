//
//  ProfilePresenter.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class ProfilePresenter: NSObject {
    weak var profileDelegate: ProfileDelegate?
    
    public func updateUserAccessory(isCall: Bool, nameProductPath: String, idComment: String, value: CustomUser) {
        DataManager.shared.addOrUpdateNotSpecific(completionBlock: { [weak self] ref in
            guard let `self` = self else { return }
            ref.reference(withPath: Constants.productPath).child(Constants.pizzaPath).child(nameProductPath).child(Constants.commentPath).child(idComment).updateChildValues([Constants.avatarPath: value.avatar])
            ref.reference(withPath: Constants.productPath).child(Constants.pizzaPath).child(nameProductPath).child(Constants.commentPath).child(idComment).updateChildValues([Constants.userNamePath: value.userName])
            self.profileDelegate?.updateAccessoryFinished(mess: GlobalText.MESS_PROFILE_EDITED)
            }, fail: {(error) in
                self.profileDelegate?.updateAccessoryFailed(mess: error)
        })
    }
    
    public func getAllCheckoutedItem(isCall: Bool) {
        DataManager.shared.getOrRetrieve(path: Constants.itemsCheckoutPath, childPath: UUID.uuid, {[weak self] snapshot in
            guard let `self` = self else { return }
            
            var dataPayment = [ItemPayment]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot, let data = ItemPayment(snapshot: snapshot) {
                    dataPayment.append(data)
                }
            }
            self.profileDelegate?.getCheckoutedItemFinished(item: dataPayment)
            }, {(error) in
                print(error)
        })
    }
    
    public func getUserInfo(_ isCall: Bool) {
        if isCall {
            DataManager.shared.queryObserve(path: Constants.userPath, {[weak self] info in
                guard let `self` = self else { return }
                guard let user = CustomUser(snapshot: info) else { return }
                self.profileDelegate?.getUserInfoFinished(info: user)
                }, {(error) in
                    self.profileDelegate?.getUserInfoFailed(mess: error)
            })
        }
    }
    
    public func uploadAvatarImage(_ isCall: Bool, image: UIImage, path: String) {
        if isCall {
            guard let uid = UUID.uuid else { return }
            DataManager.shared.uploadImage(image, parentPath: Constants.avatarUserPath, childPath: uid, path: path, completionBlock: { [weak self] urlImage in
                guard let `self` = self, let url = urlImage else { return }
                self.profileDelegate?.uploadAvatarImageFinish(urlImage: url.absoluteString)
                }, fail: {(error) in
                    self.profileDelegate?.uploadAvatarImageFailed(mess: error)
            })
        }
    }
}
