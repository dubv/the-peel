//
//  ProfileDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol ProfileDelegate: NSObjectProtocol {
    func getUserInfoFinished(info: CustomUser)
    func getUserInfoFailed(mess: String)
    
    func uploadAvatarImageFinish(urlImage: String)
    func uploadAvatarImageFailed(mess: String?)
    
    func updateAccessoryFinished(mess: String)
    func updateAccessoryFailed(mess: String)
    
    func getCheckoutedItemFinished(item: [ItemPayment])
    func getCheckoutedItemFailed(mess: String)
}

extension ProfileDelegate {
    public func getUserInfoFinished(info: CustomUser) {}
    public func getUserInfoFailed(mess: String) {}
    
    public func uploadAvatarImageFinish(urlImage: String) {}
    public func uploadAvatarImageFailed(mess: String?) {}
    
    public func updateAccessoryFinished(mess: String) {}
    public func updateAccessoryFailed(mess: String) {}
    
    public func getCheckoutedItemFinished(item: [ItemPayment]) {}
    public func getCheckoutedItemFailed(mess: String) {}
}
