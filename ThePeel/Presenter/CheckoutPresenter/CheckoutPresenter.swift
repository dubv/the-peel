//
//  CheckoutPresenter.swift
//  ThePeel
//
//  Created by Gone on 2/20/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class CheckoutPresenter: NSObject {
    weak var checkoutDelegate: CheckoutDelegate?
    
    func pushNotification(isCall: Bool, object: Notifications) {
        DataManager.shared.queryWithoutAuth(idPath: Constants.adminPath) { [weak self] ref in
            guard let `self` = self else { return }
            ref.child(Constants.notifisPath).child(object.id).setValue(object.toAnyObject())
            self.checkoutDelegate?.pushNotificationsFinihed()
        }
    }
    
    func addItemPayment(isCall: Bool, object: ItemPayment) {
        if isCall {
            DataManager.shared.addOrUpdateWithinAuth(idPath: Constants.checkoutPath, completionBlock: {[weak self] ref in
                guard let `self` = self else { return }
                ref.child(object.idItem).setValue(object.toAnyObject())
                for item in object.cart {
                    ref.child(object.idItem).child(Constants.userCartPath).child(item.key).setValue(item.toAnyObject())
                    self.delete(isCall: true, object: item)
                    
                }
                self.checkoutDelegate?.addItemPaymentFinished(mess: GlobalText.MESS_PAYMENT_FINISHED, object: object)
                }, fail: {(error) in
                    self.checkoutDelegate?.addItemPaymentFailed(mess: error)
            })
        }
    }
    
    public func delete(isCall: Bool, object: Cart) {
        if isCall {
            DataManager.shared.delete(idPath: Constants.userPath, idChildPath: Constants.userCartPath, completionBlock: { [weak self] ref in
                    guard let `self` = self else { return }
                    ref?.child(object.key).removeValue()
                self.checkoutDelegate?.processFinised(mess: GlobalText.MESS_PAYMENT_FINISHED)
                }, fail: { (error) in
                self.checkoutDelegate?.processFailed(mess: error)
            })
        }
    }

}
