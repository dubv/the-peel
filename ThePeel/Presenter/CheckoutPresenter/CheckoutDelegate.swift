//
//  CheckoutDelegate.swift
//  ThePeel
//
//  Created by Gone on 2/20/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CheckoutDelegate: NSObjectProtocol {
    func addItemPaymentFinished(mess: String, object: ItemPayment)
    func addItemPaymentFailed(mess: String)
    
    func processFinised(mess: String)
    func processFailed(mess: String)
    
    func pushNotificationsFinihed()
    func pushNotificationsFailed()
}

extension CheckoutDelegate {
    public func addItemPaymentFinished(mess: String, object: ItemPayment) {}
    public func addItemPaymentFailed(mess: String) {}
    
    public func processFinised(mess: String) {}
    public func processFailed(mess: String) {}
    
    public func pushNotificationsFinihed() {}
    public func pushNotificationsFailed() {}
}
