//
//  RegisterPresenter.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class RegisterPresenter: NSObject {
    weak var registerDelegate: RegisterDelegate?
    
    func verifyNumberPhone(isCall: Bool, phoneNumber: String) {
        DataManager.shared.verifyPhoneNumbers(phoneNumber: phoneNumber, completionBlock: { [weak self] verificarionID in
            guard let `self` = self else { return }
            print(phoneNumber)
            self.registerDelegate?.verificationID(verificarionID)
            }, fail: {(error) in
                self.registerDelegate?.verificationIDFailed(error.localizedDescription)
        })
    }
}
