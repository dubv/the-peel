//
//  RegisterDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol RegisterDelegate: NSObjectProtocol {
    func registerFinished(_ email: String)
    func registerFailed(mess: String)
    
    func verificationID(_ verificationID: String)
    func verificationIDFailed(_ verificationID: String)
}

extension RegisterDelegate {
    public func registerFinished(_ email: String) {}
    public func registerFailed(mess: String) {}
    
    public func verificationID(_ verificationID: String) {}
    public func verificationIDFailed(_ verificationID: String) {}
}
