//
//  CartPresenter.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class CartPresenter: NSObject {
    weak var cartDelegate: CartDelegate?
    
    public func checkCurrUser(_ isCall: Bool) {
        if isCall {
            DataManager.shared.checkCurrentUser(completionBlock: { [weak self] user in
                guard let `self` = self else { return }
                self.cartDelegate?.checkCurrUserFinished(mess: user)
                }, fail: {_ in
                    self.cartDelegate?.checkCurrUserFailed(mess: GlobalText.MESS_BUTTON_AUTH_CART)
            })
        }
    }
    
    public func updateCart(isCall: Bool, object: Cart) {
        if isCall {
            DataManager.shared.addOrUpdateWithinAuth(idPath: Constants.userPath, completionBlock: {[weak self] ref in
                    guard let `self` = self else { return }
                    ref.child(Constants.userCartPath).child(object.idItem).updateChildValues(["quanlity": object.quanlity])
                    self.cartDelegate?.updateCartFinished()
                }, fail: {(error) in
                    self.cartDelegate?.updateCartFailed(mess: error)
            })
        }
    }
    
    public func queryOrdered(isCall: Bool, value: String) {
        if isCall {
            DataManager.shared.queryOrderedByValue(path: Constants.productPath, childPath: Constants.pizzaPath, sortby: Constants.sortByIdProductPath, value: value, {[weak self] snapshot in
                guard let `self` = self else { return }
                var dataPizza = [Pizza]()
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let pizza = Pizza(snapshot: snapshot){
                        dataPizza.append(pizza)
                    }
                }
                self.cartDelegate?.getPizzaFinished(dataPizza)
                }, {(error) in
                    self.cartDelegate?.getPizzaFailed(error.localizedDescription)
            })
            
            DataManager.shared.queryOrderedByValue(path: Constants.productPath, childPath: Constants.drinkPath, sortby: Constants.sortByIdProductPath, value: value, {[weak self] snapshot in
                guard let `self` = self else { return }
                var dataDrink = [Drink]()
                
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let drink = Drink(snapshot: snapshot) {
                        dataDrink.append(drink)
                    }
                }
                self.cartDelegate?.getDrinkFinished(dataDrink)
                }, {(error) in
                    self.cartDelegate?.getDrinkFailed(error.localizedDescription)
            })
            
            DataManager.shared.queryOrderedByValue(path: Constants.productPath, childPath: Constants.dessertPath, sortby: Constants.sortByIdProductPath, value: value, {[weak self] snapshot in
                guard let `self` = self else { return }
                var dataDessert = [Dessert]()
                
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let dessert = Dessert(snapshot: snapshot) {
                        dataDessert.append(dessert)
                    }
                }
                self.cartDelegate?.getDessertFinished(dataDessert)
                }, {(error) in
                    self.cartDelegate?.getDessertFailed(error.localizedDescription)
            })
        }
    }
    
    public func getCartObserver(_ isCall: Bool) {
        if isCall {
            DataManager.shared.getOrRetrieveWithAuth(path: Constants.userPath, childPath: Constants.userCartPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var dataCart = [Cart]()
                
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = Cart(snapshot: snapshot) {
                        dataCart.append(data)
                    }
                }
                self.cartDelegate?.getCartFinished(dataCart)
                }, {(error) in
                    self.cartDelegate?.getCartFailed(error)
            })
        }
    }
    
    public func getCartsBasedUserCurrent(_ isCall: Bool){
        if isCall {
            DataManager.shared.getOrRetrieveWithAuth(path: Constants.userPath, childPath: Constants.userCartPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var dataCart = [Cart]()
                
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = Cart(snapshot: snapshot) {
                        dataCart.append(data)
                    }
                }
                self.cartDelegate?.getCartFinished(dataCart)
                }, {(error) in
                    self.cartDelegate?.getCartFailed(error)
            })
        }
    }
}
