//
//  CartDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CartDelegate: NSObjectProtocol {
    func getPizzaFinished(_ data: [Pizza])
    func getPizzaFailed(_ data: String)
    func getDrinkFinished(_ data: [Drink])
    func getDrinkFailed(_ data: String)
    func getDessertFinished(_ data: [Dessert])
    func getDessertFailed(_ data: String)
    
    func getCartFinished(_ data: [Cart])
    func getCartFailed(_ mess: String)
    
    func checkCurrUserFinished(mess: String)
    func checkCurrUserFailed(mess: String)
    
    func updateCartFinished()
    func updateCartFailed(mess: String)
}

extension CartDelegate {
    public func getPizzaFinished(_ data: [Pizza]) {}
    public func getPizzaFailed(_ data: String) {}
    public func getDrinkFinished(_ data: [Drink]) {}
    public func getDrinkFailed(_ data: String) {}
    public func getDessertFinished(_ data: [Dessert]) {}
    public func getDessertFailed(_ data: String) {}
    
    public func getCartFinished(_ data: [Cart]) {}
    public func getCartFailed(_ mess: String) {}
    
    public func checkCurrUserFinished(mess: String) {}
    public func checkCurrUserFailed(mess: String) {}
    
    public func updateCartFinished() {}
    public func updateCartFailed(mess: String) {}
}
