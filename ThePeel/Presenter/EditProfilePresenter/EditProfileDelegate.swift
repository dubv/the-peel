//
//  EditProfileDelegate.swift
//  ThePeel
//
//  Created by Gone on 2/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol EditProfileDelegate: NSObjectProtocol {
    func editProfileFinished(_ mess: String)
    
    func verifyPasswordFinished(_ mess: String)
    func verifyPasswordFailed(_ mess: String)
    
    func updateFinished()
    func updateFailed(_ mess: String)
}

extension EditProfileDelegate {
    public func editProfileFinished(_ mess: String) {}
    
    public func verifyPasswordFinished(_ mess: String) {}
    public func verifyPasswordFailed(_ mess: String) {}
    
    public func updateFinished() {}
    public func updateFailed(_ mess: String) {}
}
