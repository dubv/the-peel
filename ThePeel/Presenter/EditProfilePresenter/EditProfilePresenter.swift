//
//  EditProfilePresenter.swift
//  ThePeel
//
//  Created by Gone on 2/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class EditProfilePresenter: NSObject {
    weak var editProfileDelegate: EditProfileDelegate?
    
    public func checkPassword(isCall: Bool, password: String ){
        if isCall {
            DataManager.shared.queryObserveSingleEvent(path: Constants.userPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                guard let customUser = CustomUser(snapshot: snapshot) else { return }
                if password == customUser.password {
                    self.editProfileDelegate?.verifyPasswordFinished(password)
                } else {
                    self.editProfileDelegate?.verifyPasswordFailed(GlobalText.MESS_WRONG_PASSWORD)
                }
                }, {(error) in
                    self.editProfileDelegate?.verifyPasswordFailed(error)
            })
        }
    }
    
    func editProfileInfo(_ isCall: Bool, childPath: String, value: String) {
        DataManager.shared.addOrUpdateWithinAuth(idPath: Constants.userPath, completionBlock: { [weak self] ref in
            guard let `self` = self else { return }
            ref.updateChildValues([childPath: value])
            self.editProfileDelegate?.editProfileFinished(GlobalText.MESS_PROFILE_EDITED)
            }, fail: {(error) in
                print(error)
        })
    }
    
    func editGlobalUserInfo(_ isCall: Bool, path: String, childPath: String, value: String) {
        DataManager.shared.queryWithoutAuth(idPath: path) { [weak self] ref in
            guard let `self` = self else { return }
            ref.child(childPath).updateChildValues([Constants.passwordPath: value])
            self.editProfileDelegate?.updateFinished()
        }
    }
}
