//
//  ProductDetailPresenter.swift
//  ThePeel
//
//  Created by Gone on 2/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class ProductDetailPresenter: NSObject {
    weak var productDetailDelegate: ProductDetailDelegate?
    
    func handleLikes(isCall: Bool, productId: String) {
        DataManager.shared.addOrUpdateWithTransaction(productPath: Constants.productPath, childPath: Constants.pizzaPath, productId: productId, reactPath: Constants.likeByUserPath, reactCountPath: Constants.likeCountPath, completionBlock: {[weak self] in
            guard let `self` = self else { return }
            self.productDetailDelegate?.likeFinished()
            }, fail: {(error) in
                guard let error = error else { return }
                self.productDetailDelegate?.likeFailed(error)
        })
    }
    
    func addComment(isCall: Bool, object: Comment, nameObject: String, uidComment: String) {
        DataManager.shared.addOrUpdateNotSpecific(completionBlock: { [weak self] userRef in
            guard let `self` = self, let userId = UUID.uuid else { return }
            
            userRef.reference(withPath: Constants.productPath).child(Constants.pizzaPath).child(nameObject).child(Constants.commentPath).child(uidComment).setValue(object.toAnyObject())
            
            userRef.reference(withPath: Constants.userPath).child(userId).child(Constants.commentPath).child(uidComment).setValue(object.toAnyObject())
            self.productDetailDelegate?.addCommentFinished()
        }, fail: {(error) in
            self.productDetailDelegate?.addCommentFailed(error)
        })
    }
}
