//
//  ProductDetailDelegate.swift
//  ThePeel
//
//  Created by Gone on 2/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol ProductDetailDelegate: NSObjectProtocol {
    func addCommentFinished()
    func addCommentFailed(_ mess: String)
    
    func likeFinished()
    func likeFailed(_ mess: String)
}

extension ProductDetailDelegate {
    public func addCommentFinished() {}
    public func addCommentFailed(_ mess: String) {}
    
    public func likeFinished() {}
    public func likeFailed(_ mess: String) {}
}
