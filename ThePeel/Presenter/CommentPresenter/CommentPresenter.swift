//
//  CommentPresenter.swift
//  ThePeel
//
//  Created by Gone on 2/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class CommentPresenter: NSObject {
    weak var commentDelegate: CommentDelegate?
    
    public func queryOrdered(isCall: Bool, sortBy: String, value: String) {
        if isCall {
            DataManager.shared.queryObserveOrderedByValue(path: Constants.productPath, childPath: Constants.pizzaPath, sortby: sortBy, value: value, { [weak self] snapshot in
                guard let `self` = self else { return }
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = Pizza(snapshot: snapshot) {
                        self.commentDelegate?.getCommentFinished(data)
                    }
                }
                }, { (error) in
                    self.commentDelegate?.getCommentFailed(error.localizedDescription)
            })
        }
    }
}
