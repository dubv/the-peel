//
//  CommentDelegate.swift
//  ThePeel
//
//  Created by Gone on 2/16/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CommentDelegate: NSObjectProtocol {
    func getCommentFinished( _ data: Pizza)
    func getCommentFailed( _ mess: String)
}

extension CommentDelegate {
    public func getCommentFinished( _ data: Pizza) {}
    public func getCommentFailed( _ mess: String) {}
}
