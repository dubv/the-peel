//
//  CategoryPresenter.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class CategoryPresenter: NSObject {
    weak var categoryDelegate: CategoryDelegate?
    
    public func queryOrderedCategory(isCall: Bool, sortBy: String, value: String) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.pizzaPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var pizzaData = [Pizza]()
                
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let pizza = Pizza(snapshot: snapshot) {
                        if pizza.category.contains("#\(value.lowercased())") {
                            pizzaData.append(pizza)
                        }
                    }
                }
                self.categoryDelegate?.getDataSortCategoryFinished(pizzaData)
            }) { (error) in
                print(error)
            }
        }
    }
    
    public func queryByComposition(isCall: Bool, value: String) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.pizzaPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var pizzaData = [Pizza]()
                
                var valueNeedToSearch = value.lowercased().split(separator: " ")
                valueNeedToSearch.removeDuplicates()
                for values in valueNeedToSearch {
                    let valueConvert = values.replacingOccurrences(of: "’s", with: "")
                    for child in snapshot.children {
                        if let snapshot = child as? DataSnapshot, let pizza = Pizza(snapshot: snapshot), let composition = pizza.detail?.composition {
                                if composition.lowercased().contains(valueConvert) {
                                  pizzaData.append(pizza)
                                }
                        }
                    }
                }
                self.categoryDelegate?.getDataSortCategoryFinished(pizzaData)
            }) { (error) in
                print(error)
            }
        }}
}
