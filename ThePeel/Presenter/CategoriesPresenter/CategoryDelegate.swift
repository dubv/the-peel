//
//  CategoryDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CategoryDelegate: NSObjectProtocol {
    func getDataSortCategoryFinished (_ pizza: [Pizza])
    func getDataSortCategoryFailed(_ mess: String)
}

protocol PassDataToHome: NSObjectProtocol {
    func passingData(_ pizza: [Pizza])
}

extension CategoryDelegate {
    public func getDataSortCategoryFinished(_ pizza: [Pizza]) {}
    public func getDataSortCategoryFailed(_ mess: String) {}
}
