//
//  VerifyDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/25/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol VerifyDelegate: NSObjectProtocol {
    func verifyFinished(_ uid: String)
    func verifyFailed(_ mess: String)
    
    func verifyPasswordFinished(_ mess: String)
    func verifyPasswordFailed(_ mess: String)
}

extension VerifyDelegate {
    public func verifyFinished(_ uid: String) {}
    public func verifyFailed(_ mess: String) {}
    
    public func verifyPasswordFinished(_ mess: String) {}
    public func verifyPasswordFailed(_ mess: String) {}
    
}
