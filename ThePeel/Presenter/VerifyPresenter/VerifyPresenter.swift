//
//  VerifyPresenter.swift
//  ThePeel
//
//  Created by Gone on 1/25/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class VerifyPresenter: NSObject {
    weak var verifyDelegate: VerifyDelegate?
    weak var registerDelegate: RegisterDelegate?
    weak var signinDelegate: SignInDelegate?
    
    func verifyNumberPhone(_ isCall: Bool, verificationID: String, verificationCode: String) {
        if isCall {
            DataManager.shared.signInAndRetrieve(verificationID: verificationID, verificationCode: verificationCode, completionBlock: { [weak self] uid in
                guard let `self` = self else { return }
                self.verifyDelegate?.verifyFinished(uid)
                }, fail: { (error) in
                    self.verifyDelegate?.verifyFailed(error.localizedDescription)
            })
        }
    }
    
    public func register(isCall: Bool, uid: String, userName: String, email: String, phoneNumber: String, password: String) {
        if isCall {
            let customUser = CustomUser(key: "", avatar: "", descriptions: "", externalLink: "", firstName: "", lastName: "", userName: userName, email: email, password: password, phoneNumber: phoneNumber, address: "", cart: nil, comment: [Comment]())
            let ticks = Date().ticks
            let lastTimeActive = String(ticks)
            let userGlobal = UserGlobal(key: "", uid: uid, password: password, lastTimeActive: lastTimeActive, loginCount: 0, userToken: "")
            
            DataManager.shared.addOrUpdateWithinAuth(idPath: Constants.userPath, completionBlock: { [weak self] ref in
                ref.setValue(customUser.toAnyObject())
                DataManager.shared.queryWithoutAuth( idPath: Constants.userGlobalPath, completionBlock: { ref in
                    ref.child(phoneNumber).setValue(userGlobal.toAnyObject())
                })
                
                self?.registerDelegate?.registerFinished("mess")
            }) { (error) in
                self.registerDelegate?.registerFailed(mess: error)
            }
        }
    }
}
