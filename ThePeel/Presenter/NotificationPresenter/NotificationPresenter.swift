//
//  NotificationDelegate.swift
//  ThePeel
//
//  Created by Gone on 3/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class NotificationPresenter: NSObject {
    weak var notificationDelegate: NotificationDelegate?
    
    func getItemCheckoutedBaseId(isCall: Bool, itemId: String) {
        if isCall {
            DataManager.shared.getOrRetrieveWithAuth(path: Constants.checkoutPath, childPath: itemId, { [weak self] snapshot in
                guard let `self` = self else { return }
                if let itemCheckouted = ItemPayment(snapshot: snapshot) {
                    self.notificationDelegate?.getItemCheckoutedFinished(item: itemCheckouted)
                } else {
                    self.notificationDelegate?.getItemCheckoutedFailed(mess: GlobalText.TITLE_POPUP_EMPTY_NOT_AVAILABEL)
                }
            }) { (_) in
                self.notificationDelegate?.getItemCheckoutedFailed(mess: GlobalText.MESS_CHECK_USER_FAILED)
            }
        }
    }
    
    func getNotifisUnread(isCall: Bool, value: Bool) {
        if isCall {
            if let uid = UUID.uuid {
                DataManager.shared.queryWithoutAuth(idPath: Constants.userPath) { [weak self] ref in
                    guard let `self` = self else { return }
                    var notifis = [Notifications]()
                    ref.child(uid).child(Constants.notifisPath).queryOrdered(byChild: Constants.readPath).queryEqual(toValue: value).observe(DataEventType.value, with: { snapshot in
                        for child in snapshot.children {
                            if let snapshot = child as? DataSnapshot, let data = Notifications(snapshot: snapshot) {
                                notifis.append(data)
                            }
                        }
                        self.notificationDelegate?.getNotifisUnreadFinished(count: notifis.count)
                        notifis.removeAll()
                    })
                }
            }
        }
    }
    
    func getPizzaBaseId(isCall: Bool, itemId: String) {
        if isCall {
            DataManager.shared.queryObserveWithoutUUId(path: Constants.productPath, childPath: Constants.pizzaPath, key: itemId, { [weak self] snapshot in
                guard let `self` = self else { return }
                guard let data = Pizza(snapshot: snapshot) else { return }
                self.notificationDelegate?.getProductFinished(product: data)
            }) { (_) in
                self.notificationDelegate?.getProductFailed()
            }
        }
    }
    
    func getAllNotifis(isCall: Bool) {
        DataManager.shared.getOrRetrieveWithAuth(path: Constants.userPath, childPath: Constants.notifisPath, { [weak self] snapshot in
            guard let `self` = self else { return }
            var notifis = [Notifications]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot, let data = Notifications(snapshot: snapshot) {
                    notifis.append(data)
                }
            }
            self.notificationDelegate?.getNotifisFinished(notifis: notifis)
        }) { (_) in
            self.notificationDelegate?.getNotifisFailed()
        }
    }
}
