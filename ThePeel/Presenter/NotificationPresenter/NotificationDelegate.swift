//
//  NotificationPresenter.swift
//  ThePeel
//
//  Created by Gone on 3/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol NotificationDelegate: NSObjectProtocol {
    func getNotifisFinished(notifis: [Notifications])
    func getNotifisFailed()
    
    func getItemCheckoutedFinished(item: ItemPayment)
    func getItemCheckoutedFailed(mess: String)
    
    func getProductFinished(product: Any)
    func getProductFailed()
    
    func getNotifisUnreadFinished(count: Int)
    func getNotifisUnreadFailed()
}

extension NotificationDelegate {
    public func getNotifisFinished(notifis: [Notifications]) {}
    public func getNotifisFailed() {}
    
    public func getItemCheckoutedFinished(item: ItemPayment) {}
    public func getItemCheckoutedFailed(mess: String) {}
    
    public func getProductFinished(product: Any) {}
    public func getProductFailed() {}
    
    public func getNotifisUnreadFinished(count: Int) {}
    public func getNotifisUnreadFailed() {}
}
