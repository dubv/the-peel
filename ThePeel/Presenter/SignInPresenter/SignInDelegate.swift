//
//  SignInDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

protocol SignInDelegate: NSObjectProtocol {
    func signInVerificationID(_ id: String)
    func signInVerificationIDFailed(_ mess: String)
    
    func siginFinished()
    func siginFailed(mess: String)
    
    func getSuffixesFinished(suffixes: [CountrySuffixes])
}

extension SignInDelegate {
    public func signInVerificationID(_ id: String) {}
    public func signInVerificationIDFailed(_ mess: String) {}
    
    public func siginFinished() {}
    public func siginFailed(mess: String) {}
    
    public func getSuffixesFinished(suffixes: [CountrySuffixes]) {}
}
