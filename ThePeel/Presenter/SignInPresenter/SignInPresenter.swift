//
//  SignInPresenter.swift
//  ThePeel
//
//  Created by Gone on 1/22/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class SignInPresenter: NSObject {
    weak var signinDelegate: SignInDelegate?
    
    func verifyNoPhone(isCall: Bool, phoneNumber: String) {
        DataManager.shared.verifyPhoneNumbers(phoneNumber: phoneNumber, completionBlock: { [weak self] verificarionID in
            guard let `self` = self else { return }
            self.signinDelegate?.signInVerificationID(verificarionID)
            }, fail: {(error) in
                self.signinDelegate?.signInVerificationIDFailed(error.localizedDescription)
        })
    }
    
    func siginWithPassAndNoPhone(isCall: Bool, phoneNumber: String, password: String) {
        if isCall {
            DataManager.shared.getOrRetrieveNotObserve(path: Constants.userGlobalPath, childPath: phoneNumber, { [weak self] snapshot in
                guard let `self` = self else { return }
                guard let user = UserGlobal(snapshot: snapshot) else { return }
                let ticks = Date().ticks
                guard let lastestTime = UInt64(user.lastTimeActive) else { return }
                let currTimeActive = ticks - lastestTime
                
                /// If login lastest time to current is > 1 hour, then we'll reset login count
                if user.password == password {
                    if currTimeActive > UInt64(3.6*pow(10, 9)/0.1) {
                        snapshot.ref.updateChildValues(["loginCount": 0])
                    }
                    snapshot.ref.updateChildValues(["lastTimeActive": String(ticks)])
                    DataManager.shared.getOrRetrieveNotObserve(path: Constants.userGlobalPath, childPath: phoneNumber, { [weak self] snapshot in
                        guard let `self` = self else { return }
                        guard let users = UserGlobal(snapshot: snapshot) else { return }
                        if user.loginCount <= 5 {
                            snapshot.ref.updateChildValues(["loginCount": users.loginCount+1])
                            self.signinDelegate?.siginFinished()
                        } else {
                            self.signinDelegate?.siginFailed(mess: GlobalText.MESS_SIGNIN_OVERFLOW)
                        }
                    }, { (_) in })
                } else {
                    self.signinDelegate?.siginFailed(mess: GlobalText.MESS_WRONG_PASSWORD)
                }
            }) { (error) in
                self.signinDelegate?.siginFailed(mess: error)
            }
        }
    }
    
    func getPhoneNumberSuffixes(_ isCall: Bool) {
        LocalService.shared.getOrRetrieve({ [weak self] suffixes in
            self?.signinDelegate?.getSuffixesFinished(suffixes: suffixes)
        })
    }
}
