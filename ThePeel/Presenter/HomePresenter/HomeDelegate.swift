//
//  HomeDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol AddProductToCartDelegate: NSObjectProtocol {
    func saveProduct(_ id: String, _ quanlity: String, _ favicon: String, name: String, price: String, size: String, desc: String)
}

protocol HomeDelegate: NSObjectProtocol {
    
    func getAllPizza(pizza: [Pizza])
    func getAllPizzaFailed(_ mess: String)
    
    func sortProductByKey(pizza: Pizza)
    func sortProductByKeyFailed()
    
    func getAllDrink(drink: [Drink])
    func getAllDrinkFailed(_ mess: String)
    
    func getAllDessert(dessert: [Dessert])
    func getAllDessertFailed(_ mess: String)
    
    func getAllProduct(product: Product)
    func getAllProductFailed(_ mess: String)
    
    func getAllHeroImage(images: [HeroPost])
    func getAllHeroImageFailed(_ mess: String)
    
    func addOrUpdate(object: Cart, favicon: String)
    func addOrUpdateFailed(mess: String)

}

extension HomeDelegate {
    public func getAllProduct(product: Product) {}
    public func getAllProductFailed(_ mess: String) {}
    
    public func getAllPizza(pizza: [Pizza]) {}
    public func getAllPizzaFailed(_ mess: String) {}
    
    public func sortProductByKey(pizza: Pizza) {}
    public func sortProductByKeyFailed() {}

    public func getAllDrink(drink: [Drink]){}
    public func getAllDrinkFailed(_ mess: String){}
    
    public func getAllDessert(dessert: [Dessert]) {}
    public func getAllDessertFailed(_ mess: String) {}
    
    public func getAllHeroImage(images: [HeroPost]) {}
    public func getAllHeroImageFailed(_ mess: String) {}
    
    public func addOrUpdate(object: Cart, favicon: String) {}
    public func addOrUpdateFailed(mess: String) {}
}
