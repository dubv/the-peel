//
//  HomePresenter.swift
//  ThePeel
//
//  Created by Gone on 1/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class HomePresenter: NSObject {
    weak var homeDelegate: HomeDelegate?
    
    public func addProductToCart(isCall: Bool, object: Cart, favicon: String) {
        if isCall {
            DataManager.shared.addOrUpdateWithinAuth(idPath: Constants.userPath, completionBlock: {[weak self] ref in
                guard let `self` = self else { return }
                ref.child(Constants.userCartPath).child(object.idItem).setValue(object.toAnyObject())
                    self.homeDelegate?.addOrUpdate(object: object, favicon: favicon)
                }, fail: {(error) in
//                    self.homeDelegate?.addOrUpdateFailed(mess: error)
                    print(error)
            })
        }
    }
    
    public func getAllHeroImage(isCall: Bool) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.heroimage, { [weak self] snapshot in
                guard let `self` = self else { return }
                var heroImg = [HeroPost]()
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = HeroPost(snapshot: snapshot) {
                        heroImg.append(data)
                    }
                }
                self.homeDelegate?.getAllHeroImage(images: heroImg)
                }, { (error) in
                    self.homeDelegate?.getAllHeroImageFailed(error.localizedDescription)
            })
        }
    }

    public func getAllPizza(isCall: Bool) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.pizzaPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var pizzaData = [Pizza]()
                    for child in snapshot.children {
                        if let snapshot = child as? DataSnapshot, let data = Pizza(snapshot: snapshot) {
                            pizzaData.append(data)
                        }
                    }
                self.homeDelegate?.getAllPizza(pizza: pizzaData)
                }, { (error) in
                    self.homeDelegate?.getAllPizzaFailed(error.localizedDescription)
            })
        }
    }
    
    public func getAllDrink(isCall: Bool) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.drinkPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var drinkData = [Drink]()
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = Drink(snapshot: snapshot) {
                        drinkData.append(data)
                    }
                }
                self.homeDelegate?.getAllDrink(drink: drinkData)
                }, { (error) in
                    self.homeDelegate?.getAllDrinkFailed(error.localizedDescription)
            })
        }
    }
    
    public func getAllDessert(isCall: Bool) {
        if isCall {
            DataManager.shared.getOrRetrieve(path: Constants.productPath, childPath: Constants.dessertPath, { [weak self] snapshot in
                guard let `self` = self else { return }
                var dessertData = [Dessert]()
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let data = Dessert(snapshot: snapshot) {
                        dessertData.append(data)
                    }
                }
                self.homeDelegate?.getAllDessert(dessert: dessertData)
                }, { (error) in
                    print(error)
            })
        }
    }
}
