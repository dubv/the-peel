//
//  PopupViewController.swift
//  ThePeel
//
//  Created by Gone on 1/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class PopupViewController: BaseViewController {
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(dismissVCNormal), userInfo: nil, repeats: false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupUI()
    }
    
    // MARK: - Initialization
    static func viewController() -> PopupViewController? {
        return Helper.getViewController(named: "PopupViewController", inSb: "Main")
    }
    
    fileprivate func setupUI() {
        self.view.backgroundColor = UIColor.clear
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
}
