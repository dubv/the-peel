//
//  ValidateLabel.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ValidateLabel: UILabel {

    // MARK: - Properties
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateImage()
        }
    }
    
    @IBInspectable var titleLabel: String = "" {
        didSet {
            self.text = titleLabel
        }
    }
    
    @IBInspectable var labelBG: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: labelBG)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    private func updateImage() {
        self.font = UIFont(name: Constants.PFRegular, size: 15)
        self.textColor = UIColor(hexString: "F55252")
    }
}
