//
//  CustomToAuthButton.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CustomToAuthButton: UIButton {

    // MARK: - Properties
    var spacingInset: CGFloat = 10
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateButton()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var titleButton: String = "" {
        didSet {
            self.titleLabel?.text = titleButton
            self.addTextSpacing(0.2)
        }
    }
    
    @IBInspectable var backgroundBtn: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: backgroundBtn)
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 0.0 {
        didSet {
            guard let font = Constants.OpenSans[2] else { return }
            if fontSize != 0.0 {
                self.titleLabel?.font = UIFont(name: font, size: fontSize)
            } else {
                self.titleLabel?.font = UIFont(name: font, size: 20)
            }
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    private func updateButton() {
        self.setTitleColor(UIColor.white, for: .normal)
        self.tintColor = .white
        self.titleEdgeInsets = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
    }
}
