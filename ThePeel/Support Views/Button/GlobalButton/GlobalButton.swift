//
//  GlobalButton.swift
//  ThePeel
//
//  Created by Gone on 3/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialButtons_ButtonThemer
import MaterialComponents.MaterialButtons_ColorThemer

class GlobalButton: UIView {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var letsGoButton: MDCButton!
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }

    // MARK: - Initialization
    // MARK: - Private Method
    
    // MARK: - Public Method
    public func setupUI() {
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor(hexString: "003A64").cgColor
        self.backgroundColor = UIColor(hexString: "003A64")
        
        if let font = Constants.OpenSans[1] {
            letsGoButton.setTitleFont(UIFont(name: font, size: 20), for: .normal)
        }
        
        let buttonScheme = MDCButtonScheme()
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.primaryColor = UIColor(hexString: "FFFFFF")
        letsGoButton.setElevation(ShadowElevation(rawValue: 4), for: .highlighted)
        MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: letsGoButton)
        MDCOutlinedButtonColorThemer.applySemanticColorScheme(colorScheme, to: letsGoButton)
        letsGoButton.setBorderColor(.white, for: .normal)
        letsGoButton.setBorderWidth(0, for: .normal)
    }

}
