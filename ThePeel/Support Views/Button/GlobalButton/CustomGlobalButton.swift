//
//  CustomGlobalButton.swift
//  ThePeel
//
//  Created by Gone on 2/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CustomGlobalButton: UIButton {

    // MARK: - Properties
    var spacingInset: CGFloat = 10
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var buttonIconName: String = "" {
        didSet {
            updateImage(named: buttonIconName)
        }
    }
    
    @IBInspectable var buttonColorBG: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: buttonColorBG)
        }
    }
    
    @IBInspectable var titleColors: String = "" {
        didSet {
            if titleColors != "" {
                self.setTitleColor(UIColor(hexString: titleColors), for: .normal)
            } else {
                self.setTitleColor(UIColor(hexString: GlobalColor.white), for: .normal)
            }
        }
    }
    
    @IBInspectable var titleButton: String = "" {
        didSet {
            self.setTitle(titleButton, for: .normal)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    private func updateImage(named: String) {
        let bundle = Bundle(for: CustomCoverageButton.self)
        let image = UIImage(named: named, in: bundle, compatibleWith:nil)
        self.backgroundColor = UIColor.black
        
        self.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.setImage(image, for: .normal)
        self.tintColor = .white
        
        self.titleLabel?.font = UIFont(name: Constants.PFRegular, size: 20)
        
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacingInset)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: spacingInset, bottom: 0, right: 0)
    }
}
