//
//  AddToCartDelegate.swift
//  ThePeel
//
//  Created by Gone on 1/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol AddToCartButtonDelegate: NSObjectProtocol {
    func buttonIn()
}

extension AddToCartButtonDelegate {
    public func buttonIn() {}
}
