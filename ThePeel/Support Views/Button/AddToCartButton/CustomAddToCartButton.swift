//
//  CustomAddToCartButton.swift
//  ThePeel
//
//  Created by Gone on 1/23/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CustomAddToCartButton: UIButton {
    
    // MARK: - Properties
    weak var delegate: AddToCartButtonDelegate?
    var spacingInset: CGFloat = 10
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateImage()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var titleColors: String = "" {
        didSet {
            if titleColors != "" {
                self.setTitleColor(UIColor(hexString: titleColors), for: .normal)
            } else {
                self.setTitleColor(UIColor(hexString: GlobalColor.white), for: .normal)
            }
        }
    }
    
    @IBInspectable var titleButton: String = "" {
        didSet {
            self.setTitle(titleButton, for: .normal)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    internal func setup() {
        self.addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    private func updateImage() {
        let bundle = Bundle(for: CustomCoverageButton.self)
        let image = UIImage(named: "iconOrder", in: bundle, compatibleWith:nil)
        self.backgroundColor = UIColor.black
        
        self.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.setImage(image, for: .normal)
        self.tintColor = .white
        
        self.titleLabel?.font = UIFont(name: Constants.PFRegular, size: 20)
        
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacingInset)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: spacingInset, bottom: 0, right: 0)
    }
    
    // MARK: - Target
    @objc private func tapped() {
        delegate?.buttonIn()
    }

}
