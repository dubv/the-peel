//
//  ToPaymentButton.swift
//  ThePeel
//
//  Created by Gone on 1/19/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class ToPaymentButton: UIButton {
    
    // MARK: - Properties
    var spacingInset: CGFloat = 15
    @IBInspectable var checked: Bool = false {
        didSet {
            updateImage()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var image: String = "" {
        didSet {
            if image != "" {
                let bundle = Bundle(for: type(of: self))
                let img = UIImage(named: image, in: bundle, compatibleWith: nil)
                self.setImage(img, for: .normal)
            } else {
                let bundle = Bundle(for: type(of: self))
                let img = UIImage(named: "", in: bundle, compatibleWith: nil)
                self.setImage(img, for: .normal)
            }
        }
    }
    
    @IBInspectable var titleColors: String = "" {
        didSet {
            if titleColors != "" {
                self.setTitleColor(UIColor(hexString: titleColors), for: .normal)
            } else {
                self.setTitleColor(UIColor(hexString: GlobalColor.white), for: .normal)
            }
        }
    }
    
    @IBInspectable var titleButton: String = "" {
        didSet {
            self.titleLabel?.text = titleButton
            self.addTextSpacing(1.0)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    internal func setup() {
        
    }
    
    private func updateImage() {
        self.backgroundColor = UIColor.black
        
        self.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.tintColor = .white
        
        self.titleLabel?.font = UIFont(name: Constants.PFRegular, size: 20)
        self.titleEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: spacingInset)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: spacingInset, bottom: 0, right: 0)
    }
}
