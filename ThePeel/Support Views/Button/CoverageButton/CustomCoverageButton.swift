//
//  CustomCoverageButton.swift
//  ThePeel
//
//  Created by Gone on 1/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CustomCoverageButton: UIButton {

    // MARK: - Properties
    var spacingInset: CGFloat = 10
    @IBInspectable var checked: Bool = false {
        didSet {
            updateImage()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }

    // MARK: - Private Method
    internal func setup() {
        
    }
    
    private func updateImage() {
        let bundle = Bundle(for: CustomCoverageButton.self)
        let image = checked ? UIImage(named: "iconMap", in: bundle, compatibleWith:nil) : UIImage(named: "iconMap", in: bundle, compatibleWith:nil)
        
        self.backgroundColor = UIColor(hexString: "EDEDED")
        
        self.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.setImage(image, for: .normal)
        self.setTitle("Check delivery area", for: .normal)
        
        self.titleLabel?.font = UIFont(name: Constants.PFRegular, size: 20)
        self.setTitleColor(UIColor.black, for: .normal)
        self.tintColor = .black
        
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacingInset)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: spacingInset, bottom: 0, right: 0)
    }
}
