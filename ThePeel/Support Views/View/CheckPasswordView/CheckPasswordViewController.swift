//
//  CheckPasswordViewController.swift
//  ThePeel
//
//  Created by Gone on 3/4/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CheckPasswordViewDelegate: NSObjectProtocol {
    func checkPass(pass: String)
}

class CheckPasswordViewController: UIViewController {
    // MARK: - Properties
    weak var checkPasswordViewDelegate: CheckPasswordViewDelegate?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Initialization
    static func viewController() -> CheckPasswordViewController? {
        return Helper.getViewController(named: "CheckPasswordViewController", inSb: "Main")
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service

}
