//
//  CustomSegmentedControl.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol CustomProfileSegmentedControlDelegate: NSObjectProtocol {
    func changeToIndex(index:Int)
}

enum StatusButton: Int, CaseIterable {
    case OFF = 0, ON
    var value: String {
        switch self {
        case .ON:
            return "Profile"
        case .OFF:
            return "Purchases"
        }
    }
}

class CustomProfileSegmentedControl: UIView {
    private var buttons: [UIButton]!
    private var selectorView: UIView!
    
    var textColor:UIColor = .black
    var selectorViewColor: UIColor = .red
    var selectorTextColor: UIColor = .red
    
    var statusButton: StatusButton = .ON {
        didSet {
            //update button
            updateButton()
        }
    }
    
    var index: Int = 0
    weak var delegate: CustomProfileSegmentedControlDelegate?
    public var selectedIndex : Int = 0
    
    convenience init(newFrame:CGRect) {
        self.init(frame: newFrame)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.backgroundColor = UIColor.clear
        updateView()
    }
    
    func setButtonTitles(buttonTitles:[String]) {
        self.updateView()
    }
    
    func updateButton() {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            if buttonIndex == statusButton.rawValue {
                
                let selectorPosition = frame.width/CGFloat(StatusButton.allCases.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex
                delegate?.changeToIndex(index: selectedIndex)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
    }
    
    @objc func buttonAction(sender: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            if btn == sender {
                let selectorPosition = frame.width/CGFloat(StatusButton.allCases.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex
                delegate?.changeToIndex(index: selectedIndex)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
    }
}

//Configuration View
extension CustomProfileSegmentedControl {
    private func updateView() {
        createButton()
        configSelectorView()
        configStackView()
    }
    
    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    private func configSelectorView() {
        let selectorWidth = frame.width / CGFloat(StatusButton.allCases.count)
        selectorView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: selectorWidth, height: 2))
        selectorView.backgroundColor = selectorViewColor
        addSubview(selectorView)
    }
    
    private func createButton() {
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for index in StatusButton.allCases {
            let button = UIButton(type: .system)
            button.setTitle(index.value, for: .normal)
            button.addTarget(self, action:#selector(CustomProfileSegmentedControl.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(.black, for: .normal)
            buttons.append(button)
        }
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
    }
}
