//
//  PinCodeTextField.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

import UIKit

protocol PinCodeTextFieldProtocol: UITextFieldDelegate {
    var isFilled: Bool { get }
    var pinCodes: String { get }
    func didPressBackspace(textField : PinCodeTextField)
}

class PinCodeTextField: UITextField {
    
    var isFilled: Bool {
        return self.pinCode.count > 0
    }
    var pinCode: String {
        return self.text ?? ""
    }
    
    var maxCharacterCount: Int = 1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.borderColor = UIColor.lightText.cgColor
        self.layer.borderWidth = 0.0
        self.layer.cornerRadius = 0.0
        self.borderStyle = .none
        self.backgroundColor = UIColor(hexString: GlobalColor.gray)
        self.inputAccessoryView = self.generateToolbar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        (self.delegate as? PinCodeTextFieldProtocol)?.didPressBackspace(textField: self)
    }
    
    func generateToolbar() -> UIToolbar {
        let width = UIScreen.main.bounds.width
        
        let toolbar =  UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: width, height: 60.0))
        let itme1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let item2 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(doneButtonDidPressed))
        toolbar.setItems([itme1, item2], animated: false)
        return toolbar
    }
    
    @objc func doneButtonDidPressed() {
        self.resignFirstResponder()
    }
}
