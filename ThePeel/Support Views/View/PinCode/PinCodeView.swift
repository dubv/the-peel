//
//  PinCodeView.swift
//  ThePeel
//
//  Created by Gone on 1/24/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class PinCodeView: UIView {

    var textFields = [PinCodeTextField]()
    var inputComplete: ((Bool, String) -> Void)?
    
    var pinCodeCount: Int = 1 {
        didSet {
            for tf in textFields {
                tf.removeFromSuperview()
            }
            textFields.removeAll()
            
            for _ in 0..<pinCodeCount {
                let tf = PinCodeTextField(frame: CGRect.zero)
                textFields.append(tf)
                self.addSubview(tf)
                tf.textAlignment = .center
                tf.delegate = self
                tf.keyboardType = .numberPad
                tf.addTarget(self, action: #selector(textFieldCotentDidChange(_:)), for: .editingChanged)
                tf.addTarget(self, action: #selector(textFieldCotentDidEnd(_:)), for: .editingDidEnd)
            }
        }
    }
    
    init(frame: CGRect, pinCode count: Int = 1) {
        super.init(frame: frame)
        setPinCodeCount(count)
        self.backgroundColor = UIColor.white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let startX = frame.width/CGFloat(pinCodeCount+2)-1
        let size = startX
        for (i, tf) in textFields.enumerated() {
            tf.frame = CGRect(x: startX+CGFloat(i*Int(size)+i*2), y: 0, width: size, height: size)
        }
    }
    
    func setPinCodeCount(_ count: Int) {
        self.pinCodeCount = count
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getFirstResponder(in view: UIView) -> UIView? {
        for subview in view.subviews {
            print(subview)
            if subview.isFirstResponder {
                return subview
            }
            if let fr = getFirstResponder(in: subview) {
                return fr
            }
        }
        return nil
    }
}

extension PinCodeView: PinCodeTextFieldProtocol {
    var isFilled: Bool {
        
        for tf in textFields where !tf.isFilled {
            return false
        }
        return true
    }

    var pinCodes: String {
        var pinCode: String = ""
        let _ = self.textFields.map { pinCode += $0.pinCode }
        return pinCode
    }
    func didPressBackspace(textField: PinCodeTextField) {
        becomeFirstResponder(before: textField)
    }
    @objc func textFieldCotentDidEnd(_ textField: UITextField) {
        if let method = self.inputComplete {
            method(self.isFilled, self.pinCodes)
        }
    }
    @objc func textFieldCotentDidChange(_ textField: UITextField) {
        guard let textField = textField as? PinCodeTextField else { return }
        if textField.pinCode.count == 0 {
            becomeFirstResponder(before: textField)
        }
        
        // when textfield has been filled, ok! next!
        if textField.pinCode.count == textField.maxCharacterCount {
            becomeFirstResponder(after: textField)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? PinCodeTextField else { return true }
        
        if string == "" {
            return true
        }
        
        // when textfield is not empty, well, next
        if textField.pinCode.count == textField.maxCharacterCount {
            becomeFirstResponder(after: textField)
            return false
        }
        if string.count > textField.maxCharacterCount {
            return false
        }
        return true
    }
    
    func becomeFirstResponder(after textField: UITextField) {
        becomeFirstResponder(of: textField, offset: 1)
    }
    func becomeFirstResponder(before textField: UITextField) {
        becomeFirstResponder(of: textField, offset: -1)
    }
    
    //swiftlint:disable force_cast
    @discardableResult
    private func becomeFirstResponder(of textField: UITextField, offset by: Int) -> Bool {
        var result = false
        if let i = textFields.firstIndex(of: textField as! PinCodeTextField) {
            textField.resignFirstResponder()
            if (0..<self.pinCodeCount).contains(i+by) {
                result = self.textFields[i+by].becomeFirstResponder()
            } else {
                
            }
        }
        return result
    }
}
