//
//  EmptyUserView.swift
//  ThePeel
//
//  Created by Gone on 3/4/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol HandleAuthenticationDelegate: NSObjectProtocol {
    func signIn()
    func signUp()
}

class EmptyUserView: UIView {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var signUpButton: UIButton!
    @IBOutlet fileprivate weak var signInButtonView: UIView!
    @IBOutlet fileprivate weak var signInButton: UIButton!
    
    // MARK: - Properties
    weak var handleAuthenticationDelegate: HandleAuthenticationDelegate?
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }
    
    public func setupUI() {
        signInButtonView.layer.borderWidth = 1
        signInButtonView.layer.borderColor = UIColor.white.cgColor
    }
    
    // MARK: - IBAction
    @IBAction func sigUpButton(_ sender: Any) {
        handleAuthenticationDelegate?.signUp()
    }
    
    @IBAction func sigInButton(_ sender: Any) {
        handleAuthenticationDelegate?.signIn()
    }
}
