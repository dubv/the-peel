//
//  BlankViewController.swift
//  ThePeel
//
//  Created by Gone on 3/14/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Reachability

class BlankViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var containLoadingView: UIView!
    @IBOutlet fileprivate weak var loadingImageView: UIImageView!
    
    // MARK: - Properties
    public var loadingImages = [UIImage]()
    private weak var timerTrigger: Timer?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(handle), name: .toBlank, object: nil)
        didChangeNetwork = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.modalTransitionStyle = .crossDissolve
        loadingImageView.animationImages = loadingImages
        loadingImageView.animationDuration = 1.7
        loadingImageView.animationRepeatCount = 100
        loadingImageView.startAnimating()
    }
    
    // MARK: - Initialization
    static func viewController() -> BlankViewController? {
        return Helper.getViewController(named: "BlankViewController", inSb: "Main")
    }
    
    public func setupUI() {
        for i in 1...11 {
            guard let image = UIImage(named: "iconPeiceLoading\(i)") else { return }
            loadingImages.append(image)
        }
        self.view.backgroundColor = .white
    }
    
    // MARK: - Target
    @objc func handle() {
        Timer.scheduledTimer(withTimeInterval: 2.5, repeats: true) { _ in
            UIView.animate(withDuration: 1.5, animations: {
                self.dismiss(animated: true, completion: nil)
                self.loadingImageView.stopAnimating()
                NotificationCenter.default.post(name: .toHome, object: nil)
            })
        }
    }
}

extension BlankViewController: DidChangeNetwork {
    func networkAvailable() {
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            self.dismissVCNormal()
        }
    }
}
