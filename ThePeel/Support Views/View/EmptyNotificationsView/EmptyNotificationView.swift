//
//  EmptyNotificationView.swift
//  ThePeel
//
//  Created by Gone on 3/8/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class EmptyNotificationView: UIView {
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
