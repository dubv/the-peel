//
//  CustomSheetView.swift
//  ThePeel
//
//  Created by Gone on 1/31/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Kingfisher

protocol SheetViewDelegate: NSObjectProtocol {
    func isPushed()
    func isSelfPresented()
}

extension SheetViewDelegate {
    public func isPushed() {}
    public func isSelfPresented() {}
}

class CustomSheetView: UIView {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var mainContainView: UIView!
    @IBOutlet fileprivate weak var productImageView: UIImageView!
    @IBOutlet fileprivate weak var productName: UILabel!
    @IBOutlet public weak var toCartButton: UIButton!
    
    // MARK: - Properties
    weak var sheetViewDelegate: SheetViewDelegate?
    var product: String = "" {
        didSet {
            self.productName.text = product
        }
    }
    var image: String = "" {
        didSet {
            guard let url = URL(string: image) else { return }
            productImageView.kf.setImage(with: url)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = .clear
        mainContainView.layer.cornerRadius = 9
        self.clipsToBounds = true
    }
    
    // MARK: - IBAction
    @IBAction func toCartButtonAction(_ sender: Any) {
        sheetViewDelegate?.isSelfPresented()
        sheetViewDelegate?.isPushed()
    }
}
