//
//  LASD.swift
//  ThePeel
//
//  Created by Gone on 2/6/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class MyIndicator: UIView {
    let imageView = UIImageView()
    
    init(frame: CGRect, color: String) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        guard let image = UIImage(named: color) else { return }
        imageView.frame = bounds
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(imageView)
    }
    
    required init(coder: NSCoder) {
        fatalError()
    }
    
    func startAnimating() {
        isHidden = false
        rotate()
    }
    
    func stopAnimating() {
        isHidden = true
        removeRotation()
    }
    
    private func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.imageView.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func removeRotation() {
        self.imageView.layer.removeAnimation(forKey: "rotationAnimation")
    }
}

var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.black.withAlphaComponent(0.95)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        
        ai.center = self.view.center
        ai.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
