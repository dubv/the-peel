//
//  VolumeButton.swift
//  ThePeel
//
//  Created by Gone on 1/13/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class VolumeView: UIView {
    
    // MARK: - Properties
    var spacingInset: CGFloat = 10
    
    lazy var increaseBtn: UIButton = {
        let button  = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var decreaseBtn: UIButton = {
        let button  = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var countLabel: UILabel = {
        let label  = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    internal func setup() {
//        increaseBtn.addTarget(self, action: #selector(increaseBtnActive), for: .touchUpInside)
//        decreaseBtn.addTarget(self, action: #selector(decreaseBtnActive), for: .touchUpInside)
    }
    
    private func updateView() {
        self.backgroundColor = UIColor(hexString: GlobalColor.volumeBtnBG)
        self.addSubview(increaseBtn)
        increaseBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        increaseBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.3).isActive = true
        increaseBtn.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        increaseBtn.setImage(UIImage(named: "increaseBtnBlack"), for: .normal)
        increaseBtn.tintColor = .black
        increaseBtn.backgroundColor = UIColor(hexString: GlobalColor.gray)
        
        self.addSubview(decreaseBtn)
        decreaseBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        decreaseBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.3).isActive = true
        decreaseBtn.leftAnchor.constraint(equalTo: increaseBtn.rightAnchor, constant: 1).isActive = true
        decreaseBtn.setImage(UIImage(named: "decreaseBtnBlack"), for: .normal)
        decreaseBtn.tintColor = .black
        decreaseBtn.backgroundColor = UIColor(hexString: GlobalColor.gray)
        
        self.addSubview(countLabel)
        countLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        countLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4).isActive = true
        countLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true

        countLabel.font = UIFont(name: Constants.PFRegular, size: 23)
        countLabel.tintColor = .black
        countLabel.textAlignment = .center
    }
}
