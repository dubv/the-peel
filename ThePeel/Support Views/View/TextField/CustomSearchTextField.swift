//
//  CustomSearchTextField.swift
//  ThePeel
//
//  Created by Gone on 2/15/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol SearchTextFieldDelegate: NSObjectProtocol {
    func searching(searchKey: String)
}

@IBDesignable class CustomSearchTextField: UIView {

    // MARK: - Properties
    weak var searchTextFieldDelegate: SearchTextFieldDelegate?
    
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    lazy var searchingButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(toggleSearching), for: .touchUpInside)
        return button
    }()
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightButtonImage: String = "" {
        didSet {
            let bundle = Bundle(for: type(of: self))
            let img = UIImage(named: rightButtonImage, in: bundle, compatibleWith: nil)
            self.searchingButton.setImage(img, for: .normal)
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var marginLeft: CGFloat = 0.0 {
        didSet {
            textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: marginLeft).isActive = true
        }
    }
    
    @IBInspectable var placeHolder: String = "" {
        didSet {
            textField.placeholder = placeHolder
            textField.addPlaceholderSpacing(spacing: 1.4)
        }
    }
    
    @IBInspectable var textFieldBG: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: textFieldBG)
        }
    }
    
    @IBInspectable var textColor: String = "" {
        didSet {
            self.textField.tintColor = UIColor(hexString: textColor)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    internal func updateView() {
        self.addSubview(textField)
        textField.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        textField.font = UIFont(name: Constants.PFLight, size: 20)
//        textField.addTextSpacing(spacing: 3.0)
        
        self.addSubview(searchingButton)
        
        searchingButton.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        searchingButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1).isActive = true
        searchingButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -1).isActive = true
        searchingButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        searchingButton.contentMode = .scaleAspectFit
        searchingButton.clipsToBounds = true
    }
    
    // MARK: - Target
    @objc func toggleSearching() {
        guard let searchKey = textField.text else {
            return
        }
        self.searchTextFieldDelegate?.searching(searchKey: searchKey)
    }
}
