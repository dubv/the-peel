//
//  CommentTextField.swift
//  ThePeel
//
//  Created by Gone on 2/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CommentTextField: UIView {
    
    // MARK: - Properties
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var marginLeft: CGFloat = 0.0 {
        didSet {
            textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: marginLeft).isActive = true
        }
    }
    
    @IBInspectable var placeHolder: String = "" {
        didSet {
            textField.placeholder = placeHolder
        }
    }
    
    @IBInspectable var textFieldBG: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: textFieldBG)
        }
    }
    
    @IBInspectable var textColor: String = "" {
        didSet {
            self.textField.tintColor = UIColor(hexString: textColor)
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    internal func updateView() {
        self.addSubview(textField)
        textField.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        if let font = Constants.OpenSans[2] {
            textField.font = UIFont(name: font, size: 14)
        }
    }
}
