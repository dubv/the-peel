//
//  CustomTextField.swift
//  ThePeel
//
//  Created by Gone on 1/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

@IBDesignable class CustomTextField: UIView {
    // MARK: - Properties
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var visibilityButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(togglePasswordVisibility), for: .touchUpInside)
        return button
    }()
    
    @IBInspectable var checked: Bool = false {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var image: String = "" {
        didSet {
            let bundle = Bundle(for: type(of: self))
            let img = UIImage(named: image, in: bundle, compatibleWith: nil)
            self.imageView.image = img
        }
    }
    
    @IBInspectable var rightButtonImage: String = "" {
        didSet {
            let bundle = Bundle(for: type(of: self))
            let img = UIImage(named: rightButtonImage, in: bundle, compatibleWith: nil)
            self.visibilityButton.setImage(img, for: .normal)
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var marginLeft: CGFloat = 0.0 {
        didSet {
            textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: marginLeft).isActive = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: String = "" {
        didSet {
            self.layer.borderColor = UIColor(hexString: borderColor).cgColor
        }
    }
    
    @IBInspectable var placeHolder: String = "" {
        didSet {
            textField.placeholder = placeHolder
        }
    }
    
    @IBInspectable var securityPassword: Bool = false {
        didSet {
            textField.isSecureTextEntry = securityPassword
        }
    }
    
    @IBInspectable var textFieldBG: String = "" {
        didSet {
            self.backgroundColor = UIColor(hexString: textFieldBG)
        }
    }
    
    @IBInspectable var textColor: String = "" {
        didSet {
            self.textField.tintColor = UIColor(hexString: textColor)
        }
    }

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    internal func updateView() {
        self.addSubview(textField)
        
        textField.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        if let font = Constants.OpenSans[2] {
            textField.font = UIFont(name: font, size: 14)
        }
        textField.addTextSpacing(spacing: 0.0)
        
        self.addSubview(imageView)
        
        imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        imageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        self.addSubview(visibilityButton)
        
        visibilityButton.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        visibilityButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1).isActive = true
        visibilityButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -1).isActive = true
        visibilityButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        visibilityButton.contentMode = .scaleAspectFit
        visibilityButton.clipsToBounds = true
    }
    
    // MARK: - Target
    @objc func togglePasswordVisibility() {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        visibilityButton.setImage(textField.isSecureTextEntry ? UIImage(named: "iconVisibilityO") : UIImage(named: "iconVisibility"), for: .normal)
    }
}
