//
//  LogoutViewController.swift
//  ThePeel
//
//  Created by Gone on 2/21/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class LogoutViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet public weak var cancelButton: UIButton!
    @IBOutlet public weak var okayButton: UIButton!
    @IBOutlet public weak var containOkayButtonView: UIView!
    
    // MARK: - Properties
    public var doLogout: (() -> Void)?
    public var didLogout: (() -> Void)?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Initialization
    static func viewController() -> LogoutViewController? {
        return Helper.getViewController(named: "LogoutViewController", inSb: "Authorization")
    }
    
    public func setupUI() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        containOkayButtonView.layer.borderColor = UIColor.white.cgColor
        containOkayButtonView.layer.borderWidth = 1
        okayButton.backgroundColor = .clear
        okayButton.setTitleColor(.white, for: .normal)
    }
    
    // MARK: - IBAction
    @IBAction func okayButton(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            
            try firebaseAuth.signOut()
            didLogout?()
            UUID.uid = nil
            self.dismiss(animated: true, completion: nil)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        doLogout?()
    }
}
