//
//  AlertViewController.swift
//  ThePeel
//
//  Created by Gone on 1/18/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

protocol AlertViewControllerDelegate: NSObjectProtocol {
    func popingView()
    func okayButton()
}

extension AlertViewControllerDelegate {
    public func popingView() {}
    public func okayButton() {}
}

class AlertViewController: UIViewController {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var titleAlertLabel: UILabel!
    @IBOutlet fileprivate weak var messAlertLabel: UILabel!
    @IBOutlet fileprivate weak var backButton: UIButton!
    @IBOutlet fileprivate weak var toBackView: UIView!
    @IBOutlet fileprivate weak var dismissButton: UIButton!
    
    // MARK: - Properties
    weak var alertViewControllerDelegate: AlertViewControllerDelegate?
    
    var data: AlertView? {
        didSet {
            loadData()
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    fileprivate func loadData() {
        guard isViewLoaded, let data = self.data else { return }
        titleAlertLabel.text = data.title
        messAlertLabel.text = data.message
        backButton.setTitle(data.buttonTitle, for: .normal)
    }
    
    // MARK: - Initialization
    static func viewController() -> AlertViewController? {
        return Helper.getViewController(named: "AlertViewController", inSb: "Main")
    }
    
    fileprivate func setupUI() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backButton.backgroundColor = UIColor.init(hexString: GlobalColor.gray)
    }

    // MARK: - IBAction
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backToAnyButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        alertViewControllerDelegate?.popingView()
        alertViewControllerDelegate?.okayButton()
    }
}
