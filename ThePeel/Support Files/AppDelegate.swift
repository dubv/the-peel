import UIKit
import UserNotifications
import FirebaseAuth
import Firebase
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var handle: AuthStateDidChangeListenerHandle?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        let window = UIWindow(frame: UIScreen.main.bounds)
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if auth.currentUser != nil {
                guard let uid = user?.uid else {
                    return}
                /// Global uuid
                UUID.uid = uid
            } else {
                UUID.uid = nil
            }
        }
        GMSServices.provideAPIKey(gmKeyAPI)
        GMSPlacesClient.provideAPIKey(gmKeyAPI)
        Application.shared.configMainInterface(window: window)
        application.registerForRemoteNotifications()
        self.window = window
        return true
    }
    
    // MARK: - Config Firebase Auth
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        #if DEBUG
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        #else
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        #endif
        // Further handling of the device token if needed by the app
        print("APNs token retrieved: \(deviceToken)")
    }
    
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        // URL not auth related, developer should handle it.
        return false
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Network.sharedInstance.stopMonitoring()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Network.sharedInstance.startMonitoring()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        handleCloudMessage(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handleCloudMessage(userInfo: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    // [END receive_message]
    
    // [START Custom Handle CloudMessage]
    func handleCloudMessage(userInfo: [AnyHashable : Any]) {
        guard let dict = userInfo["aps"] as? NSDictionary else { return }
        if let alert = dict["alert"] as? NSDictionary, let category = dict["category"] as? String {
            if let title = alert["title"] as? String {
                if title == GlobalText.TITLE_NEW_PRODUCT {
                    let categoryIdentifier: [String: String] = ["productId": category]
                    NotificationCenter.default.post(name: .newProduct, object: nil, userInfo: categoryIdentifier)
                    UserDefaults.standard.set(category, forKey: "SHOW_PRODUCT")
                } else if title == GlobalText.TITLE_DELIVERY_STATES {
                    let categoryIdentifier: [String: String] = ["itemId": category]
                    NotificationCenter.default.post(name: .toItemOrdered, object: nil, userInfo: categoryIdentifier)
                    UserDefaults.standard.set(category, forKey: "SHOW_ITEM")
                }
            }
        }
    }
    // [END Custom Handle CloudMessage]
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        handleCloudMessage(userInfo: notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        handleCloudMessage(userInfo: response.notification.request.content.userInfo)
        completionHandler()
    }
}

// [END ios_10_message_handling]
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let dataDict: [String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        Messaging.messaging().shouldEstablishDirectChannel = true
        Messaging.messaging().useMessagingDelegateForDirectChannel = true
    }
    // [END ios_10_data_message]
}
